<?php
namespace Modules\Setup;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
/**
*
*/
class Init
{
    protected $data;

    function __construct()
    {

    }

    public function setup($vars){

        $data['tpl'] = array(
                                'top_bar' => array(
                                                    'bar_search' => false,
                                                    'navbar_custom_menu' => !Auth::check()?false:true
                                                ),
                                'left_sbar' => array(

                                                ),
                                'right_sbar' => array(

                                                ),

                            );
        if(Session::get('bghmcpermission')){
            $einfo = DB::table('bghmc_employee_info')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->first();
            $data['menu'] = 'administrators';
            $data['page_title'] = Session::get('bghmcpermission')->ugrp_name;
            $data['userid'] = Session::get('bghmcuser')->emp_id;
            $data['realname'] = $einfo->f_name[0] . '. ' . $einfo->l_name;
            $data['navs'] = $this->get_all_navigations();
        }else{
            $data['menu'] = '';
            $data['page_title'] = 'BGHMC-FTS '.'('.$vars['page'].')';
            $data['realname'] = '';
            $data['userid'] = '';
            $data['navs'] = '';
        }

        return $data;

    }

    public function get_all_navigations(){
        $navs['main'] = DB::table('bghmc_tmpl_main_navigation')
                                ->select('id', 'route', 'parent', 'arangement','properties','title')
                                ->where('group_id',Session::get('bghmcpermission')->id)
                                ->where('deleted_at',NULL)
                                ->orderBy('arangement')
                                ->get();
        $navs['subs'] = DB::table('bghmc_tmpl_main_navigation')
                    ->join('bghmc_tmpl_sub_navigation', 'bghmc_tmpl_main_navigation.id', '=', 'bghmc_tmpl_sub_navigation.parent_id' )
                    ->select('bghmc_tmpl_sub_navigation.id',
                             'bghmc_tmpl_sub_navigation.parent_id',
                             'bghmc_tmpl_sub_navigation.title',
                             'bghmc_tmpl_sub_navigation.route',
                             'bghmc_tmpl_sub_navigation.properties')
                    ->where('bghmc_tmpl_main_navigation.group_id', Session::get('bghmcpermission')->id)
                    ->orderBy('bghmc_tmpl_sub_navigation.arangement')
                    ->get();
        return $navs;
    }





}
?>