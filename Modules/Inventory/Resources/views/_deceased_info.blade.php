 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Group List
      </h1>
    </section>

    <!--@if(count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif-->

    <section class="content">


      <div class="box">
            <div class="box-header">
              <h3 class="box-title">GROUP</h3>
              <button type="button" class="btn btn-info btn-lg pull-right">NEW CLIENT <i class="fa fa-user-plus"></i> </button>

            </div>
            <div class="box-body">
             
            </div>
          </div>
    </section>
  </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
      $('#example1').DataTable();


    </script>
@stop