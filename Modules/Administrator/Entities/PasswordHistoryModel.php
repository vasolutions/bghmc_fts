<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PasswordHistoryModel extends BaseModel
{
    protected $table = 'bghmc_emp_credentials_reset';
    protected $fillable = ['emp_id'];
}
