<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\DepartmentsModel as DM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class DepartmentsModel extends BaseModel
{
    protected $table = 'bghmc_departments';
    protected $fillable = ['dept_id', 'dept_name'];
    protected $primaryKey = 'dept_id';

    protected $rules = array(
        'deptname' => 'Required|min:3|regex:/^([0-9 ]*[A-Za-z ]+[0-9 ]*)+$/'
    );

    public function setInfo($request){
        $count = DB::table('bghmc_departments')->get();
        
        $DM = new DM;
        $DM->dept_id = 680001 + $count->count();
        $DM->dept_name = $request->input('deptname');
        $DM->save();

        $L = new SLM;
        $L->setLog($request, '', 'new_department', null);
    }

    public function updateInfo($request){
        $DMinfo = DM::find($request->input('depid'));
        $DM = DM::find($request->input('depid'));
        $DM->dept_name = $request->input('deptname');
        $DM->save();

        $L = new SLM;
        $L->setLog($request, $DMinfo, 'update_department', null);
    }
}
