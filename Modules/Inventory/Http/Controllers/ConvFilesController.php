<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
class ConvFilesController extends Controller

{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null){   
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;}



// === CREATE FILE ===
    public function store(Request $request){return $this->send($request);}
    public function send($request){
        if (($request['a0-data'])==1){$importances = 1;} else{$importances = 0;}	// GET AND SET THE IMPORTANCE OF THE FILE HERE   - (1/0 = urgent/notUrgnet)
        $owner = Auth::user()->emp_id;  						// id number of the logged in user
        $ct = Carbon::now();    							// date
        $ctDATES = Carbon::now()->toDayDateTimeString();    				// formatted date
        $receiversList = [];    							// array for the logs

	if( (($request['MSGhere']) == null) || (($request['MSGhere']) ==''))		// GET AND SET THE MESSAGE SENT
		{$sadMSG = '-- NO MESSAGE --';}
	else{$sadMSG = $request['MSGhere'];}
	$docNAME = DB::table('bghmc_filetypes')->where('filetype_id',$request['docTYPE'])->first();	// GET AND SET THE DOCUMENT TYPE
        
// ================================================================================================= //
// ===== DETERMINES THE RECEIVER TYPE OF THE MESSAGE - SETS THE ROUTE MESSAGE  ===== //
// ================================================================================================= //
	$receivedARRAY = array();
	$routeStats=0;
	$actualReceiver = explode(',',$request['a1-data']);
	if($request['smOsr'] == 'sr'){
		$routeStats=1; 
		$statusShit = 'File was sent in a route.' . $ctDATES; 
		$receiverTYPE = 'ROUTE DEPARTMENTS';
		if ( (($request['HardCopy']) == '0'  &&  ($request['docName']) == null) || ($request['HardCopy'] != '0') ){
			return redirect()->back()->with('successMSG','MESSAGE FAILED! Please add your hardcopy documents or provide name/s for your hardcopy documents');
		}
        }elseif($request['smOsr'] == 'sm'){
		$receiverTYPE = 'MULTIPLE DEPARTMENTS';
		$statusShit='File was sent to multiple departments.' . $ctDATES;
	}elseif($request['smOsr'] == 'pm'){
		$receiverTYPE = 'PERSONAL MESSAGE';
		$statusShit='File was sent as a personal message.' . $ctDATES;
	}elseif(($request['smOsr'])=='sa'){
		$receiverTYPE = 'ALL DEPARTMENTS';
            	$allUsersID = DB::table('bghmc_departments')->pluck('dept_id');		// gets all departments
            	$receiverCTR = count($allUsersID); 						// counts all
            	$statusShit = 'File was sent to all department.' . $ctDATES;
            	$RECCTR = 0;
                while($RECCTR < $receiverCTR ){
                    array_push($receivedARRAY, $allUsersID[$RECCTR]);			// saves all receivers ID in the variable
                    $RECCTR = $RECCTR+1;
                }
            	$actualReceiver =  $receivedARRAY;
        }
// =========================================== END ================================================ //
            
// ================================================================================================= //
// ===== SETS THE STATUS FOR EACH RECEIVER -- IF HARDCOPY FILES ARE SENT -- WILL ALSO BE ADDED ===== //
// ================================================================================================= //
	$documentArray = array();				// where all the stats are saved
	$StatCTR = 0;
	$hc = "NO";
	$docs = array();
        if (($request['HardCopy']) == '0'  &&  ($request['docName']) == null){
		return redirect()->back()->with('successMSG','MESSAGE FAILED! Please provide name/s for your hardcopy documents');
	}elseif(($request['HardCopy']) == '0'  &&  ($request['docName']) != null){$hc = "YES";}
	if($request['smOsr'] != 'pm'){
		array_push($docs, "Documents"); 				// adds the field for document names sent
		array_push($docs, "EmpNAME"); 				// adds the field for employee name = this will be the name of the person to accept the file in each department
		array_push($docs, "EmpDEPT");				// adds the department name of each receivers
		array_push($docs, "AcceptStatus"); 			// adds the field of acceptance status
		array_push($docs, "dateAccepted");			// adds the field of date of acceptance
		array_push($docs, "ArchiveStatus");  			// adds the field of arvhive status
		array_push($docs, "dateArchived"); 			// adds the field of date of archive
		array_push($docs, "dateRead");				// adds the field for raed messages
		array_push($docs, "dateRouteForwarded");		// adds the field for forwarded ROUTE MESSAGES
		// ===== ADDS ALL THE FIELDS FOR EACH RECEIVER ===== //
        	foreach($actualReceiver as $x){
			$z = DB::table('bghmc_departments')->where('dept_id',$x)->first();
			foreach($docs as $y){
				$documentArray[$StatCTR][$x][$y] = "N";
				if($y == "EmpDEPT"){$documentArray[$StatCTR][$x][$y] = $z->dept_name;}
				if($y == "Documents"){
					$arr = array();
					foreach($request['docName'] as $a){$arr[$a] = "N";}
					$documentArray[$StatCTR][$x][$y] = $arr;
				}
			}
			$StatCTR++;
		}
	$s = implode(" ",array_keys(current($documentArray)));
	$documentArray[0][$s]['dateRouteForwarded'] = Carbon::now()->toDayDateTimeString();
	}else{
		$z = DB::table('bghmc_employee_info')->where('emp_id',implode("",$actualReceiver))->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
		$documentArray = strtoupper($z->f_name)." ".strtoupper($z->l_name)." of ".$z->dept_name." Department";
	}
//dd($documentArray);
// =========================================== END ================================================ //

// ====================================================================================================== //
// ===== CREATES A TRACKING NUMBER FOR EACH FILE SENT -- DATABASE IS A MUST STORAGE  ==================== //
// ===== with the current tracking number logic.. it can generate 19,999,998 TRACKING NUMBERS =========== //
// ===== if these are not enough. maintenance will be notified for server maintenance =================== //
// ====================================================================================================== //
        $happ = DB::table('bghmc_transactions')->count(); 				//number of records in the database
        $happy = DB::table('bghmc_transactions')->select('CTR1')->first(); 		//get counter
        $happy1 = DB::table('bghmc_transactions')->select('CTR2')->first(); 		//get counter
       // $happy2 = DB::table('bghmc_thread')->select('ctr')->first(); 			//get counter
        $additionals = strtoupper(mb_substr($owner, 0, 3));
        $tnCTR1 = 0; $tnCTR2 = '000'; $zzzzz=0;$zzzz = 0; $zzz = 0; $y1 = 0; $y2 = 0; $y3 = 0;
        
        if($happ == 0){
        DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>0]);
        $idss = '0'.'BG-'.$additionals.'000'.'-'.'000';}  				//sets the very first tracking number if only there are no records
        else{
            $sadShit = $happy->CTR1 + 1;
            DB::table('bghmc_transactions')->update(['CTR1'=>$sadShit,]);
            $idss = '0'.'BG-'.$additionals.'000'.'-'.'00'.$sadShit;
            if($sadShit == 10){
                $idss = '0'.'BG-'.$additionals.'000'.'-'.$zzz.$sadShit;
            }elseif($sadShit == 100){
                $idss = '0'.'BG-'.$additionals.'000'.'-'.$sadShit;
            }elseif($sadShit == 1000){
                $sadShit1 = $happy1->CTR2 + 1;
                DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>$sadShit1]);
                $idss = '0'.'BG-'.$additionals.$y1.$y2.$sadShit1.'-'.'000';
                    if($sadShit1 == 10){
                        $idss = $tnCTR1.'BG-'.$additionals.$y1.$sadShit1.'-'.'000';
                    }elseif($sadShit1 == 100){
                        $idss = $tnCTR1.'BG-'.$additionals.$sadShit1.'-'.'000';
                    }elseif($sadShit1 == 1000){
                        $sadShit2 = $happy1->CTR3 + 1;
                        DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>$sadShit2]);
                        $idss = $sadShit2.'BG-'.$additionals.'000'.'-'.'000';
                            if ($sadShit2 == 10){
                                DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>0]);
                                $idss = '0'.'BH-'.$additionals.'000'.'-'.'000';
                            }
                    }
                
            }
        }
// =========================================== END ================================================ //

// ====================================================================================================== //
// ===== DETERMINES IF THERE ARE ATTACHMENTS(SOFTCOPY) ADDED -- CREATES A PATH OF DOWNLOAD IF TRUE  ===== //
// ====================================================================================================== //
        if ((($request['a5-data']) == '') || (($request['a5-data']) == null)){
            $aID = 0; 
            $ANAME = 'NO ATTACHMENT';							//name of attachment -- if no attachment sent
        }else{
            $aID = 1;
            $fileAttachment = $request->file('a5-data');
            $fileUplaod = $request['a5-data'];
            $InitialNAME = $fileAttachment->getClientOriginalName();
            $name = $owner.$idss.$InitialNAME;							//name of attachment sent
            $sizes = $fileAttachment->getSize();
            $pathHere = storage_path('app/FileAttachments/'.$name);
            // =========== CHECK IF ALREADY STORED ==========
            if(Storage::exists('app/FileAttachments/'.$owner. '/' .$name)){}
            else{
                Storage::putFileAs('FileAttachments/'.$owner, $fileUplaod,$name);
                $ANAME = $name;								//name of attachment storing
            }
        }
// =========================================== END ================================================ //


// CREATES FILE FOR THE DATABASE
        TM::create(['transNO' =>    $idss,
			'recTYPE' =>    $receiverTYPE,			// enters the type of receiver - (ALL DEPT, PEROSNAL, ROUTES, ETC)
			'docTYPE' =>    $docNAME->filetype_name,	// enters the doc type - (communication letter, med redcords, purchase order, etc)
                    	'SUBJ' 	=>   	$request['a2-data'],		// enters the subject - $request['a2-data'] is the data entered by the user - a2-data reference it in views\FileManagement\createFile.blade
                    	'MSG'	=>    	$sadMSG,			// enters the message - $request['MSGhere'] is the data entered by the user - MSGhere reference it in views\FileManagement\createFile.blade
        		'IMP'	=>     	$importances,			// enters the importance of the file
			'hardCOPY'  => 	$hc,				// enters if the file have a hard copy - route files have mandatory hardcopy files
			'fileSTATS' => 	serialize($documentArray),	// enters the document names - these are the HARD COPY DOCUMENT NAMES - route files have mandatory hardcopy files
			'attachNAME'=>  $ANAME,				// enters the attachment name - these is the name of SOFT COPY FILE added to the message sent - all are optional
			'sendID'    => 	$owner,				// enters the ID of the sender
			'threadMSG' =>   '',				// enters blank - this is used or the replies and or messages sent via the reply inside a message box
			'SenderDateSENT'  => $ctDATES,
			'SenderDateArchived'  => 'N',
			'trackingSTATS' =>   $statusShit,		// just for logs purposes - and tracking of files
                    	'ROUTESTAT'	=>   $routeStats,		// 1 = route message -- 0 = normal message
                    	'CTR1'=>    0,					// counters for the tracking number
                    	'CTR2'=>    0,					// counters for the tracking number
                    	'CTR3'=>    0,					// counters for the tracking number
                    
                ]);
        

   //     $L = new SLM;

//        array_push($receiversList, $request['a2-data'], $request['FILETYPES'], $aID);
  //      $L->setLog($receiversList, '', 'send_message', null);

        return redirect()->back()->with('successMSG','Message Sent!');
    }

















   
}
