<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class TransactionsModel extends BaseModel
{
    protected $table = 'bghmc_transactions';
    protected $fillable = ['transNO','recTYPE','docTYPE','SUBJ','MSG','IMP','hardCOPY','fileSTATS','attachNAME','sendID','threadMSG','SenderDateSENT','SenderDateArchived','trackingSTATS','ROUTESTAT','CTR1','CTR2','CTR3','updateStatus'];
    protected $primaryKey = 'ID';


}
