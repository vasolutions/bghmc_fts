<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BghmcSystemLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_system_logs', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('id');
            $table->integer('logged_user');
            $table->string('log');
            $table->longText('log_data_before')->nullable();
            $table->longText('log_data_after')->nullable();
            $table->string('type');
                    
            $table->timestamps();
            $table->softDeletes();

            $table->index(['logged_user'],'bghmc_system_logs'); //keywords for db para mabilis mahanap
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_system_logs');
    }
}
