 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script>
@stop

@section('pagename')
    
    <span style="font-size: 25px;">
        @include('inventory::includes._titleFILE')
    </span>
@stop

@section('content')

    <div class="content-wrapper">
        <section class="content">
        @if(($tite_page)!='Reports')
        @include('inventory::includes._ALL')
        @endif
            <div class="box">
                @if(Session::has('successMSG'))
                  <div class="boxAlert alertSessions" id="sMSG">
                    <span> {{Session::get('successMSG')}}</span><br>
                    <button class="btn btn-new pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(Session::has('errorMSG'))
                  <div class="boxAlert alertSessions" id="eMSG">
                    <span> {{Session::get('errorMSG')}}</span><br>
                    <button class="btn btn-danger pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(($tite_page)!='Reports')
                <div class="box-header">
                    <div id="NOTIFICATIONSSTUFF" >
                        @foreach($_file as $z1)
                            @if($z1->sendID == Auth::user()->emp_id)
<!-- {{--                                @if($z1->pendingRouteRec != NULL)
                                    <div class="alert alert-danger" id="NOTIFICATIONSSTUFFALERT">{{$z1->TNO}} => {{$z1->pendingRouteRec}}
                                    <a href="{{route('CheckNotif',['tno'=>$z1->TNO])}}" style="color:#c0c0c0;"><button class="btn pull-right btn-link" style="color:#c0c0c0;">CHECK</button></a>
                                    </div>
                                    
                                	@else @endif                        
--}}-->
                            @else <div class="alertSession"></div> @endif
                        @endforeach
                    </div>
                    
                    <ul class="pull-right" style="list-style-type: none; display: flex; align-items:center; ">
                        <li style="padding-top:5px;">
                            <div style="text-align:right; margin-right: 20px;" class="searchs">
                                    <input type="text" id="searchUL" onkeyup="searchFunctions()" placeholder="Track file.." title="Enter Tracking No.">
                            </div>
                        </li>
                        <li>

                        </li>
                        <li style="padding-top: 15px;" >
                            <b id="ComposeLI">
                            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file"><i class="glyphicon glyphicon-pencil" ></i> &nbsp; Compose</button></b>
                    		<b id="searchLI" style="display:none;">
                    		<button type="button" class="btn btn-new" id="searchBtn"><i class="fa fa-search" ></i> &nbsp;Search</button>
                    		</b>
                        </li>
                    </ul>
                    

                </div>
<!-- ============== TRACKING FILES CODE SHIT ================ -->
                <h5 class="filesNotice" id="hideThis" style="color:red; font-style: italic;">NOTE: You can search using tracking number or the subject name!</h5>
                <div id="hideThis" class="showsLISTS">
                    <div id="TRAckFiles">
                        <table id="searchTBL" class="table table-striped table-bordered table-hover">
                            @foreach($SFF as $S)
                            @if($S->involved == 'INVOLVED')
                            
                            <tr>
                                <td style="max-width:100px; min-width: 100px;">
                                    <h3>{{$S->TNO}}</h3><!-- tracking number --> 
                                    File Type: {{$S->filetype_name}} <!-- File TYpe --><br>
                                    Subject: {{$S->SUBJ}}<!-- subject --><br>
                                    {{$S->dateSentBySend}}<!-- date file sent --><br>
                                    @if($S->sendID == $USERID)
                                        -- SENT TO --<br>
                                        <!-- RECEIVER NAME -->
                                        @foreach($S->NAMESHITS as $RD)
                                            {{$RD}}<br>
                                        @endforeach
					
                                        <a href="{{route('SpecificFileSender',['TNO'=>$S->TNO])}}" 
                                        class="btn btn-sm btn-block btn-primary" style="padding: 0px; margin:0px;"> VIEW FILE
                                        </a>
                                    @else
                                         -- RECEIVED From --<br>
                                        {{$S->dept_name}}  -- {{$S->f_name}} {{$S->l_name}} <br>
                                        {{$S->ACCPTSS }}

                                        @if($S->ROUTESTAT == 0)
                                            <a href="{{route('SpecificFileReceiver',['track'=>$S->TNO,'rec'=>$USERID,'acpt'=>$S->ACCPTSS,'ANum'=>$S->arrayNO]) }}"  
                                            class="btn stuff btn-sm btn-block btn-primary">
                                                VIEW FILE
                                            </a>
                                        @else
                                            <a href="{{route('SpecificFileReceiverInARoute',['track'=>$S->TNO,'rec'=>$USERID,'acpt'=>$S->ACCPTSS,'ANum'=>$S->arrayNO]) }}" 
                                            class="btn stuff btn-sm btn-block btn-primary">
                                                VIEW FILE
                                            </a>
                                        @endif
                                    @endif
                                </td>
                                <td style="max-width: 300px; min-width: 300px;"><br><b>UPDATES:</b>
                                    <?php
                                      $updates = nl2br($S->FILESTATUSSEND);
                                        if($updates == ''){
                                          echo "<i style='font-size:15px;'> NO UPDATES </i>";
                                        }else{
                                          echo "<i style='font-size:15px;'>";
                                          echo $updates;
                                          echo '</i>';
                                        }
                                    ?>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
                        
                    </div>
                    
                </div> <!-- ================== END ================ -->
                @endif
                <div class="sss">
                    @if(($tite_page)=='Outbox')
                        @include('inventory::FileManagement._sentFile')
                    @elseif(($tite_page)=='Inbox')
                        @include('inventory::FileManagement._recFile')
                    @elseif(($tite_page)=='Dashboard')
                        @include('inventory::FileManagement._dashyBoardy')
                    @elseif(($tite_page)=='Reports')
                        @include('inventory::FileManagement._repos')
                    @else
                        @include('inventory::FileManagement._otherFile')
                    @endif
                </div>
            </div>
                    
        </section>
    </div>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#TRAckFiles").load(location.href+" #TRAckFiles>*",""); });

      function searchFunctions() {
          var input, filter, table, tr, td, i;
          input = document.getElementById("searchUL");
          filter = input.value.toUpperCase();
          table = document.getElementById("searchTBL");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) { if (td.innerHTML.toUpperCase().indexOf(filter) > -1) { tr[i].style.display = ""; } 
              else { tr[i].style.display = "none"; }
            }       
          }
        }
    </script>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <!-- <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.js"></script>
   <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.min.js"></script>
   <script src="{{asset('includes-jquery')}}/Gruntfile.js"></script>

@stop @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script>
@stop

@section('pagename')
    
    <span style="font-size: 25px;">
        @include('inventory::includes._titleFILE')
    </span>
@stop

@section('content')

    <div class="content-wrapper">
        <section class="content">
        @if(($tite_page)!='Reports')
        @include('inventory::includes._ALL')
        @endif
            <div class="box">
                @if(Session::has('successMSG'))
                  <div class="boxAlert alertSessions" id="sMSG">
                    <span> {{Session::get('successMSG')}}</span><br>
                    <button class="btn btn-new pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(Session::has('errorMSG'))
                  <div class="boxAlert alertSessions" id="eMSG">
                    <span> {{Session::get('errorMSG')}}</span><br>
                    <button class="btn btn-danger pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(($tite_page)!='Reports')
                <div class="box-header">
                    <div id="NOTIFICATIONSSTUFF" >
                        @foreach($_file as $z1)
                            @if($z1->sendID == Auth::user()->emp_id)
<!--                                @if($z1->pendingRouteRec != NULL)
                                    <div class="alert alert-danger" id="NOTIFICATIONSSTUFFALERT">{{$z1->TNO}} => {{$z1->pendingRouteRec}}
                                    <a href="{{route('CheckNotif',['tno'=>$z1->TNO])}}" style="color:#c0c0c0;"><button class="btn pull-right btn-link" style="color:#c0c0c0;">CHECK</button></a>
                                    </div>
                                    
                                @else @endif                        
-->
                            @else <div class="alertSession"></div> @endif
                        @endforeach
                    </div>
                    
                    <ul class="pull-right" style="list-style-type: none; display: flex; align-items:center; ">
                        <li style="padding-top:5px;">
                            <div style="text-align:right; margin-right: 20px;" class="searchs">
                                    <input type="text" id="searchUL" onkeyup="searchFunctions()" placeholder="Track file.." title="Enter Tracking No.">
                            </div>
                        </li>
                        <li>

                        </li>
                        <li style="padding-top: 15px;" >
                            <b id="ComposeLI">
                            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file"><i class="glyphicon glyphicon-pencil" ></i> &nbsp; Compose</button></b>
                    		<b id="searchLI" style="display:none;">
                    		<button type="button" class="btn btn-new" id="searchBtn"><i class="fa fa-search" ></i> &nbsp;Search</button>
                    		</b>
                        </li>
                    </ul>
                    

                </div>
<!-- ============== TRACKING FILES CODE SHIT ================ -->
                <h5 class="filesNotice" id="hideThis" style="color:red; font-style: italic;">NOTE: You can search using tracking number or the subject name!</h5>
                <div id="hideThis" class="showsLISTS">
                    <div id="TRAckFiles">
                        <table id="searchTBL" class="table table-striped table-bordered table-hover">
                            @foreach($SFF as $S)
                            @if($S->involved == 'INVOLVED')
                            
                            <tr>
                                <td style="max-width:100px; min-width: 100px;">
                                    <h3>{{$S->TNO}}</h3><!-- tracking number --> 
                                    File Type: {{$S->filetype_name}} <!-- File TYpe --><br>
                                    Subject: {{$S->SUBJ}}<!-- subject --><br>
                                    {{$S->dateSentBySend}}<!-- date file sent --><br>
                                    @if($S->sendID == $USERID)
                                        -- SENT TO --<br>
                                        <!-- RECEIVER NAME -->
                                        @foreach($S->NAMESHITS as $RD)
                                            {{$RD}}<br>
                                        @endforeach
					
                                        <a href="{{route('SpecificFileSender',['TNO'=>$S->TNO])}}" 
                                        class="btn btn-sm btn-block btn-primary" style="padding: 0px; margin:0px;"> VIEW FILE
                                        </a>
                                    @else
                                         -- RECEIVED From --<br>
                                        {{$S->dept_name}}  -- {{$S->f_name}} {{$S->l_name}} <br>
                                        {{$S->ACCPTSS }}

                                        @if($S->ROUTESTAT == 0)
                                            <a href="{{route('SpecificFileReceiver',['track'=>$S->TNO,'rec'=>$USERID,'acpt'=>$S->ACCPTSS,'ANum'=>$S->arrayNO]) }}"  
                                            class="btn stuff btn-sm btn-block btn-primary">
                                                VIEW FILE
                                            </a>
                                        @else
                                            <a href="{{route('SpecificFileReceiverInARoute',['track'=>$S->TNO,'rec'=>$USERID,'acpt'=>$S->ACCPTSS,'ANum'=>$S->arrayNO]) }}" 
                                            class="btn stuff btn-sm btn-block btn-primary">
                                                VIEW FILE
                                            </a>
                                        @endif
                                    @endif
                                </td>
                                <td style="max-width: 300px; min-width: 300px;"><br><b>UPDATES:</b>
                                    <?php
                                      $updates = nl2br($S->FILESTATUSSEND);
                                        if($updates == ''){
                                          echo "<i style='font-size:15px;'> NO UPDATES </i>";
                                        }else{
                                          echo "<i style='font-size:15px;'>";
                                          echo $updates;
                                          echo '</i>';
                                        }
                                    ?>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </table>
                        
                    </div>
                    
                </div> <!-- ================== END ================ -->
                @endif
                <div class="sss">
                    @if(($tite_page)=='Outbox')
                        @include('inventory::FileManagement._sentFile')
                    @elseif(($tite_page)=='Inbox')
                        @include('inventory::FileManagement._recFile')
                    @elseif(($tite_page)=='Dashboard')
                        @include('inventory::FileManagement._dashyBoardy')
                    @elseif(($tite_page)=='Reports')
                        @include('inventory::FileManagement._repos')
                    @else
                        @include('inventory::FileManagement._otherFile')
                    @endif
                </div>
            </div>
                    
        </section>
    </div>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#TRAckFiles").load(location.href+" #TRAckFiles>*",""); });

      function searchFunctions() {
          var input, filter, table, tr, td, i;
          input = document.getElementById("searchUL");
          filter = input.value.toUpperCase();
          table = document.getElementById("searchTBL");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) { if (td.innerHTML.toUpperCase().indexOf(filter) > -1) { tr[i].style.display = ""; } 
              else { tr[i].style.display = "none"; }
            }       
          }
        }
    </script>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <!-- <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.js"></script>
   <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.min.js"></script>
   <script src="{{asset('includes-jquery')}}/Gruntfile.js"></script>

@stop