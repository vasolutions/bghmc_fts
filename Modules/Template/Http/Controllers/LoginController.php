<?php

namespace Modules\Template\Http\Controllers;


use Modules\Setup\Init;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

use Modules\Template\Entities\UserCredentials;
use Modules\Template\Entities\GroupPermission as Permissions;
use Modules\Administrator\Entities\PersonnelModel as PM;
use Modules\Administrator\Entities\SecurityQModel as SQM;
use Illuminate\Support\Facades\DB;

// use Modules\Administrator\Entities\PersonnelModel as PM;

class LoginController extends Controller
{
     /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Login';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        return $this->data;
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(){
        return view('template::top-nav-pages.login',$this->setup());
    }

    public function check(Request $request){
        $field = 'emp_id';

        $validator = Validator::make($request->all(), [
            'bghmc_userelogin' => 'required',
            'bghmc_password' => 'required',
        ]);

        $userdata = [
            'emp_id' => $request['bghmc_userelogin'],
            'password' => $request['bghmc_password']
        ];

        if($validator->fails()){
                    $validator->getMessageBag()->add('validation', 'The username or password is incorrect.');
                     return redirect('/')->withErrors($validator);
            }elseif(Auth::attempt($userdata,$request['remember_me'])){
                $get_user = UserCredentials::select(
                                        'id',
                                        'emp_id',
                                        'password',
                                        'group_id',
                                        'isactive'
                                    )
                                    ->where($field , $request['bghmc_userelogin'])
                                    ->where('isactive', 1)
                                    ->first();
                        if($get_user){
                            Session::put(config('app.projcode').'user', $get_user);
                            Session::put( config('app.projcode').'permission', self::getUserGroupPermissions() );
                            Session::save();  
                            return $this->redirect_by_group();
                        }
                        else{
                            $validator->getMessageBag()->add('validation', 'User account is deactivated. Contact your admin.');
                            return redirect('/login')->withErrors($validator);
                        }
            }else{
                     $validator->getMessageBag()->add('validation', 'The username or password is incorrect.');
                     return redirect('/login')->withErrors($validator);
            }

         return view('template::top-nav-pages.login',$this->setup());
    }

    public function getUserGroupPermissions(){
        if (empty(Session::get(config('app.projcode').'user'))) {
            return redirect('/logout');
        }
        $group_permission = Permissions::where('id' , Session::get(config('app.projcode').'user')->group_id )->first();
        return $group_permission;
    }

     /**
     * redirect by group home page
     */
    public function redirect_by_group(){
         return redirect()->route(Session::get(config('app.projcode').'permission')->ugrp_homepage);
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function show_reset(){
        return view('template::top-nav-pages.reset_password',$this->setup());
    }
    public function checkID(Request $request){
        $data = $request->all();
        $PM = new PM;

        if($PM->validate($data, 'check_ID')){
            $has_id = DB::table('bghmc_employee_info')->WHERE('emp_id', $request->input('reset_id'))->first();
            if($has_id){
                $data['status'] = 1;
                $data['errors']['message'] = 'Account exists';
                $secu = DB::table('bghmc_emp_credentials_secu')->WHERE('emp_id', $request->input('reset_id'))->first();
                $data['secu'] = $secu;
                $data['empid'] = $request->input('reset_id');
            }
            else{
                $data['status'] = 2;
                $data['errors'] = "ID number does not exist";
            }
        }
        else{
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        return $data;
    }
    public function resetPassword(Request $request){
        $SQ = new SQM;
        
        if($SQ->resetPass($request) == true){
            $data['status'] = 1;
            $data['errors'] = "Successful. Please Log in using your default password to change into new password.";
        }
        else{
            $data['status'] = 0;
            $data['errors'] = "Incorrect answers";
        }
        return $data;
    }

}
