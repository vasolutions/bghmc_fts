<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Modules\Administrator\Entities\FileTypeModel as FTM;

class SystemLogsModel extends BaseModel
{
    protected $table = 'bghmc_system_logs';
    protected $fillable = ['logged_user', 'log', 'log_data_before', 'log_data_after', 'type'];

    public function setLog($data, $oldinfo, $logtype, $accnt_activity){
        $this->logged_user = Session::get('bghmcuser')->emp_id;
        if($logtype == 'new_account'){
            $this->log = 'New account "' . $data->input('fname') . ' ' . $data->input('lname') . '" with ID #' . $data->input('empid') . ' added.';
        }
        else if($logtype == 'update_account'){
            $this->log = 'Account of "' . $oldinfo->f_name . ' ' . $oldinfo->l_name . '" with ID ' . $oldinfo->emp_id . ' updated.';
        }
        else if($logtype == 'update_activity'){
            $this->log = 'Account of "' . $oldinfo->f_name . ' ' . $oldinfo->l_name . '" with ID ' . $oldinfo->emp_id . ' ' . $data->act .'d.';
            $oldinfo = serialize($accnt_activity);
        }
        else if($logtype == 'new_department'){
            $this->log = 'New department "' . $data->input('deptname') . '" added.';
        }
        else if($logtype == 'update_department'){
            $this->log = 'Department "' . $oldinfo->dept_name . '" updated.';
        }
        else if($logtype == 'new_position'){
            $this->log = 'New position "' . $data->input('posname') . '" added.';
        }
        else if($logtype == 'update_position'){
            $this->log = 'Position "' . $oldinfo->pos_name . '" updated.';
        }
        else if($logtype == 'new_filetype'){
            $this->log = 'New document type "' . $data->input('filetype') . '" added.';
        }
        else if($logtype == 'update_filetype'){
            $this->log = 'Document type "' . $oldinfo->filetype_name . '" updated.';
        }
        else if($logtype == 'send_message'){
            $filetype = $data[count($data) - 2];
            $title = $data[count($data) - 3];
            $hasFile = $data[count($data) - 1];
            
            $filetypename = DB::table('bghmc_filetypes')->where('filetype_id', $filetype)->first();
            
            unset($data[count($data) - 1]);
            unset($data[count($data) - 1]);
            unset($data[count($data) - 1]);
            $names = implode(' , ', $data);
            
            if($hasFile == 1){
                $this->log = 'Message Sent with title "' . $title . '" and file type of ' . $filetypename->filetype_name . ' to ' . $names . '. (HAS FILE ATTACHMENT).';
            }
            else if($hasFile == 0){
                $this->log = 'Message Sent with title "' . $title . '" and file type of ' . $filetypename->filetype_name . ' to ' . $names . '.';
            }
            $this->log_data_after = serialize($data);
        }
        else if($logtype == 'file_importance'){
            if($accnt_activity == 'make_important'){
                $this->log = 'File tracking #' . $data->tracking_no . ' marked as IMPORTANT';
            }
            else if($accnt_activity == 'make_not_important'){
                $this->log = 'File tracking #' . $data->tracking_no . ' marked as NOT IMPORTANT';
            } 
            $this->log_data_before = serialize($oldinfo);
            $this->log_data_after = serialize($data);
        }
        else if($logtype == 'file_acceptance'){
            $this->log = 'File tracking #' . $data->tracking_no . ' ACCEPTED.';
            $this->log_data_before = serialize($oldinfo);
            $this->log_data_after = serialize($data);
        }
        else if($logtype == 'download_f'){
            $this->log = 'File received from ' . $oldinfo . ' with file name of ' . $data . ' downloaded.';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'archive_destruct'){
            $this->log = 'File "' . $data[2] . '" with Tracking No. of ' . $data[0] . ' from ' . $data[1] . ' archived';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'archive_restore'){
            $this->log = 'File "' . $data[2] . '" with Tracking No. of ' . $data[0] . ' from ' . $data[1] . ' restored from File Archives';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'flag_important'){
            if($data[1] == 0){
                $this->log = 'File "' . $data[3] . '" with Tracking No. of ' . $data[0] . ' from ' . $data[2] . ' flagged as IMPORTANT';
                $this->log_data_after = '';
            }
            else{
                $this->log = 'File "' . $data[3] . '" with Tracking No. of ' . $data[0] . ' from ' . $data[2] . ' flagged as NOT IMPORTANT';
                $this->log_data_after = '';
            }
        }
        else if($logtype == 'forward'){
            $filename = $data[count($data) - 2];
            $tno = $data[count($data) - 1];

            unset($data[count($data) - 1]);
            unset($data[count($data) - 1]);
            $names = implode(' , ', $data);

            $this->log = 'File "' . $filename . '"with Tracking No. of ' . $tno . ' forwarded to ' . $names;
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'file_accept'){
            $this->log = 'File "' . $data[0] . '" with Tracking No. of ' . $data[1] . ' accepted.';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'change_route'){
            $this->log = 'File "' . $data[0] . '" with Tracking No. of ' . $data[1] . ' rerouted to ';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'reply'){
            $this->log = 'Replied to File "' . $data[0] . '" with Tracking No. of ' . $data[1] . '.';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'report_print'){
            $this->log = $data . ' printed in PDF.';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }
        else if($logtype == 'reset_account'){
            $this->log = 'User credentials of "' . $data->f_name . ' ' . $data->l_name . '" reset';
            $this->log_data_before = '';
            $this->log_data_after = '';
        }


        if($logtype != 'file_importance' && $logtype != 'file_acceptance' && 
        $logtype != 'download_f' && $logtype != 'archive_destruct' && 
        $logtype != 'archive_restore' && $logtype != 'forward' && $logtype != 'file_accept' && $logtype != 'change_route' &&
        $logtype != 'reply' && $logtype != 'report_print' && $logtype != 'reset_account'){
            $this->log_data_before = $oldinfo;
        }
        if($logtype != 'send_message' && $logtype != 'file_importance' && 
        $logtype != 'file_acceptance' && $logtype != 'download_f' && $logtype != 'archive_destruct' && 
        $logtype != 'archive_restore' && $logtype != 'flag_important' && $logtype != 'forward' && $logtype != 'file_accept' && 
        $logtype != 'change_route' && $logtype != 'reply' && $logtype != 'report_print' && $logtype != 'reset_account'){
            $this->log_data_after = serialize($data->all());
        }
        $this->type = $logtype;
        $this->save();
    }

    public function show_logs(){
        $loginfo = DB::table('bghmc_system_logs')->WHERE('logged_user', Session::get('bghmcuser')->emp_id)->where('created_at', '>=', \Carbon::today()->toDateString())->orderBy('created_at', 'desc')->get();
        // $this->data['logs'] = $loginfo;
        return $loginfo;
        // return view('template::admin-layouts.includes.controler-sidebar', compact('logs',));
    }
}
