<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetPasswordHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_emp_credentials_reset', function (Blueprint $table) {

            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            $table->increments('h_id');
            $table->primary('emp_id');
            $table->integer('emp_id');

            $table->timestamps();
            $table->softDeletes();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_filetypes');
    }
}
