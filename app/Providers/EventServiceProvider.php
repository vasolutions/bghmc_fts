<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'mdl.created' => [
            'Modules\Inventory\Entities\Handlers\Events\ModelEventHandler@created',
        ],
        'mdl.updated' => [
            'Modules\Inventory\Entities\Handlers\Events\ModelEventHandler@updated',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
