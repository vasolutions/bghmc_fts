<script type="text/javascript">
  $ReceiverMultiArrayID = [];
  $ReceiverMultiArrayNAME = [];
$('#MR').hide(); $('#PM').hide(); $('#selRecsWrapper').hide();
 // REFRESH HERE
$("#smOsr").change(function(){
  $ReceiverMultiArrayID = []; $ReceiverMultiArrayNAME = [];
  $(nWrapper).children('span').remove();
  $selectedType = $("#smOsr").val();
  
  if(($selectedType == 'none') || ($selectedType == 'sa')){ $('#MR').hide(); $('#PM').hide(); $('#selRecsWrapper').hide(); $('#pmHC').hide(); return;}
  if(($selectedType == 'sm') || ($selectedType == 'sr')){ $('#MR').show(); $('#PM').hide(); $('#selRecsWrapper').show(); $('#pmHC').show(); return;}
  if($selectedType == 'pm'){ $('#MR').hide(); $('#PM').show(); $('#selRecsWrapper').hide(); $('#pmHC').hide(); return;}

  return;
});    
      
  //SELECT NAMES
  var nWrapper = $("#nameWrapper");
  $("#SelectReceiverForMultiple").change(function(){
      
      $nameMultiID = $("#SelectReceiverForMultiple").val();
      $nameMultiName = $('#SelectReceiverForMultiple option:selected').attr('name');
      $arrayCount = $ReceiverMultiArrayID.length+1;
      if($nameMultiName == 'none'){return false; return;}
	  	if($.inArray($nameMultiID,$ReceiverMultiArrayID) != -1){
	  		$(".errorNOTIF").text('Duplicate receiver is not allowed. '); $(".errorNOTIF").show(); return;
	  	}else{
	  		$ReceiverMultiArrayID.push($nameMultiID);  
	  		if($arrayCount == 1){
			$(nWrapper).append('<span>'+ $nameMultiName +'<button name="'+$nameMultiID+'" id="rT" class="btn-link">REMOVE</button></span>');}
			else{
			$(nWrapper).append('<span><br>'+ $nameMultiName +'<button name="'+$nameMultiID+'" id="rT" class="btn-link">REMOVE</button></span>');}
	  	}
      
      document.getElementById("receiver").value = $ReceiverMultiArrayID;
      return false; return;
  });
  
  //REMOVE SPECIFIC NAME FROM THE RECEIVERS
  $removeItem = '';
  $(nWrapper).on("click","#rT", function(e){ 
  	$s = [];
  	e.preventDefault(); $(this).parent('span').remove();
  	$removeItem = $(this).attr("name");
  	arr = jQuery.grep($ReceiverMultiArrayID, function( a ) {return  a !== $removeItem;});

	document.getElementById("receiver").value = arr;
  });
  
  
  //SELECT NAMES FOR PRIVATE MESSAGE
  $("#SelectReceiverForPM").change(function(){
      $namePMID = $("#SelectReceiverForPM").val();
      $namePMName = $('#SelectReceiverForPM option:selected').attr('name');
      if($namePMName == 'none'){return false; return;}

      document.getElementById("receiver").value = $namePMID;
      return false; return;
  });

  // CLEAR SELECTED NAMES
  $("#CLEARSELCTEDNAMES").click(function(){
    // MULTIPLE RECEIVERS
    $ReceiverMultiArrayID = [];
    $ReceiverMultiArrayNAME = [];
    $('#SelectReceiverForMultiple option:selected').removeAttr('selected');
    // ROUTE RECEIVERS
    $numOfRecs = 0;
    $ReceiverRouteArrayID = [];
    $ReceiverRouteArrayNAME = [];
    $(".errorNOTIF").text("...");
    $(".errorNOTIF").hide();
    $(".NoteNOTIF").hide();
    $('#SelectNumRecss option:selected').removeAttr('selected');
    $('#SelectReceiverForRouteFile option:selected').removeAttr('selected');
    $('#SelRecType option:selected').removeAttr('selected');


    document.getElementById("receiver").value = $ReceiverRouteArrayID;
    //document.getElementById("ALLselectedRECS").value= $ReceiverRouteArrayNAME;
	$(nWrapper).children('span').remove();
    
    return false; return;
  });

  // CLEAR ALL UPON CANCEL
  $("#CancelCompose").click(function(){
    // MULTIPLE RECEIVERS
    $ReceiverMultiArrayID = [];
    $ReceiverMultiArrayNAME = [];
    $('#SelectReceiverForMultiple option:selected').removeAttr('selected');
    // ROUTE RECEIVERS
    $numOfRecs = 0;
    $ReceiverRouteArrayID = [];
    $ReceiverRouteArrayNAME = [];
    $(".errorNOTIF").text("...");
    $(".errorNOTIF").hide();
    $(".NoteNOTIF").hide();
    $('#SelectNumRecss option:selected').removeAttr('selected');
    $('#SelectReceiverForRouteFile option:selected').removeAttr('selected');
    $('#SelRecType option:selected').removeAttr('selected');
    $('#filea5').val(null); 
    $("#a2-style").val("");
    $("#MessageInput").val("");

    document.getElementById("receiver").value = $ReceiverRouteArrayID;
    //document.getElementById("ALLselectedRECS").value= $ReceiverRouteArrayNAME;
    $(nWrapper).children('span').remove();
    $(nWrapper).children('br').remove();

  });
  
  // FOR HARD COPY FILES AND NAMES
	$('#HardCopy').change(function(){
		if(this.checked) $('#hcNames').fadeIn('slow');
		else $('#hcNames').fadeOut('slow');
	});

	var wrapper = $(".inputWrapper"); //Fields wrapper
	$("#addDocName").click(function(e){
	  e.preventDefault();
	  $(wrapper).append('<div><input type="text" class="form-control textContent" placeholder="Document Name" style="max-width:50%;" name="docName[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
	});
	$(wrapper).on("click",".remove_field", function(e){ e.preventDefault(); $(this).parent('div').remove(); });
	
	
	
	
</script>