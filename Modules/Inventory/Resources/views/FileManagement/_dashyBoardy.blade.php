<script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
      $(document).ready(function(){
        setInterval(function(){
              $("#ACCPTREC").load(location.href+" #ACCPTREC>*","");
              $("#NOTIFICATIONSSTUFF").load(location.href+" #NOTIFICATIONSSTUFF>*","");
              $("#NOTIFICATIONSSTUFFALERT").show('slow');
              $("#FOoTERCTRS").load(location.href+" #FOoTERCTRS>*","");
        }, 3000);
      });
</script>

<table id="ACCPTREC" class="table table-striped table-bordered table-hover">
    <thead id="tableHeader">
        <td style="max-width:50px;"></td>
        <td style="max-width:135px;">TRACKING NUMBER</td>
        <td>FILE INFORMATION</td>
        <td colspan="2" style="max-width:200px;">FILE STATUS</td>
        
    </thead>
	<?php $counterPage = 0; ?>
    @foreach($_file as $s)
        <tbody style=" {{$s->ImpStyle}}">
	    @if($s->INVOLVED == 'TRUE')
	    @if($s->RecArchive != 'TRUE')
            @if($s->RECEIVERS == 'TRUE')
                <td valign="center" style="margin-top:20px; padding-top:20px; {{$s->updatedStatus}}">
                    <a href="{{route('SpecificFileReceiver',['track'=>$s->transNO,'rec'=>$userHere,'sender'=>$s->SenderDept.' | '.$s->SenderName]) }}" 
			class="btn stuff" style="padding: 0px; margin:0px;"><i class="fa fa-eye"></i>
			<span class="hoverstext">View File </span> </a> &nbsp;

			<?php 
			if($s->RecArchive == 'TRUE'){ ?>
				<a href="{{route('RestoreFiles', ['transNO'=>$s->transNO,'titles'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;">	
				<?php	echo "<i class='fa fa-undo'></i>";
					echo "<span class='hoverstext'>Restore Message</span>";
			} 
			else{ ?>
				<a href="{{route('ArchiveFiles', ['transNO'=>$s->transNO,'titles'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;">	
				<?php	echo "<i class='fa fa-trash'></i>";
                    			echo "<span class='hoverstext'>Delete Message</span>";
			}					
			?>
                </td>
                <td style="{{$s->updatedStatus}}">
                    <?php
			echo "<span style='font-size: 20px; font-weight: bolder;'>$s->transNO</span><br>
			<b>$s->docTYPE</b> &nbsp;";
			if($s->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}
			echo "<br><b>Date Received: </b>$s->SenderDateSENT";?>
                <td style="{{$s->updatedStatus}}">
                    <?php
			echo "<b>Sender: </b> $s->SenderDept | $s->SenderName <br>";
			echo "<b>Subject: </b>$s->SUBJ <br>";
			if($s->ROUTESTAT == 1){echo "<b>FILE IN A ROUTE</b><br>";}?>
			<a href= <?php 
				   if($s->attachNAME != "NO ATTACHMENT"){ $a = "Download File";?>
					"{{route('DownloadFile',['tno'=>$s->transNO,'attachName'=>$s->attachNAME,'sender'=>$s->sendID]) }}"
				   <?php }else{ $a = "No file to Download"?>
					"#NoDownloadFileAlert" data-toggle='modal'
				   <?php }?>
			class='btn btn-link stuff' style='color:#000; margin-left:0px; padding-left:0px;'>
			<span><b><i class='fa fa-paperclip'></i>Attachment: </b>{{$s->attachNAME}}</span>
			<span class='hoverstext'>{{$a}}</span></a>
                </td>

                <td valign="center" style="{{$s->updatedStatus}}">
		   <div id="statuses">
			<?php
		  		if($s->recTYPE == "ALL DEPARTMENTS"){
					echo "No Hard Documents";
		  		}elseif($s->recTYPE != "PERSONAL MESSAGE"){
				   foreach(unserialize($s->fileSTATS) as $a){
			  		foreach($a as $b){
					   echo "<b>".$b['EmpDEPT']."</b><br>";
					   if($s->hardCOPY == 'NO'){echo "&nbsp; No Hard Copy Sent <br>";}
					   else{echo "&nbsp; Hard Documents: <br>";
						foreach (array_keys($b['Documents']) as $c){
						   echo "&nbsp;  &nbsp; ".$c.": ";
						   if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; Not Accepted<br>";}
						   else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
						
						}
					   }
			  		}
			 	   }
		  		}else{echo "No Hard Documents for Personal Message";}	
			?>
		   </div>
            	</td>
            @endif
            @endif
	    @if($s->SENDERS == 'TRUE')
		<td valign="center" style="margin-top:20px; padding-top:20px; width:8%; {{$s->updatedStatus}}">
                {{-- VIEW FILE --}}
                	<a href="{{route('SpecificFileSender',['transNO'=>$s->transNO])}}" class="btn stuff" style="padding: 0px; margin:0px;">
                    	<i class="fa fa-eye"></i>
                    	<span class="hoverstext">View File Status</span>
                	</a> &nbsp;
                {{-- MARK FILE AS IMPORTANT OR NOT IMPORTANT --}}
                	@if($s->IMP == 0)
                    		<a href="{{route('MarkImp',['transNO'=>$s->transNO]) }}" class="btn stuff" style="padding: 0px; margin:0px;">
                        		<i class="fa fa-flag-o"></i>
                        		<span class="hoverstext">Mark File as Important</span>
                    		</a> &nbsp;
                	@else
                    		<a href="{{route('MarkImp',['transNO'=>$s->transNO]) }}" class="btn stuff" style="padding: 0px; margin:0px;">
                        		<i class="fa fa-flag"></i>
                        		<span class="hoverstext">Mark File as Not Important</span>
                   		</a> &nbsp;
                	@endif
                {{-- DELETE FILE --}}
                    	<?php 
			if($s->SenderDateArchived != "N"){ ?>
				<a href="{{route('RestoreFiles', ['transNO'=>$s->transNO,'titles'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;">	
				<?php	echo "<i class='fa fa-undo'></i>";
					echo "<span class='hoverstext'>Restore Message</span>";
			} 
			else{ ?>
				<a href="{{route('ArchiveFiles', ['transNO'=>$s->transNO,'titles'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;">	
				<?php	echo "<i class='fa fa-trash'></i>";
                    			echo "<span class='hoverstext'>Delete Message</span>";
			}					
			?>
                	</a> &nbsp;
            	</td>
            	{{-- TRACKING NUMBER --}}
            	<td style="{{$s->updatedStatus}}">
                	<span style="font-size: 20px; font-weight: bolder;">{{$s->transNO}} </span><br>
			<b>{{$s->docTYPE}}</b> &nbsp;
			<?php if($s->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";} ?>
			<br><b>Date Sent:</b> {{$s->SenderDateSENT}}
	    	</td>
		{{-- FILE INFORMATION --}}
            	<td style="{{$s->updatedStatus}}">
                	<span style="font-size: 15px;"><b>Subject: </b>{{strtoupper($s->SUBJ)}} </span><br>						
			<?php
				if($s->recTYPE == "PERSONAL MESSAGE"){echo "<b>Sent as ".$s->recTYPE."</b><br><b>Receiver:</b> ".unserialize($s->fileSTATS);}
				else if(strpos($s->recTYPE, "FORWARDED") !== false){echo $s->recTYPE."</b>";}
				else{echo "<b>Sent to ".$s->recTYPE."</b><br>";}
			?>
			<a href= <?php 
				   if($s->attachNAME != "NO ATTACHMENT"){ $a = "Download File";?>
					"{{route('DownloadFile',['tno'=>$s->transNO,'attachName'=>$s->attachNAME,'sender'=>$s->sendID]) }}"
				   <?php }else{ $a = "No file to Download"?>
					"#NoDownloadFileAlert" data-toggle='modal'
				   <?php }?>
			class='btn btn-link stuff' style='color:#000; margin-left:0px; padding-left:0px;'>
			<span><b><i class='fa fa-paperclip'></i>Attachment: </b>{{$s->attachNAME}}</span>
			<span class='hoverstext'>{{$a}}</span></a>
		</td>
		{{-- FILE STATUS --}}
            	<td style="{{$s->updatedStatus}}">
		   <div id="statuses">
			<?php
		  		if($s->recTYPE == "ALL DEPARTMENTS"){
					echo "No Hard Documents";
		  		}elseif($s->recTYPE != "PERSONAL MESSAGE"){
				   foreach(unserialize($s->fileSTATS) as $a){
			  		foreach($a as $b){
					   echo "<b>".$b['EmpDEPT']."</b><br>";
					   if($s->hardCOPY == 'NO'){echo "&nbsp; No Hard Copy Sent <br>";}
					   else{echo "&nbsp; Hard Documents: <br>";
						foreach (array_keys($b['Documents']) as $c){
						   echo "&nbsp;  &nbsp; ".$c.": ";
						   if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; Not Accepted<br>";}
						   else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
						
						}
					   }
			  		}
			 	   }
		  		}else{echo "No Hard Documents for Personal Message";}	
			?>
		   </div>
            	</td>
	    
	    @endif
	    @endif
        </tbody>





    @endforeach
</table>
<?php if ($inboxCounter == 0){echo "<div id='FOoTERCTRS'> <h3><center>-- EMPTY FOLDER--</center></h3> </div>";} ?>
