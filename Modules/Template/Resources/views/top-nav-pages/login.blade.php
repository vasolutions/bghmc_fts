@extends('template::top-nav.default')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <img src="{{asset('img')}}/site-logo.jpg">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
     @if(count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif


    <form action="" method="post">
    {{csrf_field()}}
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="bghmc_userelogin" placeholder="ID" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="bghmc_password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
        <div class="form-group">
          <input type="checkbox"><span class="icheck">Remember Me</span> <br> 
         </div>
         <div class="form-group">
          <button type="submit" class="btn btn-bts-login btn-function btn-flat">Sign In</button>
        </div>       
    </form>
    <a href="{{route('accnt.resetView')}}">I forgot my password</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@stop