<?php

namespace Modules\Administrator\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Administrator\Entities\PersonnelModel as PM;
use Modules\Administrator\Entities\DepartmentsModel as DeptM;
use Modules\Administrator\Entities\PositionsModel as PosM;
use Modules\Template\Entities\UserCredentials as Creds;
use Modules\Administrator\Entities\AccountActivityModel as AAM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Modules\Administrator\Entities\SecurityQModel as SQM;
use Modules\Administrator\Entities\PasswordHistoryModel as PHM;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Carbon;

class PersonnelModel extends BaseModel
{
    protected $table = 'bghmc_employee_info';
    protected $fillable = ['emp_id', 'password', 'f_name', 'l_name', 'accnt_type', 'pos_id', 'dept_id'];
    protected $primaryKey = 'emp_id';

    public function setInfo($request){
        
        $grpid = 2;

        $get_department = DB::table('bghmc_departments')->WHERE('dept_name', $request->input('drp-dept'))->first();
        $get_position = DB::table('bghmc_positions')->WHERE('pos_name', $request->input('drp-pos'))->first();
        
        $PM = new PM;
        $PM->emp_id = $request->input('empid'); //coming from form names in view
        $PM->password = Hash::make(1);
        $PM->f_name = $request->input('fname');
        $PM->l_name = $request->input('lname');
        $PM->accnt_type = $request->input('rd-accnttype');
        $PM->pos_id = $get_position->pos_id;
        $PM->dept_id = $get_department->dept_id;
        $PM->isactive = 1;
        $PM->save();

        if($request->input('rd-accnttype') == "administrator"){$grpid = 1;}

        $Creds = new Creds;
        $Creds->emp_id = $request->input('empid');
        $Creds->password = Hash::make(1);
        $Creds->group_id = $grpid;
        $Creds->is_approved = 1;
        $Creds->isactive = 1;
        $Creds->save();

        $L = new SLM;
        $L->setLog($request, '', 'new_account', null);
    }

    public function updateInfo($request){

        $grpid = 2;
        
        $get_department = DB::table('bghmc_departments')->WHERE('dept_name', $request->input('drp-dept'))->first();
        $get_position = DB::table('bghmc_positions')->WHERE('pos_name', $request->input('drp-pos'))->first();

        $PMinfo = PM::find($request->input('e_id'));
        $PM = PM::find($request->input('e_id'));
        $PM->f_name = $request->input('fname');
        $PM->l_name = $request->input('lname');
        $PM->accnt_type = $request->input('accnt_type');
        $PM->pos_id = $get_position->pos_id;
        $PM->dept_id = $get_department->dept_id;
        $PM->isactive = 1;
        $PM->save();

        $L = new SLM;
        $L->setLog($request, $PMinfo, 'update_account', null);
    }

    public function updateActivityInfo($request){
        $act = 1;

        if($request->act == "deactivate"){$act = 0;}
        
        $PMinfo = PM::find($request->input('e_id'));
        $PM = PM::find($request->input('e_id'));
        $PM->isactive = $act;
        $PM->save();

        $Creds = Creds::find($request->input('e_id'));
        $Creds->isactive = $act;
        $Creds->save();

        
        $AAMinfo = DB::table('bghmc_account_activity')->WHERE('emp_id', $request->input('e_id'))->orderBy('created_at', 'desc')->first();
        $AAM = new AAM;
        $AAM->emp_id = $request->input('e_id');
        $AAM->deact_reason = $request->input('reason');
        $AAM->isactive = $act;
        $AAM->auth_by = $request->input('auth_by');
        $AAM->save();

        $L = new SLM;
        $L->setLog($request, $PMinfo, 'update_activity', $AAMinfo);
    }

    public function changePassword($request){
        $PM = PM::find($request->input('empid'));
        $PM->password = Hash::make($request->password);
        $PM->save();

        $Creds = Creds::find($request->input('empid'));
        $Creds->password = Hash::make($request->password);
        $Creds->save();

        if($request->input('hasCurrent') != 1){
            $SQ = new SQM;
            $SQ->emp_id = $request->input('empid');
            $SQ->question_1 = $request->input('secuques_1');
            // $SQ->answer_1 = Hash::make($request->input('secuans_1'));
            $SQ->answer_1 = $request->input('secuans_1');
            $SQ->question_2 = $request->input('secuques_2');
            // $SQ->answer_2 = Hash::make($request->input('secuans_2'));
            $SQ->answer_2 = $request->input('secuans_2');
            $SQ->iscurrent = 1;
            $SQ->save();
        }
    }

    public function isAdmin($request){
        $PM = PM::find($request->input('empid'));
        return $PM->accnt_type;
    }

    public function adminChangePassword($request){
        $PM = PM::find($request->input('e_id'));
        $PM->password = Hash::make(1);
        $PM->save();

        $Creds = Creds::find($request->input('e_id'));
        $Creds->password = Hash::make(1);
        $Creds->save();

        $PHM = new PHM;
        $PHM->emp_id = $request->input('e_id');
        $PHM->save();

        $SQM = SQM::find($request->input('e_id'));
        $SQM->iscurrent = 0;
        $SQM->save();

        $userdetails = DB::table('bghmc_employee_info')->WHERE('emp_id', $request->input('e_id'))->first();
        $L = new SLM;
        $L->setLog($userdetails, '', 'reset_account', '');

        return true;
    }
}
