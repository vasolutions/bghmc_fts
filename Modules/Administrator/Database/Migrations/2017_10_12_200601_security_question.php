<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class SecurityQuestion extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('bghmc_emp_credentials_secu', function (Blueprint $table) {

            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';

            

            $table->increments('id');



            $table->integer('emp_id');

            $table->string('question_1');

            $table->string('answer_1',255);

            $table->string('question_2');

            $table->string('answer_2',255);

            $table->boolean('iscurrent',255);

                    

            $table->timestamps();

            $table->softDeletes();



            // $table->index(['dept_name'],'departments'); //keywords for db para mabilis mahanap

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('bghmc_emp_credentials_secu');

    }

}

