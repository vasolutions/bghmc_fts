 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">Document Types</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <button type="button" class="btn btn-new btn-lg pull-right" data-toggle="modal" data-target="#mdl-new-ft">NEW DOCUMENT TYPE&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-user-plus"></i> </button>
                </div>
                <div class="box-body">
                    <table id="tbl_filetypes" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <!-- <td>File Type ID</td> -->
                                <td>Document Type</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($info as $i)
                            <tr>
                                <!-- <td>{{$i->filetype_id}}</td> -->
                                <td>{{$i->filetype_name}}</td>
                                <td>
                                    <a href="" class="btn btn-function btn-edit" data-toggle="modal" data-target="#mdl-edit-file_{{$i->filetype_id}}"><i class="glyphicon glyphicon-pencil"></i></a>

                                    <!--Modal to edit department  -->
                                    <div id="mdl-edit-file_{{$i->filetype_id}}" class="modal fade table-header mdl-arch">
                                        <div class="modal-dialog modal-md">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <div class="text-center">
                                                        <span class="title">Update Document Type Name</span>
                                                    </div>
                                                </div>
                                                <div class="modal-body form-group-pad-body">
                                                    <div id="status"></div>
                                                        <form class="thing-form" role="form" name="editfiletypenameform_{{$i->filetype_id}}" id="editfiletypenameform_{{$i->filetype_id}}" method="post" >
                                                        {{ csrf_field() }}
                                                        <center>
                                                        <input type="hidden" name="filetypeid" value="{{$i->filetype_id}}">
                                                            <div class="form-group has-feedback">
                                                                <div class="form-group">
                                                                    <label class="control-label float-left" for="filetype">Document Type</label>
                                                                    <div class="form-group form-group-pad">
                                                                        <input type="text" class="form-control form-control-pad" name="filetype" value="{{$i->filetype_name}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </center>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                                                                <div class="col-md-3">
                                                                    <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendFileTypeInfo('{{route('admin.updatefiletype')}}', '#editfiletypenameform_{{$i->filetype_id}}');">Update</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                             </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

<!--MODAL NEW FILE TYPE-->
   <div id="mdl-new-ft" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span class="title">New Document Type</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="newfiletypeform" id="newfiletypeform" method="post" >
                    {{ csrf_field() }}
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <label class="control-label float-left" for="filetype">Document Type</label>
                                <div class="form-group form-group-pad">
                                    <input type="text" class="form-control form-control-pad" name="filetype">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-new btn-lg" onclick="$(this).sendFileTypeInfo('{{route('admin.newfiletype')}}', '#newfiletypeform');">Add</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div> 
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
$(document).ready( function() {
			var table = $('#tbl_filetypes').dataTable( {
			"language": {
			"lengthMenu": 'Show <select style="padding: 5px;">'+
			'<option value="10">10</option>'+
			'<option value="20">20</option>'+
			'<option value="30">30</option>'+
			'<option value="40">40</option>'+
			'<option value="50">50</option>'+
			'<option value="-1">All</option>'+
			'</select>'
			}
            });
        });
        $('.modal').on('hidden.bs.modal', function () {
            $(".modal-body #status").html('<div></div>');
            $('#newfiletypeform').clearForm();
        });
        $.fn.clearForm = function() {
            return this.each(function() {
              var type = this.type, tag = this.tagName.toLowerCase();
              if (tag == 'form')
                return $(':input',this).clearForm();
              if (type == 'text' || type == 'password' || tag == 'textarea')
                this.value = '';
              else if (type == 'checkbox' || type == 'radio')
                this.checked = false;
              else if (tag == 'select')
                this.selectedIndex = -1;
            });
          };
        $.fn.sendFileTypeInfo = function(rt,fr){
            $.ajax({
                type : 'POST',
                url : rt, //from routes
                data: $(fr).serialize(),
                // dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    var errors = '';
                    if(data['status']==0){
                        for(var key in data['errors']){
                            errors += data['errors'][key]+'<br />';
                        }
                        $(' .modal-body  #status').html('<div class="alert alert-danger alert-dismissible">'+errors+'</div>').fadeIn();
                    }else{
                        $(' .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4>'+data['errors']['message']+'</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        location.reload();
                    }
            
                }
            });
        };
    </script>
@stop