 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
@stop

@section('pagename')
    <span style="font-size: 25px;">File Archives</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <table id="tbl_positions" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>File ID</td>
                                <td>Tracking No.</td>
                                <td>File Name</td>
                                <td>File Type</td>
                                <td>Created</td>
                                <td>Updated</td>
                                <td>Attached File</td>
                                <td class="hidden-xs"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <a href="" class="btn btn-function btn-restore" data-toggle="modal" data-target=""><i class="glyphicon glyphicon-open-file"></i>&nbsp;&nbsp;&nbsp;&nbsp;Restore</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
@stop