<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Modules\Inventory\Entities\FileArchiveModel;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
class FlagController extends Controller

{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null){   
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;}
// === MARK FILE AS IMPRTANT OR NOT ===
    public function MarkFileImportant($TNO){
        $flaglog = [];
        $userHere=Auth::user()->emp_id;
        $ctDATEDATE = Carbon::now()->toDayDateTimeString();

        $a0 = TM::where('transNO',$TNO)->first();
	$ctDate = Carbon::now()->toDayDateTimeString();
        $b = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$b0 = $b->l_name." of ".$b->dept_name. ' FLAGGED the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;

        array_push($flaglog, $a0->transNO);
        array_push($flaglog, $a0->IMP);
        array_push($flaglog, TM::where('transNO',$TNO)->join('bghmc_employee_info', 'bghmc_employee_info.emp_id', 'bghmc_transactions.sendID' )->first()->f_name . ' ' . TM::where('transNO',$TNO)->join('bghmc_employee_info', 'bghmc_employee_info.emp_id', 'bghmc_transactions.sendID' )->first()->l_name);
        array_push($flaglog, $a0->attachNAME);

        // ______MARK FILES IMPORTANT_______ 1 = important; 0 = not important
            if($a0->sendID == $userHere){
                if($a0->IMP == 0){
                    TM::where('transNO',$TNO)->update(['IMP'=>'1','trackingSTATS'=>$b1,'updateStatus'=>Carbon::now()]);
                    $L = new SLM;
                    $L->setLog($flaglog, 'not important', 'flag_important', null);
                    
                    return redirect()->back()->with('successMSG','File marked as important!');
                }else{
                    TM::where('transNO',$TNO)->update(['IMP'=>'0','trackingSTATS'=>$b1,'updateStatus'=>Carbon::now()]);
                    $L = new SLM;
                    $L->setLog($flaglog, 'important', 'flag_important', null);
                    return redirect()->back()->with('successMSG','File marked as not important!');
                }
            }else{
                return redirect()->back()->with('errorMSG','Sorry! You cannot change the file as important. Only the sender can.');
            }

    }


   
}
