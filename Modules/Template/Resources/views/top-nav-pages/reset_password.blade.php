@extends('template::top-nav.default') 
@section('content')
    <div class="login-box" style="margin: 0 auto;">
        <center>
            <h3>Enter your ID number.</h3>
        </center>
        <div class="login-box-body">
            <form name="checkIDform" id="checkIDform">
                {{csrf_field()}}
                <div id="status"></div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="reset_id" placeholder="ID Number">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group">
                    <a onClick="$(this).checkID();" class="btn btn-bts-login btn-function btn-flat">Submit</a>
                </div>
                <hr>
            </form>
            <form name="resetPasswordform" id="resetPasswordform">
                <center>
                    <h3>Answer your security questions</h3>
                </center>
                <br>
                {{csrf_field()}}
                <div id="status"></div>
                <input type="hidden" id="empid_secu" name="empid_secu" value="">
                <div class="form-group has-feedback">
                    Q1: <span id="q1"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="secuans_1" placeholder="Answer for question 1">
                </div>
                <br>
                <div class="form-group has-feedback">
                    Q2: <span id="q2"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="secuans_2" placeholder="Answer for question 2">
                </div>
                <hr>
                <div class="form-group">
                    <a onClick="$(this).resetPassword();" class="btn btn-bts-login btn-function btn-flat">Reset to default password</a>
                </div>
            </form>
        </div>
    </div>
@stop 
@section('plugins-script')
    <script>
        $.fn.checkID = function () {
            $.ajax({
                type: 'POST',
                url: "{{route('accnt.checkID')}}", //from routes
                data: $('#checkIDform').serialize(),
                error: function () {
                    alert('error');
                },
                success: function (data) {
                    var errors = '';
                    if (data['status'] == 0) {
                        for (var key in data['errors']) {
                            errors += data['errors'][key] + '<br />';
                        }
                        $(' #checkIDform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i>' +errors + '</div>').fadeIn();
                    } 
                    else if(data['status'] == 2){
                        $(' #checkIDform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i>' + data['errors'] + '</div>').fadeIn();
                    }
                    else {
                        $(' #checkIDform  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i>' +data['errors']['message'] + '</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        $('#q1').text(data['secu']['question_1']);
                        $('#q2').text(data['secu']['question_2']);
                        $('#empid_secu').val(data['empid']);
                        $('#resetPasswordform').show();
                    }
                }
            });
        };
        $.fn.resetPassword = function(){
            $.ajax({
                type: 'POST',
                url: "{{route('accnt.resetPass')}}",
                data: $('#resetPasswordform').serialize(),
                error: function(){
                    alert('error');
                },
                success: function(data){
                    if (data['status'] == 0) {
                        $(' #checkIDform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i>' + data['errors'] + '</div>').fadeIn();
                    } 
                    else {
                        $(' #checkIDform  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i>' +data['errors'] + '</h4></div>').fadeIn().delay(1500).fadeOut(1000);
                        setTimeout(function() {
                            window.location = "/";
                        }, 3000);
                    }
                }
            });
        }
    </script>
    @stop