<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\AllFilesModel;
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Entities\FileMngmtModel;
use Modules\Inventory\Entities\FileAttachModel;
use Modules\Inventory\Entities\FileArchiveModel;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use Modules\Administrator\Entities\SystemLogsModel as SLM;
class DLController extends Controller

{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null){   
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;}

    public function show($id){}
    public function DownloadFile($tno, $attachName,$sender){
        $path = storage_path('app/FileAttachments/'.$sender.'/'.$attachName);
	$a0 = TM::where('transNO',$tno)->first();
        $L = new SLM;
        $L->setLog($attachName, $sender, 'download_f', null);
	$b = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$ctDate = Carbon::now()->toDayDateTimeString();
	$b0 = $b->l_name." of ".$b->dept_name. ' downloaded the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;
	TM::where('transNO',$tno)->update(['trackingSTATS'=>$b1]);
        return response()->download($path);
    }

   
}// END OF EVERYTHING
