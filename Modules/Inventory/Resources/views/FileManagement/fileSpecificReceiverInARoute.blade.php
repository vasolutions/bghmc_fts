 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
@stop

@section('pagename')
    <span style="font-size: 25px;">{{$TRACKINGNUMBER}} -- {{$FILETYPENAME}} --</span>
@stop

@section('content')
<script type="text/javascript">
      $(document).ready(function(){
        setInterval(function(){
              $("#Replies").load(location.href+" #Replies>*","");
              $("#ROUTEINFO").load(location.href+" #ROUTEINFO>*","");
              $("#SPECIFICFILERECROUTE").load(location.href+" #SPECIFICFILERECROUTE>*","");
              $("#IMPORTANTMESSAGE").load(location.href+" #IMPORTANTMESSAGE>*","");
        }, 3000);
      });
</script>
    <div class="content-wrapper">
      @include('inventory::includes._ALL') 
      <section class="content">
        <div class="box">
          @if(Session::has('message'))
              <div class="alert alert-success alertSession">
                <span onclick="$('.alertSession').hide();" style="position:absolute; cursor: pointer; right:1em; top:1em; width:1.1em; height:1.1em;">&times;</span> {{Session::get('message')}}
              </div>
            @endif
          <div class="box-header" id="SPECIFICFILERECROUTE">
            <!-- BACK BUTTON -->
            <button class="backings btn btn-primary" style="margin-right:2px;">
              <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Back
            </button>&nbsp;

            <!-- CREATE MESSAGE BUTTON-->
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file" style="margin-right:2px;">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Compose
            </button>&nbsp;
      
            <!-- ACCEPT FILE BUTTON-->
            @if(count($n2)>1)
            
              <select class="ACCEPTFILEROUTE" style="max-width:15%; min-width: 15%; min-height:33px;" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                <option >FILE STATUS</option>
                @foreach($n2 as $n3)
                  @if($n1[$n3] == 1)
                    <option disabled>{{$n3+1}} - Already Accepted</option>
                  @else
                    <option value="{{route('ReceiverAccept', ['track'=>$TRACKINGNUMBER,'rec'=>$RECID,'acpt'=>$index0,'ANum'=>$n3])}}" 
                        <?php
                        $numbershit = $n3 + 1;
                          if($n3 != 0){
                            $a6 = $n3 - 1;
                            
                            if($n1[$a6] == 0){ 
                              echo "disabled> $numbershit - Cannot Accept Yet</option>";
                            }else{
                              echo "> $numbershit - Set as accepted</option>";
                            }
                          }else{
                            if($n1[0] == 0){echo "> $numbershit - Set as accepted</option>"; }

                          }
                        ?>  
                  @endif
                @endforeach
              </select>
              
              
            
            @else
            <a href="{{$AcptNotifs}}" data-toggle="modal" class="btn btn-info" style="margin-right:2px;">
                <span class="fa fa-file"> &nbsp; {{$AcptTips}}</span>
            </a>&nbsp;
            @endif

            <!-- DELETE BUTTON -->
            <a class="btn btn-primary pull-right" href="{{$deleteROUTE}}" data-toggle="modal">
              <span class="glyphicon glyphicon-trash">&nbsp;{{$deleteMSG}}</span>
            </a>

          </div>


          <!-- START OF BODY CONTENT -->
          <hr>
          <div class="box-body">
            <div class="col-md-8" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
             <h1>{{$FILETYPENAME}}</h1>
             <h4>{{$SENDER}}</h4>
             <h4>{{$DATEREC}}</h4>
             <h5>{{$SUBJECT}}</h5>
             <div id="IMPORTANTMESSAGE"><h5 id="" style="color:red;">-- {{$IMP}} --</h5></div>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <h4 style="font-weight:bolder;">{{$MSG}}</h4>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <a href="{{$AttachTipModal}}" data-toggle="modal"
                class="btn btn-link stuff" style="color:#000; margin-left:0px; padding-left:0px;">
                <span><h4>{{$ATTACHMENT}}</h4></span>
                <span class="hoverstext">{{$AttachTip}}</span>    
              </a><br>
              
              <span style="font-weight:bolder; font-size:18px">ROUTE INFORMATION: </span><button class="btn btn-link" name="CHANGEROUTE" data-toggle="modal" data-target="#changeRouteModal">CHANGE</button>
              <br>
              <div id="ROUTEINFO">
                @foreach($routeRECS as $sREC)
                  &emsp; <span style="font-size:14px">{{$sREC}}</span> &emsp;
                  <br>
                @endforeach
              </div>
              
              

            </div>
            <div class="col-md-4" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc; ">
              <br><br>
              <h4 style="border-bottom: 2px solid #000;">REPLIES</h4>
              <div id= 'Replies' style="height: 300px; max-height: 400px;">
                <?php
                  $messages = nl2br($THREADS);
                    if($messages == ''){
                      echo "<i style='font-size:15px;' class='replyThisOne'> NO REPLIES </i>";
                    }else{
                      echo "<i style='font-size:15px;' class='replyThisOne'>";
                      echo $messages;
                      echo '</i>';
                    }
                ?>
              </div>
              <form file='true' type="form" enctype="multipart/form-data" method="post" 
              action="{{route('ReplyShit', ['track'=>$TRACKINGNUMBER,'rec'=>$RECID,'acpt'=>$AcptTips,'ANum'=>$ArrayNumber]) }}">
              {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                        <input type="text" class="form-control textContent" name="thread" placeholder="Send a Reply">
                        <button class="btn btn-primary btn-block" type="submit" name="SENDREPLY">SEND REPLY</button>
                    </div>
                </div>

              </form>
            </div>
          </div>

        </div>
    

<!-- === CALLS MODALS ===  -->
@include('inventory::FileManagement.MODALS')
<!-- ===== MODAL FOR ACCEPtING the FILE CONFIrMAtiON ==== -->
        <div class="modal fade" id="AcptNotif" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- HI THERE! -</h3></center>
                <center style="font-weight: bolder;">PLEASE MAKE SURE THAT YOU HAVE ACCEPTED THE FILE. THANKS </center>
              </div>
              <div class="modal-footer">
                <a href="{{route('ReceiverAccept', ['track'=>$TRACKINGNUMBER,'rec'=>$RECID,'acpt'=>$AcptTips,'ANum'=>$ArrayNumber]) }}" class="btn btn-primary">ACCEPT</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR DOWNLOADING FILE ALERT ==== -->
        <div class="modal fade" id="DownloadFileAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DOWNLOAD -</h3></center>
                <center style="font-weight: bolder;">PLEASE CONFIRM DOWNLOAD OF FILE</center>
              </div>
              <div class="modal-footer">
                <a href="{{route('DownloadFile',['tno'=>$TRACKINGNUMBER,'attachName'=>$ss->ATTACHNAME,'sender'=>$ss->SEND_ID]) }}" class="btn btn-primary">DOWNLOAD</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR DELETING FILE ALERT ==== -->
        <div class="modal fade" id="CANdeleteAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <center><h3>- HI THERE! -</h3></center>
              </div>
              <div class="modal-body">
                <center style="font-weight: bolder;">PLEASE MAKE SURE THAT YOU HAVE ACCEPTED THE FILE. THANKS </center>
              </div>
              <div class="modal-footer">
                <a href="{{route('RouteArchive',['track'=>$TRACKINGNUMBER,'rec'=>$RECID,'ANum'=>$ArrayNumber,'titlePage'=>'none']) }}" class="btn btn-primary">ACCEPT</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR CHANGING A ROUTE OF A  FILE ALERT ==== -->
        <div class="modal fade" id="changeRouteModal" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- ROUTE CHANGE -</h3></center>
                <i style="font-size:12px; color:red;">NOTE: Changing it to wrong routes could reflect badly to the credibility of the company and the efficiency of the employees. </i>
                <form method="post" action="{{route('changeROUTErec',['tno'=>$TRACKINGNUMBER,'rec'=>$RECID])}}" id="CHANGEROUTEFORM">
                  {{ csrf_field() }}
                    <select class="form-control textContent" name="USERCHANGEPOSITION" id="USERCHANGEPOSITION">
                      <option value="none">SELECT POSITION</option>
                      @foreach($routeRECS as $sREC)
                        <option style="font-size:14px" value="{{substr($sREC,0,1)}}">Before {{$sREC}}</option>
                      @endforeach
                        <option style="font-size:14px" value="last">After {{$routeRECS[$f1CTR]}}</option>
                    </select>
                    
                    <br>
                    <select class="form-control textContent" name="USERTOCHANGEROUTE" id="USERTOCHANGEROUTE">
                      <option value="none">SELECT NEW RECEIVER</option>
                      @foreach($emps2 as $es)
                        <option value="{{$es->emp_id}}">
                          &emsp; {{$es->dept_name}} &emsp; -- {{$es->f_name}} {{$es->l_name}} &emsp; ID: {{$es->emp_id}}
                        </option>
                      @endforeach  
                    </select>
                    <div class="modal-footer">
                      <button class="btn btn-primary" type="submit" >Save Route</button>
                      <button class="btn btn-info" id="NOTIFYSENDER" type="button">NOTIFY SENDER</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">CANCEL</button>
                    </div>

                </form>

                <form method="post" action="{{route('changeROUTErecNotif',['tno'=>$TRACKINGNUMBER,'rec'=>$RECID])}}" id="NOTIFYROUTEFORM">
                  {{ csrf_field() }} <br>
                  <div class="form-group">
                      <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                          <input type="text" class="form-control textContent" name="Commentaries" placeholder="Enter Commentaries">
                      </div>
                      <div class="modal-footer">
                          <button class="btn btn-primary" type="submit">SEND QUERIES</button>
                          <button class="btn btn-danger" id="CANCELNOTIFY" type="button" data-dismiss="modal">CANCEL</button>  
                      </div>
                  </div>
              </form>
              </div>
              
            </div>
          </div>
        </div>


<script type="text/javascript">
  $("#NOTIFYROUTEFORM").hide();
  $("#NOTIFYSENDER").click(function(){
    $("#CHANGEROUTEFORM").hide('slow');
    $("#NOTIFYROUTEFORM").show('slow');
  });

  $("#CANCELNOTIFY").click(function(){
    $("#CHANGEROUTEFORM").show('slow');
    $("#NOTIFYROUTEFORM").hide('slow');
  });
</script>
        
      </section>
    </div>




@stop

@section('plugins-script')

    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <!-- <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script> -->
@stop

