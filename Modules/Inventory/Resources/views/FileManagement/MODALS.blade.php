<!-- ===== MODAL FOR IMPORTANT CHANGE ALERT ==== -->
<div class="modal fade" id="ImpAlert" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>IMPORTANT FILE ALERT!</h3></center>
        <center style="font-weight: bolder;">You can only change the importance of the file if you are the sender.</center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<!-- ===== MODAL FOR CANNOT DOWNLOAD FILE ALERT ==== -->
<div class="modal fade" id="NoDownloadFileAlert" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>ATTACHMENT ALERT!</h3></center>
        <center style="font-weight: bolder;">The sender did not upload any file.</center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- ===== MODAL FOR FILE ALREADY ACCEPTED ALERT ==== -->
<div class="modal fade" id="AlreadyAcptNotif" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>- FILE ACCEPTED -</h3></center>
        <center style="font-weight: bolder;">You already accepted the file. THANKS </center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ===== MODAL FOR NO FILES RECEIVED ==== -->
<div class="modal fade" id="NoFileToAccept" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>- YOU HAVE NO FILE TO ACCEPT -</h3></center>
        <center style="font-weight: bolder;">There are no documents or files sent your way. This is an infomative message from the sender. </center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- ===== MODAL FOR THAT CANNOT BE ACCEPTED DUE TO ROUTES ==== -->
<div class="modal fade" id="AcptNotifNOTyet" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>- DELETE FILE ALERT! -</h3></center>
        <center style="font-weight: bolder;">You cannot accept the file yet because the previous receiver has not yet accepted the file. <br>Please Check the <b>ROUTE INFORMATION</b> to know the receiver before you.</center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- ===== MODAL FOR CANNOT DELETE FILE ALERT ==== -->
<div class="modal fade" id="CANNOTdeleteAlert" type="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <center><h3>- DELETE FILE ALERT! -</h3></center>
        <center style="font-weight: bolder;">You cannot delete the file if you have not accepted the file yet. THANK YOU! </center>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>