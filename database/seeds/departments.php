<?php

use Illuminate\Database\Seeder;

class departments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create fake data to fill database
        $faker = Faker\Factory::create();
        $dt_created = Carbon\Carbon::now();

        $limit = 48;
        $depts = array(
            "Medical Center Chief",
            "Quality Management Office",
            "Professional Education and Training Office",
            "Integrated Hospital Operation Management Office",
            "Legal Services",
            "Hems",
            "Anesthesiology",
            "Family Medicine",
            "Internal Medicine",
            "Dental",
            "Obstetrics & Gynecology",
            "Ophthalmology",
            "ORL -HNS",
            "Orthopedics & Rehabilitation Medicine",
            "Pediatrics",
            "Psychiatry",
            "Surgery",
            "Out Patient Department",
            "Emergency Room",
            "Reproductive Health Care Unit",
            "Operating Room",
            "Pay Services",
            "WCPU",
            "Cancer Center",
            "Pathology",
            "Imaging",
            "Pharmacy",
            "Health Information Management Office",
            "Nutrition and Deitetics Office",
            "Medical social Works Services Office",
            "Nursing Quality Standards and Policy Unit",
            "Center Supply Room (CSR)",
            "Medical",
            "Orthopedics",
            "Hemodialysis",
            "Delivery Room Complex",
            "Contagious Disease Unit",
            "Pay/PR/SPR Suite",
            "Procurement Management Office",
            "Materials Management Office",
            "Security Services Office",
            "Human Resource Management Office",
            "Engineering & Facilities Management Office",
            "General Services Unit",
            "Cash Operations Office",
            "Accounting Office",
            "Billing and Claims Office",
            "Budget Office"
        );

        for ($i = 0; $i < $limit; $i++) {
            DB::table('bghmc_departments')->insert([
                'dept_id' => '6800' . ($i+1),
                'dept_name' => $depts[$i],
                'created_at' => $dt_created->toDateTimeString(),
            ]);
        }
    }
}
