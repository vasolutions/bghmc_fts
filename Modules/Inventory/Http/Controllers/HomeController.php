<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\FilesModel as FM;
use Modules\Inventory\Entities\FileAttachModel as FAtM;
use Modules\Inventory\Entities\FilesStatsModel as FSM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class HomeController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        //SENT FILE COUNTER
        $sentCTR = FM::where('SEND_ID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;
        $userHere=Auth::user()->emp_id;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;

        return $this->data;

    }

    public function homePage(){   
        $userHere=Auth::user()->emp_id;

        




        $tite_page = 'MY HOME';
        return view('inventory::FileManagement.homePage',$this->setup(),compact('tite_page'));
    }


// ================== END ==================
}