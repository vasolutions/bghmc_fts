@extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
@stop

@section('pagename')
    <span style="font-size: 25px;">Subject: {{$a0->SUBJ}}</span>
@stop

@section('content')
<script type="text/javascript">
      $(document).ready(function(){
        setInterval(function(){
              $("#Replies").load(location.href+" #Replies>*","");
              $("#SPECIFICFILERECROUTE").load(location.href+" #SPECIFICFILERECROUTE>*","");
        }, 5000);
      });
</script>
    <div class="content-wrapper">
      @include('inventory::includes._ALL') 
      <section class="content">
        <div class="box">
          <div class="box-header" id="SPECIFICFILERECROUTE">
            @if(Session::has('message'))
              <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
            @endif
            
            <!-- BACK BUTTON -->
            <button class="backings btn btn-primary" style="margin-right:2px;">
              <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Back
            </button>&nbsp;

            <!-- CREATE MESSAGE BUTTON-->
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file" style="margin-right:2px;">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Compose
            </button>&nbsp;

	    <!-- FORWARD MESSAGE BUTTON-->
		<?php if($a0->ROUTESTAT == 0){ ?>
			<button type="button" class="btn btn-new" data-toggle="modal" data-target="#FileReceiverForward" style="margin-right:2px; ">
              		<i class="glyphicon glyphicon-pencil"></i> &nbsp; Forward
            		</button>&nbsp;
		<?php } ?>
            
            <!-- DELETE BUTTON -->
	    @if($a0->recTYPE != "PERSONAL MESSAGE")
            @if($a0->SenderDateArchived != "N")
		<a href="{{route('RestoreFiles', ['transNO'=>$a0->transNO,'titles'=>'InboxS']) }}" class="btn btn-primary pull-right">
			<i class='fa fa-undo'>&nbsp; Restore</i>
	    @else
		<a href="{{route('ArchiveFiles', ['transNO'=>$a0->transNO,'titles'=>'InboxS']) }}" class="btn btn-primary pull-right">	
			<i class='fa fa-trash'>&nbsp; Archive</i>
            @endif </a> &nbsp;  
	    @else
            @if($a0->RecArchive == "TRUE")
		<a href="{{route('RestoreFiles', ['transNO'=>$a0->transNO,'titles'=>'InboxS']) }}" class="btn btn-primary pull-right">
			<i class='fa fa-undo'>&nbsp; Restore</i>
	    @else
		<a href="{{route('ArchiveFiles', ['transNO'=>$a0->transNO,'titles'=>'InboxS']) }}" class="btn btn-primary pull-right">	
			<i class='fa fa-trash'>&nbsp; Archive</i>
            @endif </a> &nbsp;  
	    @endif	   

          </div>


          <!-- START OF BODY CONTENT -->
          <hr>
          <div class="box-body">
            <div class="col-md-8" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
            	<span style="font-size:20px;"><b>Document Type: </b>{{$a0->docTYPE}}</span><br>
            	<span style="font-size:20px;"><b>Subject: </b>{{strtoupper($a0->SUBJ)}}</span><br>
            	<span style="font-size:17px;"><b>Sender:</b> {{$sender}}</span><br>
                <span style="font-size:17px;"><b>Tracking Number:</b> {{$a0->transNO}}</span><br> 
                <span style="font-size:17px;"><b>Date Received:</b> {{$a0->SenderDateSENT}}</span><br>
                <span style="font-size:17px;"><?php if($a0->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}?></span><br>
  		<span style="font-size:17px;"><?php if($a0->ROUTESTAT == 1){echo "<b style='color:red;'>ROUTE MESSAGE &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}?></span><br>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <h4 style="font-weight:bolder;">{{$a0->MSG}}</h4>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <a href="{{$AttachTipModal}}" data-toggle="modal"
                class="btn btn-link stuff" style="color:#000; margin-left:0px; padding-left:0px;">
                <span><h4>{{$a0->attachNAME}}</h4></span>
                <span class="hoverstext">{{$AttachTip}}</span>    
              </a><br>
              
             <!-- RECEIVERS INFROMATION -->
                <span style="font-weight:bolder; font-size:18px">RECEIVER INFORMATION: </span>
			<?php
			   $ctrDocs = 0;
			   $ctrs = 0;
			   if($a0->ROUTESTAT != '0'){echo "<a href='#ReceiverChangeRouteModal' data-toggle='modal'>Change Route</a>";}
				echo "<br><br>";
			   if($a0->recTYPE == "ALL DEPARTMENTS"){
				echo "<b>Sent to all Departments</b><br>No Hard Documents";
		 	   }elseif($a0->recTYPE != "PERSONAL MESSAGE"){
				foreach(unserialize($a0->fileSTATS) as $a){
			  	  foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){echo "<b style='font-size:larger; text-decoration:underline;'>".$b['EmpDEPT']."</b><br>";}
					else{echo "<span style='font-size:larger;'>".$b['EmpDEPT']."</span><br>";}
					
					
					if($a0->hardCOPY == 'NO'){echo "&nbsp; <b>No Hard Copy Sent </b><br>";}
					else{echo "&nbsp; <b>Hard Documents:</b> <br>";
					   foreach (array_keys($b['Documents']) as $c){
						echo "&nbsp;  &nbsp; ".$c.": ";
						if($b['Documents'][$c] == "N" && $b['dateRouteForwarded'] != "N"){
							$ctrDocs++;
							echo "&nbsp;  &nbsp;"; 
							if($b['EmpDEPT'] == $depts->dept_name){ ?>
							   <a href="{{route('AcceptFiles', ['track'=>$a0->transNO,'docNAME'=>$c,'dept'=>$depts->dept_name]) }}" 
							      class="btn btn-link stuff" style="color:#000; margin:0px; padding:0px;">
                					      <span><i style='color:red;' class="fa fa-exclamation-circle"> Not Accepted</i></i></span>
                					      <span class="hoverstext">ACCEPT FILE</span>    
              						   </a>
							<?php }else{ 
									echo "<span><i style='color:red;'> Not Accepted</i></i></span>";
								
						} echo "<br>";}
						elseif($b['Documents'][$c] == "N" && $b['dateRouteForwarded'] == "N"){
							echo "<span><i style='color:red;'> Not Accepted</i></i></span><br>";
					   	}else{
							echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";
						}
					   }
				  	} // END OF ELSE
					if($b['dateRouteForwarded'] != "N" && $ctrDocs == 0 && $b['EmpDEPT'] == $depts->dept_name){
						$over = count(unserialize($a0->fileSTATS)) - 1;
						if($b['dateAccepted'] == "N" && $over != $ctrs){
							echo "&nbsp;  &nbsp;";?>
							<b><a href="{{route('forwardDocs',['track'=>$a0->transNO,'deptName'=>$depts->dept_name,'deptID'=>$depts->dept_id,'NUM'=>$ctrs]) }}"><i class='fa fa-share'></i>FORWARD FILE/S</a></b>

						<?php } }
					if($b['dateRouteForwarded'] != "N" && $b['dateAccepted'] != "N"){
						echo "&nbsp;  &nbsp; <b>File forwarded: ". $b['dateRouteForwarded']."</b><br>";
						
					}
					
			  	   }// END OF FOREACH $a as $b
				   
				
				 echo "<br>";
//				    $ctrForward++;
				    $ctrs++;
			    	} // END OF FOREACH UNSERIALIZE
		  	   }else{echo "No Hard Documents for Personal Message";}	
			   
			?>

                <!-- END OF RECEIVERS INFORMATION -->
           
            </div>
            <div class="col-md-4" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
              <br><br>
              <h4 style="border-bottom: 2px solid #000;">REPLIES</h4>
              <div id= 'RepliesSender' style="height: 270px; max-height: 400px;">
                <?php
                  $messages = nl2br($a0->threadMSG);
                    if($messages == ''){
                      echo "<i style='font-size:15px;' class='replyThisOne'> NO REPLIES </i>";
                    }else{
                      echo "<i style='font-size:15px;' class='replyThisOne'>";
                      echo $messages;
                      echo '</i>';
                    }
                ?>
              </div>

              <form file='true' type="form" enctype="multipart/form-data" method="post" 
              action="{{route('ReplyShit', ['track'=>$a0->transNO]) }}">
              {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                        <input type="text" class="form-control textContent" name="thread" placeholder="Send a Reply">
                        <button class="btn btn-primary btn-block" type="submit" name="SENDREPLY">SEND REPLY</button>
                    </div>
                </div>

              </form>
            </div>
          </div>

        </div>
    

<!-- === CALLS MODALS ===  -->
@include('inventory::FileManagement.MODALS')
<!-- ===== MODAL FOR DOWNLOADING FILE ALERT ==== -->
        <div class="modal fade" id="DownloadFileAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DOWNLOAD -</h3></center>
                <center style="font-weight: bolder;">PLEASE CONFIRM DOWNLOAD OF FILE</center>
              </div>
              <div class="modal-footer">
                <a href="{{route('DownloadFile',['tno'=>$a0->transNO,'attachName'=>$a0->attachNAME,'sender'=>$a0->sendID]) }}" class="btn btn-primary">DOWNLOAD</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>
@if($a0->hardCOPY != 'NO')
@if($a0->ROUTESTAT == 1)
<!-- ===== MODAL FOR CHANGING A ROUTE OF A  FILE ALERT ==== -->
        <div class="modal fade" id="ReceiverChangeRouteModal" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- ROUTE CHANGE -</h3></center>
                <i style="font-size:12px; color:red;">NOTE: Changing it to wrong routes could reflect badly to the credibility of the company and the efficiency of the employees. </i>
                <form method="post" id="formChangeRoute" action="{{route('SendChangeRoute',['tno'=>$a0->transNO,'sender'=>$a0->sendID])}}" id="CHANGEROUTEFORM">
                  {{ csrf_field() }}
                  <select id="SelectActionForChange" class="form-control textContent" name="SelectActionForChange">
                    <option value="none">CHOOSE ACTION</option>
                    <option value="add">ADD RECEIVER</option>
                    <option value="change">CHANGE RECEIVER</option>
                    <option value="remove">REMOVE RECEIVER</option>
                  </select><br>
                    <div id="AddOption" style="display: none;">
                      <select class="form-control textContent" name="USERCHANGEPOSITION" id="USERCHANGEPOSITION">
                        <option value="none">SELECT POSITION</option>
			<?php $deptCTR = 0; ?>
                        @foreach($DEPTnames as $sREC12)
                          <option style="font-size:14px" value="<?php echo $deptCTR; ?>">Before {{$sREC12}}</option>
			  <?php $deptCTR++; ?>
                        @endforeach
                          <option style="font-size:14px" value="last">After {{end($DEPTnames)}}</option>
                      </select><br>
                      <select class="form-control textContent" name="USERTOCHANGEROUTE" id="USERTOCHANGEROUTE">
                        <option value="none">SELECT NEW RECEIVER</option>
                        @foreach($deptRecs as $es)
                          <option value="{{$es}}" name="{{$es}}" >{{$es}}</option>
                        @endforeach  
                      </select>
                    </div>
                    <div id="ChangeOption" style="display: none;">
                        <select id="SelectRecForChange" class="form-control textContent" name="UserChangesHerePosition">
                          <option value="none">SELECT RECEIVER TO CHANGE</option>
			  <?php $deptCTR = 0; ?>
                          @foreach($DEPTnames as $sREC12)
                            <option style="font-size:14px" value="<?php echo $deptCTR; ?>">{{$sREC12}}</option>
			    <?php $deptCTR++; ?>
                          @endforeach
                        </select><br>
                        <select id="SelectNewRec" class="form-control textContent" name="userchangeing">
                          <option value="none">SELECT NEW RECEIVER</option>
                          @foreach($deptRecs as $es)
                          	<option value="{{$es}}" name="{{$es}}" >{{$es}}</option>
                          @endforeach  
                        </select>
                    </div>
                    <div id="RemoveOption" style="display: none;">
                        <select id="SelectRecForRemove" class="form-control textContent" name="UserRemove">
                          <option value="none">SELECT RECEIVER TO REMOVE</option>
                          <?php $deptCTR = 0; ?>
                          @foreach($DEPTnames as $sREC12)
                          	<option style="font-size:14px" value="<?php echo $deptCTR; ?>">{{$sREC12}}</option>
			  	<?php $deptCTR++; ?>
                          @endforeach
                        </select>
                    </div>
                    
                    <div class="modal-footer">
                      <button class="btn btn-primary" type="button" id="saveChanges" >Save Changes</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">CANCEL</button>
                    </div>

                    <script type="text/javascript">
                      $recsINFOS="";
                      $("#SelectActionForChange").change(function(){
                        $z0 = $("#SelectActionForChange").val();
                        if($z0 == "change"){
                          $("#ChangeOption").show('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").hide('slow');
                        }else if($z0 == "add"){
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").show('slow');
                          $("#RemoveOption").hide('slow');
                        }else if($z0 == "remove"){
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").show('slow');
                        }else{
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").hide('slow');
                        }
                      });
                     
                      $("#saveChanges").click(function(){
                        $x0 = $("#SelectActionForChange").val();
                        $x1 = $("#USERTOCHANGEROUTE").val();$x2 = $("#USERCHANGEPOSITION").val();
                        $x3 = $("#SelectRecForChange").val();$x4 = $("#SelectNewRec").val();
                        $x5 = $("#SelectRecForRemove").val();
                        if($x0 == 'none'){alert("NO CHANGES CHOSEN"); return;}
                        else if($x0 == 'add'){
                            if($x1 == 'none'){alert("NO CHANGES CHOSEN");return;}
                            if($x2 == 'none'){alert("NO CHANGES CHOSEN");return;}
                        }else if($x0 == 'change'){
                            if($x3 == 'none'){alert("NO CHANGES CHOSEN");return;}
                            if($x4 == 'none'){alert("NO CHANGES CHOSEN");return;}
                        }else if($x0 == 'remove'){ if($x5 == 'none'){alert("NO CHANGES CHOSEN");return;} }

                        $("#formChangeRoute").submit();
                      });
                    </script>

                </form>
              </div>
              
            </div>
          </div>
        </div>
@endif
@endif

        
        
        

<!-- ===== MODAL FOR FORWARDING A FILE ==== -->
        <div class="modal fade" id="FileReceiverForward" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE FORWARD -</h3></center>
                <center style="font-weight: bolder;">PLEASE BE SURE WHO YOU SEND THE FILE TO. THANKS </center><br>
                  <i style="font-size:12px; color:red;" class="errorFNOTIF" id="hideThis"></i>
		
                <form method="post" action="{{route('forwardByRec', ['track'=>$a0->transNO,'forwRec'=>$a0->sendID]) }}">
                  {{ csrf_field() }}
                  <select id="SelectReceiverForForwardFile" class="form-control textContent">
                    <option value="none" name="none">SELECT RECEIVER</option>
                    @foreach($deptRecs as $emps1)
                      <option value="{{array_search ($emps1, $deptRecs)}}" name="{{$emps1}}" 
                        id="OptionReceiverForForwardFile">{{$emps1}}
                      </option>
                    @endforeach
                  </select>
                  <hr style="width:50%;">
                  <div id="selRecsFWrapper">
			<span id="suckME">SELECTED RECEIVERS:</span>
			<textarea type="text" id="ALLselectedFRECS" placeholder="SELECTED RECEIVERS" name="ALLselectedFRECS" class="form-control textContent" value="FNAMES" disabled required style="display:none;"></textarea>
			<div style="border:3px solid #c0c0c0; width:100%; z-index:9; background:#fff; border-radius:4px; overflow:auto; max-height:100px;min-height:100px;">
				<div id ="nameFWrapper" style="max-height:100px;min-height:100px;font-size:15px;"></div>
			</div>
			<button type="button" class="btn btn-primary pull-right" id="CLEARSELCTEDFNAMES">CLEAR SELECTED</button><br>
		</div>
		
                  <input type="text" name="AllReceiversForward" id="AllReceiversForward" style="display:none;" required>
                  <input type="text" name="AllReceiversForwardName" id="AllReceiversForwardName" style="display:none;" required>
                    <script type="text/javascript">
                    $("#AllReceiversForward").hide();
                      $RecArrID = []; $RecArrNAME = [];
                      var nWrapper = $("#nameFWrapper");
                      $("#SelectReceiverForForwardFile").change(function(){
                        $RecID = $("#SelectReceiverForForwardFile").val();
                        $RecName = $('#SelectReceiverForForwardFile option:selected').text();
                        $arrayCount = $RecArrID.length+1;
                        if($RecName == 'none'){return false; return;}
                        if($RecID == "none"){ return; }
                        
	                        if($.inArray($RecID,$RecArrID) != -1){
			  		$(".errorFNOTIF").text('Duplicate receiver is not allowed. '); $(".errorFNOTIF").show(); return;
			  	}else{
			  		$RecArrID.push($RecID);
		                        $RecArrNAME.push($RecName);
			  		if($arrayCount == 1){
					$(nWrapper).append('<span>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">Delete</button></span>');}
					else{
					$(nWrapper).append('<span><br>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">Delete</button></span>');}
			  	}
                        
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                      });
                      
                      //REMOVE SPECIFIC NAME FROM THE RECEIVERS
				  $removeItem = '';
				  $(nWrapper).on("click","#rTF", function(e){ 
				  	$s = [];
				  	e.preventDefault(); $(this).parent('span').remove();
				  	$removeItem = $(this).attr("name");
				  	arr = jQuery.grep($RecArrID, function( a ) {return  a !== $removeItem;});
				
					document.getElementById("AllReceiversForward").value = arr;
					document.getElementById("AllReceiversForwardName").value = arr;
				  });
				
                      
                      $("#CLEARSELCTEDFNAMES").click(function(){
                        $RecArrID = []; $RecArrNAME = [];
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                        $('#SelectReceiverForForwardFile option:selected').removeAttr('selected');
                        $(".errorNOTIF").text(".");
			$(".errorNOTIF").hide();
			$(nWrapper).children('span').remove();
			$(nWrapper).children('br').remove();
                        return;
                      });
                    </script>
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" id="submitForward" type="submit">FORWARD</button>
                <button class="btn btn-danger" data-dismiss="modal" type="button">CANCEL</button>
              </div>
              </form>
            </div>
          </div>
        </div>

        
      </section>
    </div>




@stop

@section('plugins-script')
        <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script>
    <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>

@stop

