 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
@stop

@section('pagename')
    <span style="font-size: 25px;">Subject: {{strtoupper($a0->SUBJ)}} </span>
@stop

@section('content')
<script type="text/javascript">
      // $(document).ready(function(){
        setInterval(function(){ $("#SpecificHeader").load(location.href+" #SpecificHeader>*","");}, 5000);
        setInterval(function(){
              $(".RepliesSender").load(location.href+" .ReRepliesSenderplies>*","");
              $(".receiverInfo").load(location.href+" .receiverInfo>*","");
        }, 1000);
      // });
</script>
    <div class="content-wrapper">
      @include('inventory::includes._ALL') 
      <section class="content">
        <div class="box">
           @if(Session::has('successMSG'))
              <div class="boxAlert alertSessions" id="sMSG">
                </span> {{Session::get('successMSG')}}<br>
                <button class="btn btn-new pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
              </div>
            @endif
            @if(Session::has('errorMSG'))
              <div class="boxAlert alertSessions" id="eMSG">
                </span> {{Session::get('successMSG')}}<br>
                <button class="btn btn-danger pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
              </div>
            @endif
          <div class="box-header" id="SpecificHeader">
            <!-- BACK BUTTON -->
            <button class="backings btn btn-primary" style="margin-right:2px;">
              <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Back
            </button>&nbsp;

            <!-- CREATE MESSAGE BUTTON-->
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file" style="margin-right:2px;">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Compose
            </button>&nbsp;
      
            <!-- FORWARD MESSAGE BUTTON-->
            @if($a0->ROUTESTAT == 0)
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#FileSenderForward" style="margin-right:2px;">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Forward
            </button>&nbsp;
            @endif
            <!-- MARK FILE AS IMPORTANT-->
            <a href={{route('MarkImp',['transNO'=>$a0->transNO]) }} data-toggle="modal" class="btn btn-info" style="margin-right:2px;">
                <span class="{{$FlagLogo}}"> &nbsp; {{$FlagWord}}</span>
            </a>&nbsp;
            
            <!-- DELETE BUTTON -->
            <a class="btn btn-primary pull-right" href="#CANdeleteAlert" data-toggle="modal">
              <span class="glyphicon glyphicon-trash">&nbsp;Archive</span>
            </a>

          </div>


          <!-- START OF BODY CONTENT -->
          <div class="box-body">
            <div class="col-md-8" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
                <span style="font-size:20px;"><b>Document Type:</b> {{$a0->docTYPE}}</span><br>
                <span style="font-size:17px;"><b>Subject:</b> {{strtoupper($a0->SUBJ)}}</span><br>
                <span style="font-size:17px;"><b>Tracking Number:</b> {{$a0->transNO}}</span><br> 
                <span style="font-size:17px;"><b>Date Sent:</b> {{$a0->SenderDateSENT}}</span><br>
                <span style="font-size:17px;"><b>{{$IMP}}</b></span><br>
                <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
                <h4 style="font-weight:bolder;">{{$a0->MSG}}</h4>
                <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
                <a href="{{$AttachTipModal}}" data-toggle="modal" 
                  class="btn btn-link stuff" style="color:#000; margin-left:0px; padding-left:0px;">
                  <span><h4>Attachment: {{$a0->attachNAME}}</h4></span>
                  <span class="hoverstext">{{$AttachTip}}</span>    
                </a><br>

                <!-- RECEIVERS INFROMATION -->
                <span style="font-weight:bolder; font-size:18px">RECEIVER INFORMATION: </span>
			<?php
			   if($a0->ROUTESTAT != '0'){echo "<a href='#SenderchangeRouteModal' data-toggle='modal'>Change Route</a>";}
				echo "<br>";
			   if($a0->recTYPE == "ALL DEPARTMENTS"){
				echo "<b>Sent to all Departments</b><br>No Hard Documents";
		 	   }elseif($a0->recTYPE != "PERSONAL MESSAGE"){
				foreach(unserialize($a0->fileSTATS) as $a){
			  	  foreach($a as $b){
					echo $b['EmpDEPT']."<br>";
					if($a0->hardCOPY == 'NO'){echo "&nbsp; No Hard Copy Sent <br>";}
					else{echo "&nbsp; Hard Documents: <br>";
					   foreach (array_keys($b['Documents']) as $c){
						echo "&nbsp;  &nbsp; ".$c.": ";
						if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; Not Accepted<br>";}
					   	else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
					   }
				  	}
			  	   }
			    	}
		  	   }else{echo "No Hard Documents for Personal Message";}	
			   
			?>

                <!-- END OF RECEIVERS INFORMATION -->


            </div>
            <div class="col-md-4" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
              <br><br>
              <h4 style="border-bottom: 2px solid #000;">REPLIES</h4>
              <div id= 'RepliesSender' style="height: 270px; max-height: 400px;">
                <?php
                  $messages = nl2br($a0->threadMSG);
                    if($messages == ''){
                      echo "<i style='font-size:15px;' class='replyThisOne'> NO REPLIES </i>";
                    }else{
                      echo "<i style='font-size:15px;' class='replyThisOne'>";
                      echo $messages;
                      echo '</i>';
                    }
                ?>
              </div>
              <form file='true' type="form" enctype="multipart/form-data" method="post" 
              action="{{route('ReplyShit', ['track'=>$a0->transNO]) }}">
              {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                        <input type="text" class="form-control textContent" name="thread" placeholder="Send a Reply">
                        <button class="btn btn-primary btn-block" type="submit" >SEND REPLY</button>
                    </div>
                </div>
              </form>
            </div>
          </div>

        </div>
    

<!-- === CALLS MODALS ===  -->
@include('inventory::FileManagement.MODALS')
<!-- ===== MODAL FOR DELETING FILE ALERT ==== -->
        <div class="modal fade" id="CANdeleteAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DELETE -</h3></center>
                <center style="font-weight: bolder;">ARE YOU SURE YOU WANT TO DELETE THIS FILE? </center>
              </div>
              <div class="modal-footer">
                <a href="{{route('senderArchive', ['tno'=>$a0->TNO,'sender'=>$a0->sendID]) }}" class="btn btn-primary">DELETE</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR FLAGGING A FILE ==== -->
        <div class="modal fade" id="FlagAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE IMPORTANCE -</h3></center>
                <center style="font-weight: bolder;">{{$FlagModal}}</center>
              </div>
              <div class="modal-footer">
                <a href="{{route('MarkImp', ['track'=>$a0->TNO]) }}" class="btn btn-primary">YES</a>
                <button class="btn btn-danger" data-dismiss="modal">NO</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR DOWNLOADING FILE ALERT ==== -->
        <div class="modal fade" id="DownloadFileAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DOWNLOAD -</h3></center>
                <center style="font-weight: bolder;">PLEASE CONFIRM DOWNLOAD OF FILE</center>
              </div>
              <div class="modal-footer">
                <a href="{{route('DownloadFile',['tno'=>$a0->transNO,'attachName'=>$a0->attachNAME,'sender'=>$a0->sendID]) }}" class="btn btn-primary">DOWNLOAD</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR FORWARDING A FILE ==== -->
        <div class="modal fade" id="FileSenderForward" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE FORWARD -</h3></center>
                <center style="font-weight: bolder;">PLEASE BE SURE WHO YOU SEND THE FILE TO. THANKS </center><br>
                  <i style="font-size:12px; color:red;" class="errorFNOTIF" id="hideThis"></i>
		
                <form method="post" action="{{route('forwardByRec', ['track'=>$a0->transNO,'forwRec'=>$a0->sendID]) }}">
                  {{ csrf_field() }}
                  <select id="SelectReceiverForForwardFile" class="form-control textContent">
                    <option value="none" name="none">SELECT RECEIVER</option>
                    @foreach($deptRecs as $emps13)
                      <option value="{{array_search ($emps13, $deptRecs)}}" name="{{$emps13}}"
                        id="OptionReceiverForForwardFile">{{$emps13}}
                      </option>
                    @endforeach
                  </select>
                  <hr style="width:50%;">
                  <div id="selRecsFWrapper">
			<span id="suckME">SELECTED RECEIVERS:</span>
			<textarea type="text" id="ALLselectedFRECS" placeholder="SELECTED RECEIVERS" name="ALLselectedFRECS" class="form-control textContent" value="FNAMES" disabled required style="display:none;"></textarea>
			<div style="border:3px solid #c0c0c0; width:100%; z-index:9; background:#fff; border-radius:4px; overflow:auto; max-height:100px;min-height:100px;">
				<div id ="nameFWrapper" style="max-height:100px;min-height:100px;font-size:15px;"></div>
			</div>
			<button type="button" class="btn btn-primary pull-right" id="CLEARSELCTEDFNAMES">CLEAR SELECTED</button><br>
		</div>
		
                  <input type="text" name="AllReceiversForward" id="AllReceiversForward" style="display:none;" required>
                  <input type="text" name="AllReceiversForwardName" id="AllReceiversForwardName" style="display:none;" required>
                    <script type="text/javascript">
                    $("#AllReceiversForward").hide();
                      $RecArrID = []; $RecArrNAME = [];
                      var nWrapper = $("#nameFWrapper");
                      $("#SelectReceiverForForwardFile").change(function(){
                        $RecID = $("#SelectReceiverForForwardFile").val();
                        $RecName = $('#SelectReceiverForForwardFile option:selected').text();
                        $arrayCount = $RecArrID.length+1;
                        if($RecName == 'none'){return false; return;}
                        if($RecID == "none"){ return; }
                        
	                        if($.inArray($RecID,$RecArrID) != -1){
			  		$(".errorFNOTIF").text('Duplicate receiver is not allowed. '); $(".errorFNOTIF").show(); return;
			  	}else{
			  		$RecArrID.push($RecID);
		                        $RecArrNAME.push($RecName);
			  		if($arrayCount == 1){
					$(nWrapper).append('<span>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">REMOVE</button></span>');}
					else{
					$(nWrapper).append('<span><br>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">REMOVE</button></span>');}
			  	}
                        
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                      });
                      
                      //REMOVE SPECIFIC NAME FROM THE RECEIVERS
				  $removeItem = '';
				  $(nWrapper).on("click","#rTF", function(e){ 
				  	$s = [];
				  	e.preventDefault(); $(this).parent('span').remove();
				  	$removeItem = $(this).attr("name");
				  	arr = jQuery.grep($RecArrID, function( a ) {return  a !== $removeItem;});
				
					document.getElementById("AllReceiversForward").value = arr;
					document.getElementById("AllReceiversForwardName").value = arr;
				  });
				
                      
                      $("#CLEARSELCTEDFNAMES").click(function(){
                        $RecArrID = []; $RecArrNAME = [];
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                        $('#SelectReceiverForForwardFile option:selected').removeAttr('selected');
                        $(".errorNOTIF").text(".");
			$(".errorNOTIF").hide();
			$(nWrapper).children('span').remove();
			$(nWrapper).children('br').remove();
                        return;
                      });
                    </script>
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" id="submitForward" type="submit">FORWARD</button>
                <button class="btn btn-danger" data-dismiss="modal" type="button">CANCEL</button>
              </div>
              </form>
            </div>
          </div>
        </div>
@if($a0->hardCOPY != 'NO')
@if($a0->ROUTESTAT == 2)
<!-- ===== MODAL FOR CHANGING A ROUTE OF A  FILE ALERT ==== -->
        <div class="modal fade" id="SenderchangeRouteModal" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- ROUTE CHANGE -</h3></center>
                <i style="font-size:12px; color:red;">NOTE: Changing it to wrong routes could reflect badly to the credibility of the company and the efficiency of the employees. </i>
                <form method="post" id="formChangeRoute" action="{{route('SendChangeRoute',['tno'=>$a0->transNO,'sender'=>$a0->sendID])}}" id="CHANGEROUTEFORM">
                  {{ csrf_field() }}
                  <select id="SelectActionForChange" class="form-control textContent" name="SelectActionForChange">
                    <option value="none">CHOOSE ACTION</option>
                    <option value="add">ADD RECEIVER</option>
                    <option value="change">CHANGE RECEIVER</option>
                    <option value="remove">REMOVE RECEIVER</option>
                  </select><br>
                    <div id="AddOption" style="display: none;">
                      <select class="form-control textContent" name="USERCHANGEPOSITION" id="USERCHANGEPOSITION">
                        <option value="none">SELECT POSITION</option>
                        @foreach($DEPTnames as $sREC12)
                          <option style="font-size:14px" value="{{$sREC12}}">Before {{$sREC12}}</option>
                        @endforeach
                          <option style="font-size:14px" value="last">After {{end($DEPTnames)}}</option>
                      </select><br>
                      <select class="form-control textContent" name="USERTOCHANGEROUTE" id="USERTOCHANGEROUTE">
                        <option value="none">SELECT NEW RECEIVER</option>
                        @foreach($deptRecs as $es)
                          <option value="{{array_search ($es, $deptRecs)}}" name="{{$es}}" >{{$es}}</option>
                        @endforeach  
                      </select>
                    </div>
                    <div id="ChangeOption" style="display: none;">
                        <select id="SelectRecForChange" class="form-control textContent" name="SelectRecForChange">
                          <option value="none">SELECT RECEIVER TO CHANGE</option>
                          @foreach($DEPTnames as $sREC12)
                            <option style="font-size:14px" value="{{$sREC12}}">{{$sREC12}}</option>
                          @endforeach
                        </select><br>
                        <select id="SelectNewRec" class="form-control textContent" name="SelectNewRec">
                          <option value="none">SELECT NEW RECEIVER</option>
                          @foreach($deptRecs as $emps1)
                          <option value="{{array_search ($emps1, $deptRecs)}}" name="{{$emps1}}" id="OptionReceiverForForwardFile">
                          {{$emps1}}</option>
                            
                          @endforeach
                        </select>
                    </div>
                    <div id="RemoveOption" style="display: none;">
                        <select id="SelectRecForRemove" class="form-control textContent" name="SelectRecForRemove">
                          <option value="none">SELECT RECEIVER TO CHANGE</option>
                          @foreach($DEPTnames as $sREC12)
                            <option style="font-size:14px" value="{{$sREC12}}">{{$sREC12}}</option>
                          @endforeach
                        </select>
                    </div>
                    
                    <div class="modal-footer">
                      <button class="btn btn-primary" type="button" id="saveChanges" >Save Changes</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">CANCEL</button>
                    </div>

                    <script type="text/javascript">
                      $recsINFOS="";
                      $("#SelectActionForChange").change(function(){
                        $z0 = $("#SelectActionForChange").val();
                        if($z0 == "change"){
                          $("#ChangeOption").show('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").hide('slow');
                        }else if($z0 == "add"){
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").show('slow');
                          $("#RemoveOption").hide('slow');
                        }else if($z0 == "remove"){
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").show('slow');
                        }else{
                          $("#ChangeOption").hide('slow');
                          $("#AddOption").hide('slow');
                          $("#RemoveOption").hide('slow');
                        }
                      });
                     
                      $("#saveChanges").click(function(){
                        $x0 = $("#SelectActionForChange").val();
                        $x1 = $("#USERTOCHANGEROUTE").val();$x2 = $("#USERCHANGEPOSITION").val();
                        $x3 = $("#SelectRecForChange").val();$x4 = $("#SelectNewRec").val();
                        $x5 = $("#SelectRecForRemove").val();
                        if($x0 == 'none'){alert("NO CHANGES CHOSEN"); return;}
                        else if($x0 == 'add'){
                            if($x1 == 'none'){alert("NO CHANGES CHOSEN");return;}
                            if($x2 == 'none'){alert("NO CHANGES CHOSEN");return;}
                        }else if($x0 == 'change'){
                            if($x3 == 'none'){alert("NO CHANGES CHOSEN");return;}
                            if($x4 == 'none'){alert("NO CHANGES CHOSEN");return;}
                        }else if($x0 == 'remove'){ if($x5 == 'none'){alert("NO CHANGES CHOSEN");return;} }

                        $("#formChangeRoute").submit();
                      });
                    </script>

                </form>
              </div>
              
            </div>
          </div>
        </div>
@endif
@endif
        
      </section>
    </div>




@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <!-- <script src="{{asset('js')}}/"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script> -->

@stop

