<script type="text/javascript">
      $(document).ready(function(){
        setInterval(function(){
              $("#importantsTBL").load(location.href+" #importantsTBL>*","");
              $("#NOTIFICATIONSSTUFF").load(location.href+" #NOTIFICATIONSSTUFF>*","");
              $("#NOTIFICATIONSSTUFFALERT").show('slow');
              $("#FOoTERCTRS").load(location.href+" #FOoTERCTRS>*","");
        }, 3000);
      });
</script>
<table id="importantsTBL" class="table table-striped table-bordered table-hover">
    <thead id="tableHeader">
        <td style="max-width:50px;"></td>
        <td style="max-width:100px; min-width: 100px;">TRACKING NUMBER</td>
        <td>SUBJECT</td>
        <td colspan="3">FILE STATUS</td>
        
    </thead>
    <?php
	foreach($_file as $s){
		if($tite_page == 'Archived Files'){
			if($s->INVOLVED == 'TRUE'){
			  echo "<tbody><td>";
				if($s->sendID == $USERID){ ?>
				  <a href="{{route('senderRestore', ['tno'=>$s->transNO,'sender'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;"><?php
				}else{ ?>
		                  <a href="{{route('RecRestore',['tno'=>$s->transNO,'rec'=>$userHere,])}}" class="btn stuff" style="padding: 0px; margin:0px;"><?php
				}
				echo "<button class='btn btn-primary'>Restore</button>";
				echo "<span class='hoverstext'>Restore This Archived File</span>";
                    		echo "</a> &nbsp;";
			  echo "</td><td>";
				echo "<b>$s->transNO | $s->docTYPE </b><br>";
				if($s->IMP == 1){echo "<i><b>IMPORTANT FILE</b></i>";}
				else{echo "<i>FILE NOT IMPORTANT</i>";}
				echo "<br><i class='fa fa-paperclip'></i>$s->attachNAME";
			  echo "</td><td>";
				echo "Subject: $s->SUBJ <br> Message: $s->MSG";
			  echo "</td><td>";
				if($s->sendID == $USERID){echo "Archived: $s->dateARCHIVED";}
				else{
					
				}
			  echo "</td></tbody>";
			}else{}
		}elseif($tite_page == 'All Files'){	
			if($s->INVOLVED == 'TRUE'){
			   if($s->IMP == 1){echo "<tbody style='color:red;'>";}
			   else{echo "<tbody>";}
				echo"<td>";	?>
				   <a href="{{route('SpecificFileReceiver',['track'=>$s->transNO,'rec'=>$userHere,'sender'=>$s->SenderDept.' | '.$s->SenderName]) }}" 
					class="btn stuff" style="padding: 0px; margin:0px;"><i class="fa fa-eye"></i>
					<span class="hoverstext">View File</span> </a> &nbsp;

				   <a href="{{route('RouteArchive',['track'=>$s->transNO,'rec'=>$userHere,'titlePage'=>$tite_page]) }}" class="btn stuff" style="padding: 0px; margin:0px;">
					<i class="fa fa-trash"></i><span class="hoverstext">Delete Message</span>
                        		</a> &nbsp;<?php
				echo "</td><td>";
				   echo "<span style='font-size:20px;'><b>$s->transNO</b></span> <br> 
				   <span style='font-size:medium;'><b>".strtoupper($s->docTYPE)."</b></span> <br> 
				   <b>Date Received: </b>$s->SenderDateSENT";
				echo "</td><td>";
				   echo "<b>Sender: </b> $s->SenderDept | $s->SenderName <br>";
				   echo "<b>Subject: </b>$s->SUBJ <br>";
				   if($s->ROUTESTAT == 1){echo "<b>FILE IN A ROUTE</b><br>";}
					echo "<b><i class='fa fa-paperclip'></i>Attachment: </b>$s->attachNAME";
			
				echo "</td><td> <div id='statuses'>";
				   if($s->recTYPE == "PERSONAL MESSAGE"){echo "NO STATUS FOR PERSONAL MESSAGE";}
				   else{
			   		foreach(unserialize($s->fileSTATS) as $a){
			   			foreach($a as $b){
				   		   if($b['EmpDEPT'] == $userDATA->dept_name){
						    foreach (array_keys($b['Documents']) as $c){
							echo "<b>".strtoupper($c).":</b> ";
							if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; <span style='color:red;'>Not Accepted</span><br>";}
							else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
						    }
					
				   		}
			   		}
			   	   }
				}
				echo "</div></td></tbody>";
	  		}

		}
	}
    ?>
    
</table>
<div id="FOoTERCTRS">
    <center>{{ $_file->links() }}</center>
        <?php if ($counterOthersPage== 0){ echo "<h3><center>-- EMPTY FOLDER --</center></h3>"; } ?>	
</div>