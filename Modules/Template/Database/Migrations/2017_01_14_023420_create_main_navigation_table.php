<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainNavigationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('app.projcode').'_tmpl_main_navigation', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('group_id')->unsigned()->comment('refer to '.config('app.projcode').'_user_groups');
            $table->string('title',50);
            $table->string('route',50);
            $table->enum('parent',['0','1'])->comment('0 no subs , 1 parent with subs');
            $table->integer('arangement')->unsigned()->comment('arangement of the navigation');
            $table->string('properties');
            $table->timestamps();
            $table->softDeletes();

            $table->index(['id', 'title', 'route', 'parent', 'arangement'],config('app.projcode').'_tmpl_main_navigation');
        });

         # Insert
        $admin_group = 1;
        DB::table(config('app.projcode').'_tmpl_main_navigation')->insert(array(
            [
                'group_id' => $admin_group,
                'title' => 'Dashboard',
                'route' => 'admin.index',
                'parent' => '0',
                'arangement' => 1,
                'properties' => '{"i" : {"class":"fa fa-dashboard"}}'
            ],
            [
                'group_id' => $admin_group,
                'title' => 'Accounts',
                'route' => 'admin.accounts_list',
                'parent' => '0',
                'arangement' => 2,
                'properties' => '{"i" : {"class":"fa fa-group"}}'
            ],
            [
                'group_id' => $admin_group,
                'title' => 'References',
                'route' => '',
                'parent' => '1',
                'arangement' => 3,
                'properties' => '{"i" : {"class":"fa fa-file-text"}}'
            ],
            [
                'group_id' => $admin_group,
                'title' => 'File Archives',
                'route' => 'admin.file_archives',
                'parent' => '0',
                'arangement' => 4,
                'properties' => '{"i" : {"class":"fa fa-archive"}}'
            ],
            [
                'group_id' => $admin_group,
                'title' => 'Reports',
                'route' => 'admin.file_generate',
                'parent' => '0',
                'arangement' => 5,
                'properties' => '{"i" : {"class":"fa fa-file-text"}}'
            ],
            [
                'group_id' => 1,
                'title' => 'File',
                'route' => 'display.ALL',
                'parent' => '1',
                'arangement' => 4,
                'properties' => '{"i" : {"class":"glyphicon glyphicon-folder-open"}}'
            ],
            [
                'group_id' => 2,
                'title' => 'File',
                'route' => 'display.ALL',
                'parent' => '1',
                'arangement' => 1,
                'properties' => '{"i" : {"class":"glyphicon glyphicon-folder-open"}}',
            ],
            [
                'group_id' => 2,
                'title' => 'File Archives',
                'route' => 'admin.file_archives',
                'parent' => '0',
                'arangement' => 6,
                'properties' => '{"i" : {"class":"fa fa-archive"}}'
            ],
            [
                'group_id' => $admin_group,
                'title' => 'Database',
                'route' => 'admin.database',
                'parent' => '0',
                'arangement' => 6,
                'properties' => '{"i" : {"class":"fa fa-database"}}'
            ]
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('app.projcode').'_tmpl_main_navigation');
    }
}
