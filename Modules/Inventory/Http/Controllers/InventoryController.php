<?php



namespace Modules\Inventory\Http\Controllers;

use Modules\Setup\Init;



use Illuminate\Http\Request;

use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;

use Modules\Inventory\Entities\TransactionsModel as TM;

use Auth;

use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;

use Modules\Administrator\Entities\SystemLogsModel as SLM;





class InventoryController extends Controller

{

   /**

     * Display a listing of the resource.

     * @return Response

     */

    protected $data;

    protected $page_title = 'Login';



    function setup($vars = null)

    {

        $Init = new Init;

        $vars['page'] = $this->page_title;

        $this->data['template'] = $Init->setup($vars);

        $logs = new SLM;

        $this->data['logs'] = $logs->show_logs();

        return $this->data;

    }



    public function index()

    {

        $emp_info = DB::table('bghmc_employee_info')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->first();

        $hasSecu = DB::table('bghmc_emp_credentials_secu')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->first();

        $hasCurrent = DB::table('bghmc_emp_credentials_secu')->WHERE('emp_id', Session::get('bghmcuser')->emp_id)->WHERE('iscurrent', 1)->first();
        $this->data['hasSecu'] = "";
        if(password_verify(1, $emp_info->password)){
            if(count($hasSecu) == 0){$this->data['hasSecu'] = false;}
            else{$this->data['hasSecu'] = true;}
            if(count($hasCurrent) == 0){$this->data['hasCurrent'] = false;}
            else{$this->data['hasCurrent'] = true;}

            $secu_arr = [

                'What was your childhood nickname?',

                'What is the name of your favorite childhood friend?',

                'In what city or town did your mother and father meet?',

                'What is the middle name of your oldest child?',

                'What is your favorite team?',

                'What is your favorite movie?',

                'What is your favorite sport?',

                'What was your favorite food as a child?',

                'What was the make and model of your first car?',

                'What was the name of the hospital where you were born?',

                'Who is your childhood sports hero?',

                'What school did you attend for sixth grade?',

                'What was the last name of your third grade teacher?',

                'In what town was your first job?',

                'What was the name of the company where you had your first job?'

            ];

            $this->data['secuqs'] = $secu_arr;

            // return redirect()->route('accnt.passchange');

            $this->data['empid'] = $emp_info->emp_id;

            return view('template::top-nav-pages.password_change', $this->setup());

        }else{

            return redirect()->route('displaySdashboard');

        }

    }

   

}

