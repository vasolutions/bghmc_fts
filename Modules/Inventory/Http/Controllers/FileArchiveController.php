<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Illuminate\Support\Facades\Storage;
use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Carbon\Carbon;


class FileArchiveController extends Controller

{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null){   
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

// RECEIVER ARCHIVE MESSAGES
    public function RouteDestruction($tno, $rec,$ANum,$titlePage){
        $getFileName = [];
        $a0=FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO')->first();
        $ctDate = Carbon::now()->toDayDateTimeString();// DATE UPON DELETE
        $a1 = unserialize($a0->REC_ID);
        $a2 = unserialize($a0->ARCHIVED);
        $a2[$ANum] = 1;
        $a3 = DB::table('bghmc_employee_info')->where('emp_id',$rec)
        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $b0 = $a3->l_name." of ".$a3->dept_name. ' deleted the file. (' .$ctDate.')'; 
        $b1 = $a0->FILESTATUSREC . "\n" . $b0;
        $b2 = $a0->FILESTATUSSEND . "\n" . $b0;

        FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO') 
            ->update(['FILESTATUSREC'=>$b1,'FILESTATUSSEND'=>$b2,'ARCHIVED'=>serialize($a2), 'dateArchiveByRec'=>$ctDate]);

        array_push($getFileName, $tno);
        array_push($getFileName, FM::where('TNO',$tno)->join('bghmc_employee_info', 'bghmc_employee_info.emp_id', 'bghmc_allfiles.SEND_ID')->first()->f_name . ' ' . FM::where('TNO',$tno)->join('bghmc_employee_info', 'bghmc_employee_info.emp_id', 'bghmc_allfiles.SEND_ID')->first()->l_name);
        array_push($getFileName, FM::where('TNO',$tno)->first()->ATTACHNAME);
        $L = new SLM;
        $L->setLog($getFileName, '', 'archive_destruct', null);

        if(($titlePage == 'Outbox') || ($titlePage == 'Inbox') || ($titlePage == 'Flagged') || ($titlePage == 'All Files')){
            return redirect()->back()->with('successMSG','FILE SUCCESSFULLY DELETED!'); }
        else{ 
            return redirect()->route('display.RECEIVED')->with('successMSG','FILE SUCCESSFULLY DELETED!'); 
        }
            
    }// END OF ARCHIVE

// RECEIVER RESTORE MESSAGES
    public function ReceiverRestore($tno, $rec){
        $getFileName = [];

        $a0=TM::where('transNO',$tno)
	->join('bghmc_employee_info','emp_id',$rec)
        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $ctDate = Carbon::now()->toDayDateTimeString();
        $b0 = $a0->l_name." of ".$a0->dept_name. ' restored the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;

        TM::where('transNO',$tno)
            ->update(['STATS'=>$b1]);

        $L = new SLM;
        $L->setLog($getFileName, '', 'archive_restore', null);

        return redirect()->back()->with('successMSG','FILE SUCCESSFULLY RESTORED!');
        
            
    }// END OF ARCHIVE
        

// ====================================================== //
// =========== ARCHIVE THE FILES ========== //
// ====================================================== //
    public function ArchiveFile($tno,$titles){
	
        $getFileName = [];
        $a0=TM::where('transNO',$tno)->first();
	$b = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$ctDate = Carbon::now()->toDayDateTimeString();
	$b0 = $b->l_name." of ".$b->dept_name. ' archived the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;
	$d =$b->dept_name; 
	$d1 = $b->dept_id; 
	
	if($a0->sendID == Auth::user()->emp_id || $a0->recTYPE == "PERSONAL MESSAGE"){
		TM::where('transNO',$tno)->update(['trackingSTATS'=>$b1,'SenderDateArchived'=>$ctDate]);
	}else{
		$ctr = 0;
		$e = unserialize($a0->fileSTATS);
		foreach($e as $a){
			foreach($a as $b){
				if($b['EmpDEPT'] == $d){
					$e[$ctr][$d1]['dateArchived'] = Carbon::now()->toDayDateTimeString();
				}
			}
			$ctr++;
		}
		TM::where('transNO',$tno)->update(['trackingSTATS'=>$b1,'fileSTATS'=>serialize($e)]);
	}

        
        

        array_push($getFileName, $tno);
        array_push($getFileName, $titles);
        array_push($getFileName, TM::where('transNO',$tno)->first()->ATTACHNAME);
        $L = new SLM;
        $L->setLog($getFileName, '', 'archive_destruct', null);

        if(($titles == 'Outbox') || ($titles == 'Inbox') || ($titles == 'Flagged') || ($titles == 'All Files')){
            return redirect()->back()->with('successMSG','FILE SUCCESSFULLY ARCHIVED!'); }
	elseif(($titles == 'OutboxS')){return redirect()->route('display.SENT')->with('successMSG','FILE SUCCESSFULLY ARCHIVED!');}
        else{ return redirect()->route('display.RECEIVED')->with('successMSG','FILE SUCCESSFULLY ARCHIVED!'); }
        
    }

// ====================================================== //
// =========== RESTORE THE FILES ========== //
// ====================================================== //
    public function RestoreFile($transNO,$titles){
	$getFileName = [];
        $a0=TM::where('transNO',$transNO)->first();
	$b = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$ctDate = Carbon::now()->toDayDateTimeString();
	$b0 = $b->l_name." of ".$b->dept_name. ' archived the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;


        if($a0->sendID == Auth::user()->emp_id || $a0->recTYPE == "PERSONAL MESSAGE"){
		TM::where('transNO',$transNO)->update(['trackingSTATS'=>$b1,'SenderDateArchived'=>'N']);
	}else{
		$ctr = 0;
		$e = unserialize($a0->fileSTATS);
		foreach($e as $a){
			foreach($a as $b){
				if($b['EmpDEPT'] == $d){
					$e[$ctr][$d1]['dateArchived'] = "N";
				}
			}
			$ctr++;
		}
		TM::where('transNO',$tno)->update(['trackingSTATS'=>$b1,'fileSTATS'=>serialize($e)]);
	}

        array_push($getFileName, $transNO);
        array_push($getFileName, $titles);
        array_push($getFileName, TM::where('transNO',$transNO)->first()->ATTACHNAME);
        $L = new SLM;
        $L->setLog($getFileName, '', 'archive_restore', null);

        return redirect()->back()->with('successMSG','FILE SUCCESSFULLY RESTORED!');
        
        
    }





   
}
