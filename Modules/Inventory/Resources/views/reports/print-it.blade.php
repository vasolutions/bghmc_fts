<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="{{asset('adminlte')}}/bootstrap/css/bootstrap_print.css">
        <title>{{$title}}</title>
        <style>
            tbody > tr > td{
                font-size: 12px;
            }
        </style>
    </head>
    <body>
        <br>
        <div class="container">
            <center>
            <div style="width: fit-content;">
                <img src="{{asset('img')}}/site-logo.jpg" style="width: 100px; display: inline-block; vertical-align: middle; padding: 0 10px;">
                <div style="display: inline-block; vertical-align: middle; padding: 0px 10px;">
                    <p><b>Baguio General Hospital and Medical Center</b><br>
                    File Tracking System</p>
                </div>
            </div>
            </center>
            <br>
            <h3 style="text-align: center;"><b>{{$title}}</h3></b>
            <hr>
            <table id="tbl_reports" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <?php echo $tbl_type == "action" ? '<td>Action</td>' : ''; ?>
                        <td>Tracking No.</td>
                        <td>Document Type</td>
                        <td>Subject</td>
                        <?php echo $tbl_type == "sent" ? '' : '<td>Sender ID</td>'; ?>
                        <?php echo $tbl_type == "sent" ? '' : '<td>Sender Name</td>'; ?>
                        <?php echo $tbl_type == "sent" ? '<td>Receiver Department/s</td>' : '<td>Sender Department</td>'; ?>
                        <td>Accept Status</td>
                        <td>File Importance</td>
                        <?php echo $tbl_type == "sent" ? '<td>Date Sent</td>' : '<td>Date Received</td>'; ?>
                    </tr>
                </thead>
                <tbody>
                @foreach($allfiles as $s)
                    <tr>
                        @if($tbl_type == "action")
                            <td>
                            @if($s->dateRead == 0 && $s->dateAccepted == 0 && $s->ARCHIVED == 0)
                                No Action Taken
                            @else
                                @if($s->dateRead == 1)
                                    Read
                                @endif
                            <br>
                                @if($s->dateAccepted == 1)
                                    {{$s->dateAccepted}}
                                @endif
                            <br>
                                @if($s->ARCHIVED == 1)
                                    {{$s->ARCHIVED}}
                                @endif
                            @endif
                            </td>
                        @endif
                        <td>{{$s->TNO}}</td>
                        <td>{{$s->FT_ID}}</td>
                        <td>{{$s->SUBJ}}</td>
                        <!-- td for SENDER/RECEIVER ID-->
                        @if($tbl_type == "received" || $tbl_type == "action")
                            <td>{{$s->dateRestoredByRec}}</td>
                        @endif

                        <!-- td for SENDER/RECEIVER ID-->
                        @if($tbl_type == "received" || $tbl_type == "action")
                            <td>{{$s->dateArchiveBySend}}</td>
                        @endif
                        
                        <td>
                            @if($tbl_type == "sent")
                                <!-- LOOP FOR DEPARTMENT NAME GROUPED-->
                                <?php $isunique = []; ?>
                                @for($i = 0; $i < count($s->dateArchiveByRec); $i++)
                                    <?php if(!in_array($s->dateArchiveByRec[$i], $isunique)){ ?>
                                        - {{$s->dateArchiveByRec[$i]}}<br>
                                    <?php } ?>
                                    <?php array_push($isunique, $s->dateArchiveByRec[$i]); ?>
                                @endfor
                            @elseif($tbl_type == "received" || $tbl_type == "action")
                                {{$s->dateArchiveByRec}}
                            @endif
                        </td>
                        <td>
                            @for($i = 0; $i < count($s->pendingRouteRec); $i++) 
                                - <?php echo $s->dateArchiveByRec[$i] == 0 ? 'Not Accepted' : 'Accepted' ?><br>
                            @endfor
                        </td>
                        <td><?php echo ($s->IMP == 0 ? 'Not Important' : 'Important') ?></td>
                        <td style="width: 100px;">{{$s->dateSentBySend}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr>
            <br><br>
            _______________________________________
            <p>By: {{$user_info}}</p>
            Date printed: {{$date_printed}}
        </div>
    </body>
</html>