<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\FileTypeModel as FTM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class FilesController extends Controller
{
      /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'File Types';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(){
        $dept_info = DB::table('bghmc_filetypes')->get(); 
        $this->data['info'] = $dept_info;

        return view('administrator::filetypes_index',$this->setup());
    }

    public function new_filetype(Request $request){
        $data = $request->all();
        
        $is_existing = DB::table('bghmc_filetypes')->WHERE(strtolower('filetype_name'), strtolower($request->input('filetype')))->first();
        
        $FTM = new FTM;
        if($FTM->validate($data, ''))
        {
            if($is_existing){ 
                $data['status'] = 0;
                $data['errors']['message'] = "Document Type Already Registered";
            }
            else{
                $FTM->setInfo($request);
                $data['status'] = 1;
                $data['errors']['message'] = 'Document Type successfully added';
            }
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $FTM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }

    public function update_filetype(Request $request){
        $data = $request->all();
        
        $FTM = new FTM; 
        
        if($FTM->validate($data, '')) 
        {
            $FTM->updateInfo($request); 
            $data['status'] = 1;
            $data['errors']['message'] = 'Document Type successfully updated';
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $FTM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }
}
