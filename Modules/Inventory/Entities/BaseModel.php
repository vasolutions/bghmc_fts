<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Validator;


class baseModel extends Model
{
    protected $fillable = [];
    protected $table = '';
    protected $rules = array();

    protected $errors;

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);
        if($v->fails())
        {
            $this->errors = $v->messages();
            return false;
        }
        else{
            return true;
        }
    }

    public function errors()
    {
        return $this->errors;
    }
}
