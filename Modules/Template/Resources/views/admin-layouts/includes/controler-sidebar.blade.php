  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark ">

    <div class="tab-content" style="max-height: 100%;">
        <a>
          <h3 class="control-sidebar-heading text-center" style="color: #fff;">RECENT ACTIVITY</h3>
        </a>
        <ul class="control-sidebar-menu">
        @if(count($logs) != 0)
          @foreach($logs as $log)
          <li>
            <a href="javascript:void(0)">
              @if(strpos($log->type, 'new') !== false)
                <i class="menu-icon glyphicon glyphicon-pencil" style="color:LightGreen;"></i>
              @elseif(strpos($log->type, 'update') !== false)
                <i class="menu-icon glyphicon glyphicon-pencil" style="color:yellow;"></i>
              @elseif(strpos($log->type, 'send') !== false || strpos($log->type, 'reply') !== false)
                <i class="menu-icon glyphicon glyphicon-send" style="color:LightBlue;"></i>
              @elseif(strpos($log->type, 'file') !== false)
                <i class="menu-icon glyphicon glyphicon-file" style="color:white;"></i>
              @elseif(strpos($log->type, 'download') !== false || strpos($log->type, 'report') !== false)
                <i class="menu-icon glyphicon glyphicon-download-alt" style="color:white;"></i>
              @elseif(strpos($log->type, 'archive') !== false)
                <i class="menu-icon glyphicon glyphicon-folder-close" style="color:red;"></i>
              @elseif(strpos($log->type, 'flag') !== false)
                <i class="menu-icon glyphicon glyphicon-flag" style="color:yellow;"></i>
              @elseif(strpos($log->type, 'forward') !== false)
                <i class="menu-icon glyphicon glyphicon-export" style="color:white;"></i>
              @elseif(strpos($log->type, 'reset') !== false)
                <i class="menu-icon glyphicon glyphicon-lock" style="color:yellow;"></i>
              @endif
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">{{$log->log}}</h4>
                <p>by: #{{$log->logged_user}} ({{\Carbon\Carbon::parse($log->created_at)->format('M d Y h:i A')}})</p>
              </div>
            </a>
          </li>
          @endforeach
        @else
          <li>
            <center>No Activities for today</center>
          </li>
        @endif
        </ul>
    </div>
  </aside>
  <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>