 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/dataTables/dataTables.bootstrap.css">
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
@stop

@section('pagename')
    
    <span style="font-size: 25px;">
        @include('inventory::includes._titleFILE')
    </span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
                
            @include('inventory::includes._ALL') 
            <div class="box">
                <div class="box-header">
                    <ul class="pull-right" style="list-style-type: none; display: flex; align-items:center; ">
                        <li style="padding-top:5px;">
                            <div style="text-align:right; margin-right: 20px;" class="searchs">
                                    <input type="text" id="searchUL" onkeyup="searchFunction()" placeholder="Track file.." title="Enter Tracking No.">
                            </div>
                        </li>
                        <li></li>
                        <li style="padding-top: 15px;">
                            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file"><i class="glyphicon glyphicon-pencil" ></i> &nbsp; Compose</button>
                    
                        </li>
                    </ul>

                </div>
<!-- ============== TRACKING FILES CODE SHIT ================ -->
                <h5 class="filesNotice" id="hideThis" style="color:red; font-style: italic;">NOTE: You can search using tracking number,subject name, or even the message content!</h5>
                <div id="hideThis" class="showsLISTS">

                </div> <!-- ================== END SHOWLIST ================ -->
                
            </div> <!-- =====END BOX===== -->

            <div class="box">
                @if(Session::has('message'))
                <div class="box-header">
                    <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
                </div>
                @endif
                <!-- =================== -->
                    
                @if(($template['page_title'])=='Personnel')
                    <!-- == PERSONNEL HOME PAGE == -->
                    <div class="COUNTERS" style="display: inline-table; padding: 10px 20px 20px 170px; width: 100%;">
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;" >IMPORTANT FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-6" style="font-size:15px;"><i>Unread:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Read:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Sent:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-6" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span>
                                </div>
                            </button>
                            &emsp;&emsp;
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;" >ACCEPTED FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-7" style="font-size:15px;"><i>Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Not Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-7" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span><br><br>
                                </div>
                            </button>
                            &emsp;&emsp;
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;">UNACCEPTED FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-7" style="font-size:15px;"><i>Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Not Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-7" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span><br><br>
                                </div>
                            </button>
                        
                    </div>
                @else
                    <!-- == ADMIN HOME PAGE == -->
                    <div class="COUNTERS" style="display: inline-table; padding: 10px 20px 20px 170px; width: 100%;">
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;" >IMPORTANT FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-6" style="font-size:15px;"><i>Unread:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Read:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Sent:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-6" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-6" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span>
                                </div>
                            </button>
                            &emsp;&emsp;
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;" >ACCEPTED FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-7" style="font-size:15px;"><i>Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Not Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-7" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span><br><br>
                                </div>
                            </button>
                            &emsp;&emsp;
                            <button class="btn btn-primary btn-lg" id="importantFileCTR" style="color:#000000; font-weight: bolder; border-radius:8px solid:#ccc; display: inline-table; min-width: 25%; max-width: 25%; box-shadow: 3px 3px 3px 3px #000000; text-align: left; padding: 0px; min-height: 200px; height: 200px; max-height: 200px;">UNACCEPTED FILES         <br>
                                <div class="col-lg-12">
                                    <span class="col-lg-7" style="font-size:15px;"><i>Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Not Important:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <br>
                                    <span class="col-lg-7" style="font-size:15px;"><i>Archived:</i></span>
                                    <span class="col-lg-3">2</span>
                                    <hr style="width:90%;">
                                    <span class="col-lg-7" style="font-size:15px;">TOTAL:</span>
                                    <span class="col-lg-3">2</span><br><br>
                                </div>
                            </button>
                        
                    </div>
                @endif

            </div><!-- ==== END BOX ==== -->
                    
        </section>
    </div>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <script src="{{asset('js')}}/jquery.min.js"></script>
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    

@stop