<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\SystemLogsModel as SLM;
use Cornford\Backup\Engines\BackupEngineMysql;
use Cornford\Backup\BackupProcess;
use Symfony; use Backup;
use Illuminate\Support\Facades\Storage;
// use Illuminate\Http\Response;

class DbController extends Controller
{
    protected $data;
    protected $page_title = 'Departments';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        // $this->data['exportmess'] = "Exported to downloads folder";
        // $this->data['importmess'] = "Import successful.";
        return $this->data;
    }
    
    public function checkfunctioneximp(Request $request){
        if($request->input('db_type') == 'db_export'){
            $request->session()->forget('exportmess');
            $request->session()->forget('importmess');
            define("BACKUP_PATH", "C:\Users\user\Desktop\/");
            
            $server_name   = "localhost";
            $username      = "root";
            $password      = "";
            $database_name = "bghmc_fts";
            $date_string   = date("Ymd");
            
            $cmd = "mysqldump --routines -h {$server_name} -u {$username} -p{$password} {$database_name} > " . BACKUP_PATH . "{$date_string}_{$database_name}.sql";
            exec($cmd);

            if($request->input('exp_type') == 'true'){ Backup::setCompress(true); }
            Backup::export();

            if(Backup::export() == true){ 
                //get the lastest file uploaded in db-backup-files/
                $path = $_SERVER['DOCUMENT_ROOT'] . "/db-backup-files"; 
                $latest_ctime = 0;
                $dbfilename = '';    
                $d = dir($path);
                while (false !== ($entry = $d->read())) {
                $filepath = "{$path}/{$entry}";
                    $latest_ctime = filectime($filepath);
                    $dbfilename = $entry;
                }

                // download file
                $this->data['exportmess'] = 'Exported.'; 
                header("Content-disposition: attachment; filename=" . $dbfilename);
                header("Content-type: application/pdf");
                readfile( $_SERVER['DOCUMENT_ROOT'] . "/db-backup-files" . "/" . $dbfilename);

                return redirect()->route('admin.database')->with('exportmess','Database exported.'); 
            }
            else{
                // return if no file was exported
                return redirect()->route('admin.database')->with('exportmess',''); 
            }
        }
        else if($request->input('db_type') == 'db_import'){
            $request->session()->forget('exportmess');
            $request->session()->forget('importmess');
            
            $fileAttachment = $request->file('getdbfile');
            $nameOfFile = $fileAttachment->getClientOriginalName();
            $sizes = $fileAttachment->getSize();
            $pathHere = storage_path('app/DBfiles/'.$nameOfFile);

            if(!Storage::exists('app/DBfiles/'.$nameOfFile))
            {
                Storage::putFileAs('DBfiles/', $fileAttachment,$nameOfFile);
            }
            // return $pathHere;
            Backup::restore($pathHere);

            if(Backup::restore($pathHere) == true){ 
                $this->data['importmess'] = 'Import Successful.'; 
            }
            return redirect()->route('admin.database')->with('importmess','Import Successful.');
            // return view('administrator::database', $this->setup());
        //     $restore_file  = "/home/abdul/20140306_world_copy.sql";
        //     $server_name   = "localhost";
        //     $username      = "root";
        //     $password      = "";
        //     $database_name = "bghmc_fts";
            
        //     $cmd = "mysql -h {$server_name} -u {$username} -p{$password} {$database_name} < $restore_file";
        //     exec($cmd);

        //     $this->data['mess'] = 'Successfully imported database.';
        //     return view('administrator::database', $this->setup());
            
            // Backup::restore('./database_backup.sql');
        }
    }
}
