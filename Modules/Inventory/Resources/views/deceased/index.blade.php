<!-- @extends('template::admin-pages.menus.'.$template['menu']) -->
@extends('template::admin-pages.menus.administrators')


@section('content')
  <div class="content-wrapper">
    <section class="content">
        <span class="pull-left" style="font-size: 25px;">Deceased Summary List</span>
        <button type="button" class="btn btn-function btn-new pull-right" data-toggle="modal" data-target="#mdl-new-deceased">NEW<i class="fa fa-sm fa-plus" style="margin-left: 10px;"></i> </button>
        <div style="margin-top: 60px;"></div>
        <div class="box">
            <div class="box-body">
                <table id="tbl_deceased" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>First Name</td>
                            <td class="hidden-xs">Middle Name</td>
                            <td>Last Name</td>
                            <td>Gender</td>
                            <td>Age</td>
                            <td>Status</td>
                            <td class="hidden-xs">View / Archive</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($info as $d_info)
                        <tr>
                            <td>{{$d_info->id}}</td>
                            <td>{{$d_info->f_name}}</td>
                            <td class="hidden-xs">{{$d_info->m_name}}</td>
                            <td>{{$d_info->l_name}}</td>
                            <td>
                                @if ($d_info->gender == 'f')
                                    Female
                                @elseif ($d_info->gender == 'm')
                                    Male
                                @endif
                            </td>
                            <td>{{$d_info->age}}</td>
                            <td>
                                @if($d_info->status == "Cremation Service Completed")
                                    <label class="c-completed">{{$d_info->status}}</label><br>
                                @elseif($d_info->status =="Incomplete Requirements")
                                    <label class="c-incomplete">{{$d_info->status}}</label><br>
                                @elseif($d_info->status =="Initially Scheduled")
                                    <label class="c-initially">{{$d_info->status}}</label><br>
                                @elseif($d_info->status =="Schedule Confirmed")
                                    <label class="c-confirmed">{{$d_info->status}}</label><br>
                                @elseif($d_info->status =="Canceled")
                                    <label class="c-canceled">{{$d_info->status}}</label><br>
                                @endif
                            </td>
                            <td class="hidden-xs">
                                <a href="{{ route('deceased.show',array('id'=>$d_info->id)) }}" class="btn btn-function btn-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                                <!--<a class="btn btn-function btn-update"><i class="glyphicon glyphicon-pencil"></i></a>-->
                                @if($d_info->status == "Canceled" || $d_info->status == "Cremation Service Completed")
                                    <a class="btn btn-function btn-archive" data-toggle="modal" data-target="#mdl-confirm-archive_{{$d_info->id}}"><i class="glyphicon glyphicon-folder-open"></i></a>
                                @else
                                    <a class="btn btn-function btn-archive-disabled" disabled data-toggle="tooltip" title="Cannot archive if status is not Canceled or Completed"><i class="glyphicon glyphicon-folder-open btn-archive-disabled" disabled></i></a>
                                @endif
                            </td>

                            <div id="mdl-confirm-archive_{{$d_info->id}}" class="modal fade table-header mdl-arch">
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <div class="text-center">
                                                <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                                            </div>
                                        </div>
                                        <div class="modal-body form-group-pad-body row">
                                            <div id="status"></div>
                                            <h4 class="modal-title text-center">Move Deceased <span style="color: #dd1e1e;">{{$d_info->f_name}} {{$d_info->l_name}}</span> to Archives?</h4><br>
                                            <div class="text-center">
                                                @if($d_info->status == "Cremation Service Completed")
                                                    <label class="c-completed">{{$d_info->status}}</label><br>
                                                @elseif($d_info->status =="Incomplete Requirements")
                                                    <label class="c-incomplete">{{$d_info->status}}</label><br>
                                                @elseif($d_info->status =="Initially Scheduled")
                                                    <label class="c-initially">{{$d_info->status}}</label><br>
                                                @elseif($d_info->status =="Schedule Confirmed")
                                                    <label class="c-confirmed">{{$d_info->status}}</label><br>
                                                @elseif($d_info->status =="Canceled")
                                                    <label class="c-canceled">{{$d_info->status}}</label><br>
                                                @endif
                                            </div>
                                            <div style="margin-top: 20px;">
                                                <div class="col-sm-6"><a class="btn btn-function btn-cancel" data-dismiss="modal">Cancel</a></div>
                                                <div class="col-sm-6"><a onclick="$(this).setArchive( '{{ route('inventoryarchive.set', $d_info->id) }}', {{$d_info->id}} );" class="btn btn-function btn-confirm-archive">Confirm Archive</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
  </div>


<!--MODAL NEW DECEASED-->
  <div id="mdl-new-deceased" class="modal fade table-header">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">
                    <span title="Beyond the Sunset" class="title" style="font-size:30px;">Beyond the Sunset</span>
                </div>
            </div>
            <div class="modal-body form-group-pad-body">
                <h4 class="modal-title text-center">Add Deceased Details</h4><br>
                <!--<br>action="{{url('/inventory/deceased')}}"-->
                <div id="status">
                </div>
                    <form class="thing-form" role="form" name="newdeceasedform" id="newdeceasedform" method="post" >
                    {{ csrf_field() }}
                        <div class="form-group has-feedback">
                                <div class="form-group">
                                    <label class="control-label float-left" for="name">Name</label>
                                    <div class="form-group form-group-pad">
                                        <input type="text" class="form-control form-control-pad" name="fname" placeholder="First name">
                                        <input type="text" class="form-control form-control-pad" name="mname" placeholder="Middle name">
                                        <input type="text" class="form-control form-control-pad" name="lname" placeholder="Last name">
                                        <input type="text" class="form-control form-control-pad" name="ename" placeholder="Suffix name (e.g. Jr, III) --optional">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="gender" name="gender">Gender</label><br>
                                    <div class="form-group-pad">
                                        <input type="radio" class="form-control-pad" name="rd-gender" value="f"> Female
                                        <input type="radio" class="form-control-pad" name="rd-gender" value="m" style="margin-left: 10px;"> Male
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label float-left" for="age">Age</label><br>
                                    <div class="form-group-pad">
                                        <input type="number" id="age" class="form-control form-control-pad" name="age" placeholder="0.1-0.11 months for less than a year old">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label float-left" for="citizenship">Address</label><br>
                                    <div class="form-group-pad">
                                        <input type="text" name="province" class="form-control form-control-pad" placeholder="Province">
                                        <input type="text" name="city" class="form-control form-control-pad" placeholder="City / Municipality">
                                        <input type="text" name="brgy" class="form-control form-control-pad" placeholder="Barangay">
                                        <textarea name="details" class="form-control form-control-pad" placeholder="Details"></textarea>
                                    </div>
                                </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-9" style="text-align: right;"><a class="btn btn-lg" data-dismiss="modal">Cancel</a></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block btn-bts btn-lg" onclick="$(this).sendDeceasedInfo();"> Add</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
  </div>
@stop

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/select2/select2.min.css">
@stop
@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <script type="text/javascript">

        $.fn.sendDeceasedInfo = function(){
            $.ajax({
                type : 'POST',
                url : '{{route('deceased.sendDeceased')}}',
                data: $('#newdeceasedform').serialize(),
                dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    console.log(data);
                    var errors = '';
                    if(data['status']==0){
                        for(var key in data['errors']){
                            errors += data['errors'][key]+'<br />';
                        }
                        $('#mdl-new-deceased .modal-body  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Alert!</h4>'+errors+'</div>').fadeIn();
                    }else{
                        $('#mdl-new-deceased .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i> Success!</h4>'+errors+'</div>').fadeIn().delay(1500).fadeOut(100);
                        location.reload();
                    }
            
                }
            });
        };

        $.fn.setArchive = function(rt, id){
            $.ajax({
                type : 'GET',
                url : rt,
                data: {'id' : id},
                dataType : 'json',
                error : function(){
                    alert('error');
                },
                success : function(data){
                    $('.mdl-arch .modal-body  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-ban"></i> Success!</h4>Deceased moved to Archives</div>').fadeIn();
                    location.reload();
                }
            });
        };

        $('#tbl_deceased').DataTable({
            order: [ [0, 'desc'] ]
        });

        $('.bdate').on('blur', function(){
            var bday = $('#bday').val();
            var bmonth = $('#bmonth').val();
            var byear = $('#byear').val();


            if(bday != '' && bmonth != '' && byear != ''){
                if(bday.length == 1){
                    bday = '0' + bday;
                    $('#bday').val(bday);
                }
                var checkdate = moment(byear+'-'+bmonth+'-'+bday, 'YYYY-MM-DD').isValid(); 
                if(checkdate){
                    $('#byear').removeClass('error'); $('#bday').removeClass('error');
                    $('#birthday').val(byear + '-' + bmonth + '-' + bday);
                }
                else{
                    var checkyear = moment(byear, 'YYYY').isValid();
                    var checkday = moment(bday, 'DD').isValid();
                    if(checkyear != true){
                        $('#byear').addClass('error');
                    }
                    if(checkday != true){
                        $('#bday').addClass('error');
                    }
                }
            }
        });
    </script>
@stop