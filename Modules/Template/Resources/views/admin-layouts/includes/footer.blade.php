<footer class="main-footer">
  <div class="pull-left hidden-xs">
    <strong>Copyright &copy; {{Carbon\Carbon::now()->format('Y')}} 
    <!-- <a target="_blank" href="https://ubaguio.edu">ARM &amp; RG</a> -->
    </strong>
  </div>
  <span class="pull-right hidden-xs">All rights reserved.</span>

</footer>