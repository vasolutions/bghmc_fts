<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class SpecificFileController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;

        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
            $allDepts = DB::table('bghmc_departments')->get();
            $this->data['deptRecs'] = $allDepts;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;

        return $this->data;

    }
// ============================================ //
// ===== RECEIVER VIEWS THE SPECIFIC FILE ===== //
// ============================================ //

    public function SpecificFileReceiver($track, $rec, $sender){ 
	
        $this->data['a0']=$a0 = TM::where('transNO',$track)->first();
	$this->data['THREADS'] = $a0->THREADMSG;
        $this->data['sender'] = $sender; 
        $this->data['RECID'] = $rec; 
	$this->data['depts'] = $depts = DB::table('bghmc_employee_info')->where('emp_id',$rec)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	// ===== FILE ATTACHMENT
	// ===== SETS THE MODAL AND TEXT IF FILE HAS ATTACHMENT OR NONE
        if($a0->attachNAME == "NO ATTACHMENT"){
            $this->data['AttachTip'] = "No File Attached.";   
            $this->data['AttachTipModal'] = "#NoDownloadFileAlert";
        }else{
            $this->data['AttachTip'] = "Download File";
            $this->data['AttachTipModal'] = "#DownloadFileAlert"; 
        }
	

       	$userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)
            ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;
            
            $allDepts = DB::table('bghmc_employee_info')
	    	->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
	    	$deptArray=[];  $dArray=[];
	    	foreach ($allDepts as $sadShit){array_push($deptArray,$sadShit->dept_id); $dArray[$sadShit->dept_id]=$sadShit->dept_name;}
	        $this->data['deptRecs'] = $dArray;
	    $DEPTnames = array();
	if($a0->recTYPE != "PERSONAL MESSAGE"){
	foreach(unserialize($a0->fileSTATS) as $a){
		foreach($a as $b){
			if($b['dateArchived'] == "N"){$this->data['RecArchive'] = 'FALSE';}
			else{$this->data['RecArchive'] = 'TRUE';}
			array_push($DEPTnames,$b['EmpDEPT']);
		}
	}
	}
	
	$this->data['DEPTnames'] = $DEPTnames;
        return view('inventory::FileManagement.fileSpecificReceiver',$this->setup());
    }

// RECEIVER VIEWS THE SPECIFIC FILE IN A ROUTE
    public function SpecificFileReceiverInARoute($track, $rec, $acpt,$ANum){   

        $emps=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
        $this->data['emps'] = $emps;
        $a0 = FM::where('TNO',$track)->join('bghmc_filestatus','dateTNO','TNO')->join('bghmc_filetypes','filetype_id','FT_ID')->first();
        $this->data['ss']=$a0;
        $b0 =  unserialize($a0->ACCPT);
        $b1 = $b0[$ANum];
        $a3 = DB::table('bghmc_employee_info')->where('emp_id',$a0->sendID)
            ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
            
        $this->data['acpt'] = $acpt;
        $this->data['TRACKINGNUMBER'] = $track;
        $this->data['ArrayNumber'] = $ANum;
        $this->data['FILETYPENAME'] = $a0->filetype_name;
        $this->data['RECID'] = $rec;
        $this->data['SUBJECT'] = 'Subject: '.$a0->SUBJ;
        $this->data['MSG'] = $a0->MSG;
        $this->data['ATTACHMENT'] = 'Attachment: ' . $a0->ATTACHNAME;
        $this->data['THREADS'] = $a0->THREADMSG;
        $this->data['SENDER'] = 'From: '.$a3->dept_name . ' department --- by ' . $a3->f_name . ' '. $a3->l_name ;
        $this->data['DATEREC'] = 'Date Received: ' . $a0->dateSentBySend;

        //importance of the file
        if($a0->IMP == 0){$this->data['IMP']="NOT IMPORTANT"; $this->data['imports']="fa fa-flag-o"; $this->data['impName']="Mark as Important";}
        else{$this->data['IMP']="IMPORTANT!"; $this->data['imports']="fa fa-flag"; $this->data['impName']="Mark as Not Important";}

        //attachments
        if($a0->ATTACHNAME == "No Attachment"){$this->data['AttachTip']="No File Attached."; $this->data['AttachTipModal']="#NoDownloadFileAlert";}
        else{ $this->data['AttachTip'] = "Download File"; $this->data['AttachTipModal'] = "#DownloadFileAlert";}

    // ===== ROUTE =====
    $ctDATES = Carbon::now()->toDayDateTimeString();
    if($a0->ROUTESTAT == 1){
        $rouetCTR = 0; $receiversArray = array(); $arrayNo = array();
        $bad = unserialize($a0->dateRestoredBySend);
        foreach($bad[$ANum] as $s){
        return $s;
            $ac = unserialize($a0->ACCPT)[$rouetCTR];

            $namess = DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')
                ->where('emp_id',$s)->first();
            if($ac == 0){
            array_push($receiversArray,($rouetCTR + 1).' - '.$namess->f_name.' '.$namess->l_name.' of '.$namess->dept_name." --- NOT ACCEPTED\r\n");
            }else{
            array_push($receiversArray,($rouetCTR + 1).' - '.$namess->f_name.' '.$namess->l_name.' of '.$namess->dept_name." --- ACCEPTED\r\n");
            }
            
            if($s == $rec){ array_push($arrayNo,($rouetCTR+1));}
            $rouetCTR++;
        }
        // == cannot delete if not yet accepted ==
        
        foreach ($arrayNo as $arrrays) {
            $s0 = $arrrays - 1;
            if((unserialize($a0->ACCPT)[$s0]) == 0){ 
                $deleteROUTE = "#CANNOTdeleteAlert";
                $this->data['deleteMSG']="Can't Delete!";  $this->data['deleteROUTE']=$deleteROUTE; 

            }
            else{ $this->data['deleteMSG'] = "Delete"; 
                $deleteROUTE = "#CANdeleteAlert";
                $this->data['deleteROUTE']=$deleteROUTE;
            }
        }
        // == FILE ACCEPT STUFF DESIGNS AND STUFF ==
        $n0 = unserialize($a0->REC_ID);
        $n1 = unserialize($a0->ACCPT);
        $n2 = array_keys($n0,$rec); //return index of receivers
        
        $this->data['n2'] = $n2;
        $this->data['n1'] = $n1;
        if(count($n2)>1){ // RECEIVER iS LOCATED IN ROUTE MORE THAN ONCE
            $this->data['AcptTips'] = "MOREMOREMORE"; $this->data['AcptNotifs'] = '';
            if(!in_array(0,$n2)){$this->data['index0'] = 'no'; }
            else{ $this->data['index0'] = 'yes'; }
        }else{// RECEIVER iS LOCATED IN ROUTE ONCE
            if($ANum != 0){// IF RECEIVER HAS NO INDEX 0
                if($n1[$ANum-1] == 0){ $this->data['AcptTips']="Can't Accept File Yet"; $this->data['AcptNotifs']='#AcptNotifNOTyet'; }
                elseif($n1[$ANum-1] == 1){
                    if($n1[$ANum] == 0){ $this->data['AcptTips']='Accept Filesss'; $this->data['AcptNotifs']='#AcptNotif'; }
                    else{ $this->data['AcptTips']='File Accepted'; $this->data['AcptNotifs']='#AlreadyAcptNotif'; }
                }
            }else{// IF RECEIVER iS LOCATED AT INDEX 0
                if($n1[0] == 0){ $this->data['AcptTips'] = 'Accept Files'; $this->data['AcptNotifs'] = '#AcptNotif'; }
                else{ $this->data['AcptTips'] = 'File Accepted'; $this->data['AcptNotifs'] = '#AlreadyAcptNotif'; }
            }
        }

        $f = implode('',$receiversArray); $f1 = explode("\r\n",$f); array_pop($f1); $f2 = array(); $this->data['f2'] = array();
        foreach ($f1 as $ff){ array_push($f2, substr($ff, 0,1)); }
        $this->data['f1CTR'] = count($f1) - 1; $this->data['routeRECS'] = $f1;

    } // END OF ROUTE


        //SENT FILE COUNTER
        $sentCTR = FM::where('sendID',Auth::user()->emp_id)->count();  $this->data['sentCTR'] = $sentCTR;

        $userHere=Auth::user()->emp_id; $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
        $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get(); $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
        $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
        $this->data['emps2'] = $emps2;
        // === file type ===
        $ftypes=DB::table('bghmc_filetypes')->get(); $this->data['ftypes'] = $ftypes;
        // === departments ===
        $asd = DB::table('bghmc_departments')->get(); $this->data['allDepts'] = $asd;
        // == UPDATES ==

        $c0 = unserialize($a0->dateReadByRec);
        $c00 = unserialize($a0->dateRead);
        $c000 = unserialize($a0->dateAccepted);
        foreach ($n2 as $nn2) { 
            $c0[$nn2] = 1; 
            if($c00[$nn2] == 0){$c00[$nn2] = $ctDATES;}
            if($c000[$nn2] == 0){$c000[$nn2] = $ctDATES;}
        }
        $c1 = serialize($c0);
        $c11 = serialize($c00);
        $c111 = serialize($c000);
        

        FM::where('TNO',$track)->join('bghmc_filestatus','dateTNO','TNO') 
            ->update(['dateReadByRec'=>$c1,'dateRead'=>$c11,'dateAccepted'=>$c111]);
        // == END UPDATES ==
        return view('inventory::FileManagement.fileSpecificReceiverInARoute',$this->setup());
    }

// ======================================== //
// ===== USERS REPLIES TO THE MESSAGE ===== //
// ======================================== //

    public function RepyShit($track, Request $request){ 
    	$forlog = [];
    	Carbon::now()->toDayDateTimeString();
        $a0 = TM::where('transNO',$track)->first();  
        $a1 = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)
            ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first(); 
        $a3 = $a1->l_name. ' (' .Carbon::now()->toDayDateTimeString().'): '. $request['thread']; 
        $a4 = $a0->threadMSG."\n".$a3; 
        $a5 = $a0->trackingSTATS."\n".$a1->l_name. ' sent a reply. ' .Carbon::now()->toDayDateTimeString();

        if($a0->threadMSG == ''){
            TM::where('transNO',$track)->update(['threadMSG'=>$a3,'trackingSTATS'=>$a5]);
        }else{
            TM::where('transNO',$track)->update(['threadMSG'=>$a4,'trackingSTATS'=>$a5]);
        }
        
        $L = new SLM;
        array_push($forlog, $a0->attachNAME, $a0->transNO);
        $L->setLog($forlog, '', 'reply', null);
        
        return redirect()->back()->with('successMSG','Reply Sent ');
    }
// ==================== END ======================//


// RECEIVER ROUTE CHANGES THE ROUTE
    public function changeROUTErec($tno, $rec, Request $request){
        $forlog = [];

        if(($request['USERTOCHANGEROUTE'] == 'none')||($request['USERCHANGEPOSITION'] == 'none')){return redirect()->back();}
            $k0 = $request['USERTOCHANGEROUTE'];
            $k1 = substr($request['USERCHANGEPOSITION'], 0,1);
		$x0 = $request['reasons'];
            $a2 = FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO')->first();
            $b0 = DB::table('bghmc_employee_info')->where('emp_id',$rec)
                ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
            $b1 = $b0->l_name.' of '.$b0->dept_name. ' changed the route (set of receivers). (' .Carbon::now()->toDayDateTimeString().')'; 
            $b2 = $a2->FILESTATUSREC . "\n" . $b1;
            $b3 = $a2->FILESTATUSSEND . "\n" . $b1;
            $a3 = unserialize($a2->REC_ID); $c0 = unserialize($a2->ACCPT); $c1 = unserialize($a2->ARCHIVED); 
            $c2 = unserialize($a2->dateReadByRec); $c3 = unserialize($a2->dateRead); $c4 = unserialize($a2->dateAccepted);
            $docArr = unserialize($a2->dateRestoredBySend);
            $docArr000 = count(unserialize($a2->dateRestoredBySend));
            if($request['USERCHANGEPOSITION'] == "last"){
                array_push($c0, 0); array_push($c1, 0); array_push($a3,$k0);
                array_push($c2, 0); array_push($c3, 0); array_push($c4,0);
                $x1 = $a2->THREADMSG."\n"."Reason for changing route: ".$x0.' by '. $b0->l_name.' of '.$b0->dept_name. '-'.Carbon::now()->toDayDateTimeString(); 
                FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO')->update(['REC_ID'=>serialize($a3),
                'ACCPT'=>serialize($c0),
                'ARCHIVED'=>serialize($c1),
                'FILESTATUSREC'=>$b2,
                'FILESTATUSSEND'=>$b3,
                'dateReadByRec'=>serialize($c2),
                'dateRead'=>serialize($c3),
                'dateAccepted'=>serialize($c4),
                'THREADMSG'=>$x1]);
            }else{
                $k1 = $k1 - 1;
                array_splice($a3, $k1, 0 , $k0); array_splice($c0, $k1, 0 , 0);
                array_splice($c1, $k1, 0 , 0); array_splice($c2, $k1, 0 , 0);
                array_splice($c3, $k1, 0 , 0); array_splice($c4, $k1, 0 , 0);
                
                $l0 = array_values($a3);
	        $l1 = array_values($c0);
	        $l2 = array_values($c1);
	        $l3 = array_values($c2);
	        $l4 = array_values($c3);
	        $l5 = array_values($c4);
		$x1 = $a2->THREADMSG."\n"."Reason for changing route: ".$x0.' by '. $b0->l_name.' of '.$b0->dept_name. '-'.Carbon::now()->toDayDateTimeString();
                FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO') ->update(['REC_ID'=>serialize($l0),
                'ACCPT'=>serialize($l1),
                'ARCHIVED'=>serialize($l2),
                'FILESTATUSREC'=>$b2,
                'FILESTATUSSEND'=>$b3,
                'dateReadByRec'=>serialize($l3),
                'dateRead'=>serialize($l4),
                'dateAccepted'=>serialize($l5),
                'THREADMSG'=>$x1]);
            }
            

            $L = new SLM;
            array_push($forlog, $a2->ATTACHNAME, $tno);
            $L->setLog($forlog, '', 'change_route', null);
        
            return redirect()->back()->with('successMSG','Receiver was successfully added to the route');
    }

    public function ChangeRouteRecNotif($tno, $rec, Request $request){

        $a0 = FM::where('TNO',$tno)->first();
        $b0 = DB::table('bghmc_employee_info')->where('emp_id',$rec)
            ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $b1 = $b0->l_name.' of '.$b0->dept_name. ' notified the sender about the changes on the route of a file. (' .Carbon::now()->toDayDateTimeString().')'; 
        $b2 = $a0->FILESTATUSREC . "\n" . $b1;
        $b3 = $a0->FILESTATUSSEND . "\n" . $b1;

        $b4 = 'Notification sent by '.$b0->f_name.' '.$b0->l_name.' of '.$b0->dept_name. '(' .Carbon::now()->toDayDateTimeString().'): '.$request['Commentaries'];

        FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO') 
            ->update(['pendingRouteRec'=>$b4,'FILESTATUSREC'=>$b2,'FILESTATUSSEND'=>$b3]);

        return redirect()->back()->with('successMSG','Notification has been sent to the sender');

    }

    

// ================== END ==================
}
