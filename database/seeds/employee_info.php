<?php 

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class employee_info extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create fake data to fill database
        $faker = Faker\Factory::create();
        $dt_created = Carbon\Carbon::now();

        // $limit = 2;
        // $accnt_type = array("administrator", "personnel");

        // for ($i = 0; $i < $limit; $i++) {
            DB::table('bghmc_employee_info')->insert([
                'emp_id' => '123456789',
                'password' => Hash::make('root'),
                'f_name' => $faker->firstName,
                'l_name' => $faker->lastName,
                'accnt_type' => 'administrator',
                'pos_id' => '8000' . $faker->numberBetween($min = 1, $max = 9), //080 is ascii of letter "P" for positions
                'dept_id' => '6800' . $faker->numberBetween($min = 1, $max = 48), //068 is ascii of letter "D" for departments
                'isactive' => '1',
                'created_at' => $dt_created->toDateTimeString(),
            ]);
            DB::table('bghmc_employee_info')->insert([
                'emp_id' => '987654321',
                'password' => Hash::make('root'),
                'f_name' => $faker->firstName,
                'l_name' => $faker->lastName,
                'accnt_type' => 'personnel',
                'pos_id' => '8000' . $faker->numberBetween($min = 1, $max = 9), //080 is ascii of letter "P" for positions
                'dept_id' => '6800' . $faker->numberBetween($min = 1, $max = 48), //068 is ascii of letter "D" for departments
                'isactive' => '1',
                'created_at' => $dt_created->toDateTimeString(),
            ]);
        // }
    }
}
