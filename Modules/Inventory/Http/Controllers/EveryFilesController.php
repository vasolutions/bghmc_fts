<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class EveryFilesController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;
        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->where('bghmc_employee_info.isactive',1)->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->where('bghmc_employee_info.isactive',1)->get();
            $this->data['emps2'] = $emps2;
//            $allDepts = DB::table('bghmc_departments')->get();
		$allDepts = DB::table('bghmc_employee_info')
	    	->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
	    	$deptArray=[];  $dArray=[];
	    	foreach ($allDepts as $sadShit){array_push($deptArray,$sadShit->dept_id); $dArray[$sadShit->dept_id]=$sadShit->dept_name;}
	//    	return array_unique($deptArray);

            $this->data['deptRecs'] = $dArray;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;
        // $z = $this->data;

        return $this->data;

    }
// ============================================================= //
// ===== DASHBOARD FOLDER -- ALL SENT AND RECEIVED FILES ======= //
// ============================================================= //
    public function DASHY(){
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
        $tite_page = 'Dashboard';
	$in1="TRUE";$r1="FALSE"; $s1="TRUE";$ar1="FALSE";$rar1="FALSE";$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    }

// ============================================================== //
// ===== ALL FILES FOLDER -- ALL SENT AND RECEIVED FILES ======== //
// ============================================================== //
    public function allFiles(){
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
        $tite_page = 'All Files';
	$in1="TRUE";$r1="FALSE"; $s1="TRUE";$ar1="FALSE";$rar1="FALSE";$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    } 


// ====================================================== //
// ===== FLAGGED FOLDER -- ALL FLAGGED SENT FILES ======= //
// ====================================================== //
    public function importantFiles(){
	$SF = DB::table('bghmc_transactions')->where('IMP',1)->orderBy('updated_at','desc')->paginate(10);
        $tite_page = 'Flagged';
        $in1="TRUE";$r1="FALSE"; $s1="TRUE";$ar1="FALSE";$rar1="FALSE";$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    }

// ====================================================== //
// ===== INBOX FOLDER -- ALL RECEIVED FILES ============= //
// ====================================================== //

    public function receivedFiles(){
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
        $tite_page = 'Inbox';
        $in1="FALSE";$r1="FALSE"; $s1="FALSE";$ar1="FALSE";$rar1="FALSE";$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    }

// ======================================================== //
// ===== ARCHIVE FOLDER -- ALL ARCHIVED FILES ============= //
// ======================================================== //
    public function ArchivedFiles(){
        $SF = DB::table('bghmc_transactions')->paginate(10);
        $tite_page = 'Archived Files';
        $in1="TRUE";$r1="FALSE"; $s1="TRUE";$ar1="FALSE";$rar1="TRUE";
	$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";
	$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    }
// ====================================================== //
// =========== OUTBOX FOLDER -- ALL SENT FILES ========== //
// ====================================================== //
    public function sentFiles(){
        $SF = DB::table('bghmc_transactions')->where('sendID',Auth::user()->emp_id)->where('SenderDateArchived','=','N')->orderBy('created_at','desc')->paginate(10);
        $tite_page = 'Outbox';
        $in1="TRUE";$r1="FALSE"; $s1="TRUE";$ar1="FALSE";$rar1="FALSE";$in2="TRUE";$r2="TRUE";$s2="FALSE";$ar2="FALSE";$rar2="FALSE";$in3="TRUE";$r3="TRUE";$s3="FALSE";$ar3="FALSE";$rar3="FALSE";
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3);
    }

    public function REPORTS(){
        $userHere=Auth::user()->emp_id;
        $this->data['USERID'] = $userHere;
        
        $SF = FM::join('bghmc_filestatus','dateTNO','=','TNO')
        ->join('bghmc_filetypes','filetype_id','=','FT_ID')
        ->join('bghmc_employee_info','bghmc_employee_info.emp_id','sendID')
        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')
        ->where('dateArchiveBySend',null)
        ->orderBy('dateReply','desc')
        ->paginate(10);
        foreach ($SF as $m) {
            if($m->sendID == $userHere){
                $m->involved = "INVOLVED";
                $NAMESHIT = array(); $DEPTSHIT = array(); $ACCPTS = array();
                // GETS THE DEPARTMENT AND NAME OF RECEIVERS
                foreach(unserialize($m->REC_ID) as $sss){
                    $NameAndDEPT = DB::table('bghmc_employee_info')->where('emp_id',$sss)
                    ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
                    array_push($NAMESHIT, $NameAndDEPT->f_name . ' ' . $NameAndDEPT->l_name);
                    array_push($DEPTSHIT, $NameAndDEPT->dept_name);
                    $m->NAMESHITS = $NAMESHIT;
                    $m->DEPTSHITS = $DEPTSHIT; }

                // GETS THE ACCEPteD FILE STATUS OF EACH RECEIVER
                foreach(unserialize($m->ACCPT) as $sa){
                    if($sa == 0){   array_push($ACCPTS, 'NOT ACCEPTED'); $m->ACCPTSS = $ACCPTS; }
                    else{  array_push($ACCPTS, 'ACCEPTED'); $m->ACCPTSS = $ACCPTS; } }

                $NAMESHIT = array(); $DEPTSHIT = array(); $ACCPTS = array();
            }else{
                
                //CHECKS IF USER IS RECEIVER
                if(in_array($userHere, unserialize($m->REC_ID) )){
                    $m->involved = "INVOLVED";
                    $m->arrayNO = array_search($userHere,unserialize($m->REC_ID) );}
                else{$m->involved = "NOT INVOLVED"; $m->arrayNO = 0;}

                //CHECKS IF ACCEPTED OR NOT
                if((unserialize($m->ACCPT)[$m->arrayNO]) == 0){
                    $m->ACCPTSS = 'FILE NOT ACCEPTED';
                    $m->AcptTip = 'Accept File';
                    $m->AcptNotif = "{{route('ReceiverAccept',['track'=>$m->TNO,'rec'=>$userHere,'acpt'=>$m->ACCPTSS,'ANum'=>$m->arrayNO])}}"; 
                    $m->AcpTogle = "";
                    $m->AcptBox = 'fa fa-square-o';
                    $m->AcptNo = 0;
                    $m->AcptStyle = "color:red;";
                }else{
                    $m->ACCPTSS = 'FILE ACCEPTED';
                    $m->AcptTip = 'You have accepted the file already!';
                    $m->AcptNotif = "#AlreadyAcptNotif"; $m->AcpTogle = "modal";
                    $m->AcptBox = 'fa fa-check-square-o';
                    $m->AcptNo = 1;
                    $m->AcptStyle = "";
                }

                //CHECKS IF ARCHIVED OR NOT
                if((unserialize($m->ARCHIVED)[$m->arrayNO]) == 0){ $m->ARCH = 'NOT ARCHIVED';}
                else{ $m->ARCH = 'ARCHIVED'; }

                //CHECKS IF READ OR NOT
                if((unserialize($m->dateReadByRec)[$m->arrayNO]) == 0){ $m->ReadStyle = 'background:#ccc;'; }
                else{ $m->ReadStyle = ''; }
                
                //GETS THE FILE TYPE 
                $sadFT = DB::table('bghmc_filetypes')->where('filetype_id',$m->FT_ID)->first(); 
                $m->FT = $sadFT->filetype_name;

                if($m->ROUTESTAT == 1){
                    $m->ACCPTSS = nl2br(e('VIEW FILE FOR MORE INFORMATION -- FILE SENT AS A ROUTE'));
                    $m->AcptTip = 'VIEW FILE FOR MORE INFORMATION';
                    $m->AcptNotif = '#AcptNotif';
                }
            }
            //CHECKS IMPORTANT OR NOT
                if($m->IMP == 0){    $m->ImpStyle = ' '; }
                else{  $m->ImpStyle= 'color: red;'; }
        }


        $a = $this->data['_file'] = $SF;
        $tite_page = 'Reports';
        return $this->returnsEverything($SF,$tite_page,$in1,$in2,$in3,$in4,$r1,$r2,$r3,$r4,$s1,$s2,$s3,$s4,$ar1,$ar2,$ar3,$ar4);
        
    }
    

    public function returnsEverything($SF,$tite_page,$in1,$in2,$in3,$r1,$r2,$r3,$s1,$s2,$s3,$ar1,$ar2,$ar3,$rar1,$rar2,$rar3){
	$this->data['RECID'] = $userHere = Auth::user()->emp_id;
	$this->data['userDATA'] = $depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	$inboxCounter = 0;
        foreach ($SF as $ss){
		if($ss->sendID == $userHere){
			$inboxCounter++;		
			$ss->INVOLVED = $in1; 	
			$ss->RECEIVERS = "FALSE";	
			$ss->SENDERS = "TRUE";
			$ss->TRACKS = "TRUE";
			if($ss->SenderDateArchived != 'N'){$ss->SenderArchive = "TRUE";}else{$ss->SenderArchive = "FALSE";}
			$ss->RecArchive = "FALSE";}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = $in2;
				$ss->RECEIVERS = "TRUE";	
				$ss->SENDERS = "FALSE";
				$ss->TRACKS = "TRUE";
				if($ss->SenderDateArchived != 'N'){$ss->SenderArchive = "TRUE"; $ss->RecArchive = "TRUE";}
				else{$ss->SenderArchive = "FALSE"; $ss->RecArchive = "FALSE";}
			}else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = $in2;
						$ss->RECEIVERS = "TRUE";	
						$ss->SENDERS = "FALSE";
						$ss->TRACKS = "TRUE";
						if($ss->SenderDateArchived != 'N'){$ss->SenderArchive = "TRUE";}else{$ss->SenderArchive = "FALSE";}
						if($b['dateArchived'] == "N"){$ss->RecArchive = 'FALSE';}else{$ss->RecArchive = 'TRUE';}
						
						break 2;
					}else{
						$ss->INVOLVED = 'FALSE';
						$ss->TRACKS = "FALSE";
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';
						$ss->SenderArchive = 'FALSE';
						$ss->RecArchive = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: #000;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;

		if($ss->updateStatus == $ss->updated_at){$ss->updatedStatus = 'background:#c0c0c0;';}
		else{$ss->updatedStatus = 'background:none;';}
        } 
        $this->data['inboxCounter'] = $inboxCounter;
        $SFTRACKINGS = 's';
	
	$this->data['_file'] = $SF;
        $this->data['SFF'] = $SFTRACKINGS;

        return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));

    }
  


// ================== END ==================
}
