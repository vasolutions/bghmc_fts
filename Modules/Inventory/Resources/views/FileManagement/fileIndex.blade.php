 @extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script>
@stop

@section('pagename')
    
    <span style="font-size: 25px;">
        @include('inventory::includes._titleFILE')
    </span>
@stop

@section('content')
<style>
#ACCPTREC tbody td #statuses,
#tblSentFiles tbody td #statuses
	{min-height:80px; max-height:200px; overflow-y:scroll; padding-left:1%;}
</style>
    <div class="content-wrapper">
        <section class="content">
        @if(($tite_page)!='Reports')
        @include('inventory::includes._ALL')
        @endif
            <div class="box">
                @if(Session::has('successMSG'))
                  <div class="boxAlert alertSessions" id="sMSG">
                    <span> {{Session::get('successMSG')}}</span><br>
                    <button class="btn btn-new pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(Session::has('errorMSG'))
                  <div class="boxAlert alertSessions" id="eMSG">
                    <span> {{Session::get('errorMSG')}}</span><br>
                    <button class="btn btn-danger pull-right" onclick="$('.alertSessions').hide();">CLOSE</button>
                  </div>
                @endif
                @if(($tite_page)!='Reports')
                <div class="box-header">
                    <div id="NOTIFICATIONSSTUFF" >
                        @foreach($_file as $z1)
                            @if($z1->sendID == Auth::user()->emp_id)

                            @else <div class="alertSession"></div> @endif
                        @endforeach
                    </div>
                    
                    <ul class="pull-right" style="list-style-type: none; display: flex; align-items:center; ">
                        <li style="padding-top:5px;">
                            <div style="text-align:right; margin-right: 20px;" class="searchs">
                                    <input type="text" id="searchUL" onkeyup="searchFunctions()" placeholder="Track file.." title="Enter Tracking No.">
                            </div>
                        </li>
                        <li>

                        </li>
                        <li style="padding-top: 15px;" >
                            <b id="ComposeLI">
                            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file"><i class="glyphicon glyphicon-pencil" ></i> &nbsp; Compose</button></b>
                    		<b id="searchLI" style="display:none;">
                    		<button type="button" class="btn btn-new" id="searchBtn"><i class="fa fa-search" ></i> &nbsp;Search</button>
                    		</b>
                        </li>
                    </ul>
                    

                </div>
<!-- ============== TRACKING FILES CODE SHIT ================ -->
                <h5 class="filesNotice" id="hideThis" style="color:red; font-style: italic;">NOTE: You can search using the tracking number or the subject name!<br>Press on the "Search" button to display your search results </h5>
                <div id="hideThis" class="showsLISTS">
                    <div id="TRAckFiles">
                        <table id="searchTBL" class="table table-striped table-bordered table-hover">
			  @foreach($_file as $s)
                          @if($s->TRACKS == 'TRUE')
				<?php if($s->RECEIVERS == "TRUE" || $s->SENDERS == "TRUE"){ ?>
				<tr>
					<td style="font-size:15px;">
						@if($s->RecArchive == 'TRUE') <h2 style="color:red;">FILE ARCHIVED</h2> @endif
						<b>TRACKING NO:</b> {{$s->transNO}}<br>
						<b>DOCUMENT: </b> {{strtoupper($s->docTYPE)}}<br>
						<?php if($s->RECEIVERS == 'TRUE'){ ?>
							<b>DATE RECEIVED: </b>{{$s->SenderDateSENT}}<br>
							<a href="#" id="mors" style="font-weight:800; display:none;">--- SHOW MORE ----</a>
							<div id="mores00">
							<?php echo "<b>SENDER: </b> $s->SenderDept | $s->SenderName <br>"; 
						      	if($s->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}
						      	else{echo"<b>NOT IMPORTANT</b>";}
						}elseif($s->SENDERS == 'TRUE'){ ?>
							<b>DATE SENT: </b>{{$s->SenderDateSENT}}<br>
							<a href="#" id="mors" style="font-weight:800; display:none;">--- SHOW MORE ----</a>
							<div id="mores00">
							<?php 
						      	if($s->recTYPE == "PERSONAL MESSAGE"){echo "<b>Sent as ".$s->recTYPE."</b><br><b>RECEIVER:</b> ".unserialize($s->fileSTATS);}
						      	else if(strpos($s->recTYPE, "FORWARDED") !== false){echo $s->recTYPE."</b>";}
						      	else{echo "<b>Sent to ".$s->recTYPE."</b><br>";}
						      	if($s->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}
						      	else{echo"<b>NOT IMPORTANT</b>";}
						}	?>
						<br><b>SUBJECT: </b>{{strtoupper($s->SUBJ)}}
						<br><a href= <?php  if($s->attachNAME != "NO ATTACHMENT"){ $a = "Download File";?>
							"{{route('DownloadFile',['tno'=>$s->transNO,'attachName'=>$s->attachNAME,'sender'=>$s->sendID]) }}"
				   		<?php }else{ $a = "No file to Download"?>
							"#NoDownloadFileAlert" data-toggle='modal'
				   		<?php }?>
						class='btn btn-link stuff' style='color:#000; margin-left:0px; padding-left:0px;'>
						<span><b>ATTACHMENT: </b>{{$s->attachNAME}}</span>
						<span class='hoverstext'>{{$a}}</span></a><br>
						<a href="#" id="lers" style="font-weight:800; display:none;">--- SHOW LESS ----</a>
						</div>
						<?php if($s->recTYPE == "ALL DEPARTMENTS"){ echo "No Hard Documents";
		  				      }elseif($s->recTYPE != "PERSONAL MESSAGE"){
				   			foreach(unserialize($s->fileSTATS) as $a){
			  					foreach($a as $b){
					   				echo "<b>".$b['EmpDEPT']."</b><br>";
					   				if($s->hardCOPY == 'NO'){echo "&nbsp; No Hard Copy Sent <br>";}
					   				else{echo "&nbsp; Hard Documents: <br>";
										foreach (array_keys($b['Documents']) as $c){
						   					echo "&nbsp;  &nbsp; ".$c.": ";
						   					if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; Not Accepted<br>";}
						   					else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
										}
					   				}
			  					}
			 	   			}
		  					}else{echo "No Hard Documents for Personal Message";}	
						?>
					</td>
					<td style="font-size:15px;">
						@if($s->RECEIVERS == "TRUE")
						<a href="{{route('SpecificFileReceiver',['track'=>$s->transNO,'rec'=>$userHere,'sender'=>$s->SenderDept.' | '.$s->SenderName]) }}" class="btn btn-block btn-primary pull-right" style="width:200px; border-bottom:4px solid #000; text-align:center; color:#000; font-weight:900; ">OPEN</a>
						@elseif($s->SENDERS == "TRUE")
						<a href="{{route('SpecificFileSender',['transNO'=>$s->transNO])}}" class="btn btn-block btn-primary pull-right" style="width:200px; border-bottom:4px solid #000; text-align:center; color:#000; font-weight:900; ">OPEN</a>
						@endif
						<hr>
						<span class="pull-left" style="margin-bottom:2%; width:100%;"><h3 style="font-weight:800;">TRACKING STATUS</h3></span>
						<div id="MOREsecondCOLS00">
						<?php
                  					$messages = nl2br($s->trackingSTATS);
                    					if($messages == ''){
                      						echo "<i style='font-size:15px;' class='replyThisOne'> NO STATUS </i>";
                    					}else{
                      						echo "<span style='font-size:15px;' class='replyThisOne'>";
                      						echo $messages;
                      						echo '</span>';
                    					}
                				?>
						</div>
					</td>
				</tr>
				<?php } ?>
			  @endif
   			  @endforeach
                        </table>
                        
                    </div>
                    
                </div> <!-- ================== END ================ -->
                @endif
                <div class="sss">
		    @if(($tite_page) == 'Archived Files')
			@include('inventory::FileManagement.archivedFiles')
                    @elseif(($tite_page) != 'Reports')
			@include('inventory::FileManagement._dashyBoardy') 
                    @else
                        @include('inventory::FileManagement._repos')
                    @endif
                </div>
            </div>
                    
        </section>
    </div>
    <script type="text/javascript">
	$(document).ready(function(){$("#TRAckFiles").load(location.href+" #TRAckFiles>*",""); });
	function searchFunctions() {
	$('#mors').click(function(){
		$('#secondCOLS').hide('slow');
		$('#mores').show('slow');
		$('#mors').hide('slow');
		$('#MOREsecondCOLS').show('slow');
  	});
  	$('#lers').click(function(){
		$('#mores').hide('slow');
		$('#secondCOLS').show('slow');
		$('#mors').show('slow');
		$('#MOREsecondCOLS').hide('slow');
  	});
          var input, filter, table, tr, td, i;
          input = document.getElementById("searchUL");
          filter = input.value.toUpperCase();
          table = document.getElementById("searchTBL");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) { if (td.innerHTML.toUpperCase().indexOf(filter) > -1) { tr[i].style.display = ""; } 
              else { tr[i].style.display = "none"; }
            }       
          }
	}
    </script>
@stop

@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script> -->
    <!-- <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.js"></script>
   <script src="{{asset('includes-jquery')}}/jquery.twbsPagination.min.js"></script>
   <script src="{{asset('includes-jquery')}}/Gruntfile.js"></script>

@stop 