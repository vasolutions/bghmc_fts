<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class AcceptFileController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;

        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
            $allDepts = DB::table('bghmc_departments')->get();
            $this->data['deptRecs'] = $allDepts;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;

        return $this->data;

    }


// ========================================= //
// ===== USERS ACCEPTS A FILE DOCUMENT ===== //
// ========================================= //

    public function AcceptFile($track,$docNAME,$dept){
	$a0 = TM::where('transNO',$track)->first();
	$b0 = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$stats = unserialize($a0->fileSTATS) ;
	$ctrs = 0;
	foreach($stats as $a){
		foreach($a as $b){
			if($b['EmpDEPT'] == $dept){	
				if($b['Documents'][$docNAME] == "N"){
					$stats[$ctrs][$b0->dept_id]['Documents'][$docNAME] = "Accepted by ".$b0->f_name." ".$b0->l_name."(".Carbon::now()->toDayDateTimeString().")";
				}
			}
			$ctrs++;
		}
	}
	$b1 = $b0->l_name.' of '.$b0->dept_name. ' accepted a file. (' .Carbon::now()->toDayDateTimeString().')'; 
        $b2 = $a0->trackingSTATS . "\n" . $b1;
        TM::where('transNO',$track)->update(['fileSTATS'=>serialize($stats), 'trackingSTATS'=>$b2,'updateStatus'=>Carbon::now()]);
        return redirect()->back()->with('successMSG','Reply Sent ');
    }
// ==================== END ======================//



} // ================== END ==================
