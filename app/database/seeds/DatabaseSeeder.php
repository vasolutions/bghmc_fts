<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 3;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('disease_info')->insert([ //,
                'f_name' => $faker->name,
                'l_name' => $faker->name,
                'm_name' => $faker->name,
                'date_birth' => $faker->date,
                'gender' => $faker,
                'civil_status' => $faker,
            ]);
        }// $this->call(UsersTableSeeder::class);
    }
}
