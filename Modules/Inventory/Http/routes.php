<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'inventory', 'namespace' => 'Modules\Inventory\Http\Controllers'], function()
{
    Route::get('/', 'InventoryController@index')->name("inventory.index");
    // Route::resource('EveryFiles','EveryFilesController');
    // Route::resource('Forward','InventoryController');
    // Route::resource('Archive','FileArchiveController');
    
    // ===== DISPLAY ACCOUNT =====
    Route::get('/mine/ACCOUNT_KO_TO','HomeController@homePage')->name('DisplayAccount');
    
    

// ======================================================================================================
// ===== BOTH RECEIVER AND SENDER ACTIONS ======
// ======================================================================================================
    // = 1. HOME PAGE =
    Route::get('/mine/HOME','HomeController@homePage')->name('DisplayHomePage');
    // = 2. CREATE FILE =
    Route::resource('Conversations','ConvFilesController');
    // = 3. DISPLAYS ALL FILES
    Route::get('/mine/INBOX','EveryFilesController@receivedFiles')->name('display.RECEIVED'); // RECEIVED FILES
    Route::get('/mine/OUTBOX','EveryFilesController@sentFiles')->name('display.SENT'); // SENT FILES
    Route::get('/mine/MESSAGES','EveryFilesController@allFiles')->name('display.ALL'); // ALL FILES
    Route::get('/mine/IMPORTANT','EveryFilesController@importantFiles')->name('display.IMPS'); // IMPORTANT FILES
    Route::get('/mine/ARCHIVED','EveryFilesController@ArchivedFiles')->name('display.ARCHIVE'); // ARCHIVED FILES
    Route::get('/mine/REPORTS','EveryFilesController@REPORTS')->name('display.REPORTS'); // REPORT FILES
    Route::get('/mine/DASHY','EveryFilesController@DASHY')->name('displaySdashboard'); // DASHBOARD
    
    // report view of admin -- new
    Route::get('/ReportGeneration','ReportPrintController@viewReport')->name('admin.REPORTS'); // REPORT FILES
    Route::get('/ReportGeneration/view', array('uses' => 'ReportPrintController@GenerateReports'))->name('admin.gREPORTS');
    Route::get('/ReportGeneration/view/print', array('uses' => 'ReportPrintController@PrintReports'))->name('admin.pREPORTS');
    Route::get('/ReportGeneration/view/browser', array('uses' => 'ReportPrintController@ViewReportinBrowser'))->name('admin.vREPORTS');
    


// ======================================================================================================
// ===== SENDER ACTIONS ======
// ======================================================================================================
    // = 01. VIEW SPECIFIC FILE =
    Route::get('SentFile/{tno}', ['as' => 'SpecificFileSender', 'uses' => 'SpecificFileSenderController@SpecificFileSender']);
    // = 02. MARK FILE IMPORTANT/NOT =
    Route::get('IMPORTANT/{TNO}', ['as' => 'MarkImp', 'uses' => 'FlagController@MarkFileImportant']);

    

    // = 04. RESTORE FILE =
    Route::get('SenderRestore/{tno}/{sender}', ['as' => 'senderRestore', 'uses' => 'FileArchiveController@SenderRestore']);
    // = 05. CHANGE ROUTE OF RECEIVERS =
    Route::post('FileRoute/{tno}/{sender}', ['as' => 'SendChangeRoute', 'uses' => 'SpecificFileSenderController@SendChangesRoute']);
    // = 06. REMOVE NOTIFICATION SENT BY ROUTE PROBLEM
    Route::get('FileRouteNotif/{tno}', ['as' => 'CheckNotif', 'uses' => 'SpecificFileSenderController@CheckNotifs']);
    // = 4. GENERATE REPORTS =
    //Route::get('/mine/Generate','SpecificFileSenderController@DASHY')->name('GenerateReport'); // GenerateReport
    Route::post('REPORTS/Generate', ['as' => 'GenerateReport', 'uses' => 'ReportPrintController@GenerateReports']);
    

// ======================================================================================================
// ===== RECEIVER ACTIONS ======
// ======================================================================================================
    // = 6. VIEW SPECIFIC FILES =
      // = 6A. NORMAL FILE =
      Route::get('ViewFile/{track}/user/{rec}/{sender}', ['as' => 'SpecificFileReceiver', 'uses' => 'SpecificFileController@SpecificFileReceiver']);
      // = 6B. ROUTE FILE =
      Route::get('FileRoute/{track}/user/{rec}/{acpt}/{ANum}', ['as' => 'SpecificFileReceiverInARoute', 'uses' => 'SpecificFileController@SpecificFileReceiverInARoute']);  

    // ====== FORWARD ROUTE DOCUMENTS FILE ====== //
    	Route::get('ForwardDocuments/{track}/{deptName}/{deptID}/{NUM}', ['as' => 'forwardDocs', 'uses' => 'ForwardDocumentController@ForwardDocument']);    

    // ====== FORWARD FILE ====== //
    	Route::post('ForwardByRec/{track}/user/{forwRec}', ['as' => 'forwardByRec', 'uses' => 'ForwardController@ForwardByReceiver']);

    // ====== REPLY ============= //
    	Route::post('Reply/{track}/user', ['as' => 'ReplyShit', 'uses' => 'ReplyController@RepyShit']);

    // ====== ACCEPT FILE ======= //
	Route::get('Accept/NO/{track}/DOC/{docNAME}/DEPARTMENT/{dept}', ['as' => 'AcceptFiles', 'uses' => 'AcceptFileController@AcceptFile']);

    // ====== DOWNLOAD FILE ===== //
    	Route::get('Download/{tno}/{attachName}/{sender}', ['as' => 'DownloadFile', 'uses' => 'DLController@DownloadFile']);

    // = 9. CHANGE ROUTE OF THE FILE AND NOTIFIES THE SENDER
    Route::post('FileRoute/{tno}/user/{rec}/ChangeRouteReceiver', ['as' => 'changeROUTErec', 'uses' => 'SpecificFileController@ChangeRouteRec']);
    Route::post('FileRoute/{tno}/user/{rec}/ChangeRouteReceiverNotify',['as'=>'changeROUTErecNotif','uses'=>'SpecificFileController@ChangeRouteRecNotif']);

    // ====== ARCHIVE FILE ============= //
    Route::get('ArchivesFile/{tno}/{titles}', ['as' => 'ArchiveFiles', 'uses' => 'FileArchiveController@ArchiveFile']);
    // ====== RESTORE FILE ============= //
    Route::get('Restore/{transNO}/{titles}', ['as' => 'RestoreFiles', 'uses' => 'FileArchiveController@RestoreFile']);

// ======================================================================================================
// ======================================================================================================
// ======================================================================================================    

    // ===== VIEW SPECIFIC FILE SENDER =====
    

});