<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

use Modules\Administrator\Entities\PersonnelModel as PM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class PersonnelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Personnel';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $this->data['index'] = 0;
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $emp_info = DB::table('bghmc_employee_info')->get(); 

        foreach($emp_info as $i){
            // change all position id and department id into names
            $pos_info = DB::table('bghmc_positions')->WHERE('pos_id', $i->pos_id)->first();
            $i->pos_id = $pos_info->pos_name;
            $dept_info = DB::table('bghmc_departments')->WHERE('dept_id', $i->dept_id)->first();
            $i->dept_id = $dept_info->dept_name;
        }
        $this->data['info'] = $emp_info;
        $this->data['depts'] = DB::table('bghmc_departments')->get();
        $this->data['pos'] = DB::table('bghmc_positions')->get();
        $this->data['hash'] = Hash::make('root');

        return view('administrator::personnel_index', $this->setup());
    }

    public function showAccount($id){
        $emp_info = DB::table('bghmc_employee_info')->WHERE('emp_id', $id)->first();
        
        $this->data['iposid'] = $emp_info->pos_id;
        $this->data['ideptid'] = $emp_info->dept_id;

        $pos_info = DB::table('bghmc_positions')->WHERE('pos_id', $emp_info->pos_id)->first();
        $dep_info = DB::table('bghmc_departments')->WHERE('dept_id', $emp_info->dept_id)->first();
        $emp_info->pos_id = $pos_info->pos_name;
        $emp_info->dept_id = $dep_info->dept_name;

        $this->data['info'] = $emp_info;
        $this->data['depts'] = DB::table('bghmc_departments')->get();
        $this->data['pos'] = DB::table('bghmc_positions')->get();
        $this->data['act'] = DB::table('bghmc_account_activity')->WHERE('emp_id', $id)->orderBy('created_at', 'asc')->get();
        $this->data['backroute'] = 'admin.accounts_list';
        $this->data['passwordhistory'] = DB::table('bghmc_emp_credentials_reset')->where('emp_id', $id)->orderBy('created_at', 'desc')->get();
        return view('administrator::single_personnel', $this->setup());
    }

    public function register_personnel(Request $request){
        $data = $request->all();

        // check if employee id is present already
        $is_existing = DB::table('bghmc_employee_info')->WHERE('emp_id', $request->input('empid'))->first();

        $PM = new PM; // access Personnel Model for the database tables
        if($PM->validate($data, 'register_personnel')) // validate if all fields are correct
        {
            if($is_existing){ //check if emp id is already present
                $data['status'] = 0;
                $data['errors']['message'] = "Account Already Registered";
            }
            else{
                $PM->setInfo($request); //Proceeds to Personnel Model to process input
                $data['status'] = 1;
                $data['errors']['message'] = 'Account successfully added';
            }
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        // Return to modal if success or fail
       return $data;
    }

    public function update_personnel(Request $request){
        $data = $request->all();

        $PM = new PM; // access Personnel Model for the database tables

        if($PM->validate($data, 'update_personnel')) // validate if all fields are correct
        {
            $PM->updateInfo($request); //Proceeds to Personnel Model to process input
            $data['status'] = 1;
            $data['errors']['message'] = 'Account successfully updated';
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        // Return to modal if success or fail
       return $data;
    }

    public function update_activity(Request $request){
        $data = $request->all();
        // $willvalidate = '';
        
        $PM = new PM;
        // if($request->act == "deactivate"){$willvalidate = 'deactivate';}

        if($PM->validate($data, $request->act)){
            $PM->updateActivityInfo($request);
            $data['status'] = 1;
            $data['errors']['message'] = 'Account successfully ' . $request->act . 'd';
        }
        else{
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        
        return $data;
    }

    public function password_change(){
        $this->data['empid'] = Session::get('bghmcuser')->emp_id;
        return view('template::top-nav-pages.password_change',$this->setup());
    }

    public function password_change_post(Request $request){
        $data = $request->all();
        $whatreallydata = "";
        $PM = new PM; // access Personnel Model for the database tables

        if($PM->validate($data, 'change_password')) // validate if all fields are correct
        {
            $hasCurrentSecu = DB::table('bghmc_emp_credentials_secu')->WHERE('emp_id', $request->input('empid'))->WHERE('iscurrent', 1)->first();
            
            if(count($hasCurrentSecu) != 0){
                $PM->changePassword($request); //Proceeds to Personnel Model to process input
            }
            else{
                if(empty($request->input('secuans_1')) || empty($request->input('secuans_2'))){
                    $data['status'] = 2;
                    $data['errors'] = "Security Answers can not be empty";
                }
                else{
                    $PM->changePassword($request);
                }
            }
            $data['status'] = 1;
            $data['errors']['message'] = 'Account successfully updated';
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        $data['accnttype'] = $PM->isAdmin($request);
        return $data;
    }

    public function password_admin_reset(Request $request){
        $data = $request->all();
        $PM = new PM;
        if($PM->adminChangePassword($request)){
            $data['status'] = 1;
            $data['errors']['message'] = "Account password & security questions reset done.";
        }
        else{
            $data['status'] = 0;
            $data['errors'] = $PM->errors();
        }
        return $data;
    }
}
