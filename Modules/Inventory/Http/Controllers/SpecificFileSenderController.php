<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class SpecificFileSenderController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;

        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
            $allDepts = DB::table('bghmc_employee_info')
	    	->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
	    	$deptArray=[];  $dArray=[];
	    	foreach ($allDepts as $sadShit){array_push($deptArray,$sadShit->dept_id); $dArray[$sadShit->dept_id]=$sadShit->dept_name;}
	        $this->data['deptRecs'] = $dArray;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;

        return $this->data;

    }
// ========================================================= //
// ===== SENDER VIEWS THE SPECIFIC FILE ==================== //
// ===== WILL REFLECT IN FILESPECIFICSENDER.BLADE.PHP ====== //
// ========================================================= //
    public function SpecificFileSender($tno){
	TM::where('transNO',$tno)->update(['transNO'=>$tno]);
	return $this->SpecificFileSender2($tno);
    }
    public function SpecificFileSender2($tno){
        $this->data['a0'] = $a0 = TM::where('transNO',$tno)->first();
        $this->data['THREADS'] = $a0->THREADMSG;
	$this->data['depts'] = $depts = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	// FILE IMPORTANCE
        // SETS THE MODAL AND STYLE OF THE TEXT IF FILE IS IMPORTANT OR NOT
        if($a0->IMP == 1){
            $this->data['FlagModal'] = "DO YOU REALLY WANT TO MAKE THIS FILE AS NOT IMPORTANT?";
            $this->data['FlagWord'] ="Remove Flag";
            $this->data['IMP'] ='IMPORTANT FILE';
            $this->data['FlagLogo'] ="fa fa-flag";
        }else{
            $this->data['FlagModal'] = "DO YOU REALLY WANT TO MAKE THIS FILE AS IMPORTANT?";
            $this->data['FlagWord'] ="Set Flag";
            $this->data['IMP'] ="";
            $this->data['FlagLogo'] ="fa fa-flag-o";
        }
	
	$DEPTnames = array();
	if($a0->recTYPE != "PERSONAL MESSAGE"){
	foreach(unserialize($a0->fileSTATS) as $a){
		foreach($a as $b){
			array_push($DEPTnames,$b['EmpDEPT']);
		}
	}}
	
	
	$this->data['DEPTnames'] = $DEPTnames;
        // FILE ATTACHMENT
	// SETS THE MODAL AND TEXT IF FILE HAS ATTACHMENT OR NONE
        if($a0->attachNAME == "NO ATTACHMENT"){
            $this->data['AttachTip'] = "No File Attached.";   
            $this->data['AttachTipModal'] = "#NoDownloadFileAlert";
        }else{
            $this->data['AttachTip'] = "Download File";
            $this->data['AttachTipModal'] = "#DownloadFileAlert"; 
        }

        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;

        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)
            ->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
            $this->data['emps2'] = $emps2;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;
            
            $allDepts = DB::table('bghmc_employee_info')
	    	->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
	    	$deptArray=[];  $dArray=[];
	    	foreach ($allDepts as $sadShit){array_push($deptArray,$sadShit->dept_id); $dArray[$sadShit->dept_id]=$sadShit->dept_name;}
	        $this->data['deptRecs'] = $dArray;

        
        return view('inventory::FileManagement.fileSpecificSender',$this->setup());
        
    }
// ==================================================== //
// ===== CHANGES ROUTE INFORMATION ==================== //
// ==================================================== //
    public function SendChangesRoute($tno, $sender, Request $request){
        $a0= TM::where('transNO',$tno)->first();
        $b0 = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $b1 = $b0->l_name.' of '.$b0->dept_name. ' changed the route (set of receivers). (' .Carbon::now()->toDayDateTimeString().')'; 
        $b2 = $a0->trackingSTATS . "\n" . $b1;

	$nameReplacement = $request['USERTOCHANGEROUTE'];	// returns the name of department for replacement/ additional
	$positionNo = $request['USERCHANGEPOSITION']; 		// returns the position of change - if they want to change user at index 0, 0 will return
	$oldSTATS = unserialize($a0->fileSTATS);
	$newSTATS = array();
	if($request['SelectActionForChange'] == 'change'){
		$nameReplacement = $request['userchangeing'];
		$positionNo = $request['UserChangesHerePosition'];}
	if($request['SelectActionForChange'] == 'remove'){$nameReplacement = $request['UserRemove'];}
	
	$newKEY = DB::table('bghmc_departments')->where('dept_name',$nameReplacement)->first();
	$copySTATS = end($oldSTATS);
	$key = implode(",",array_keys($copySTATS));
	if($request['SelectActionForChange'] != 'remove'){
		if($key == $newKEY->dept_id){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
		$copySTATS[$key]['EmpDEPT'] = $nameReplacement;
		$newSTATS[$newKEY->dept_id] = $copySTATS[$key];
	}

	
        if($request['SelectActionForChange'] == 'none'){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
        elseif($request['SelectActionForChange'] == 'add'){
            if(($nameReplacement == 'none')||($nameReplacement == 'none')){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
            if($positionNo == "last"){
		if($key == $newKEY->dept_id){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
		foreach($oldSTATS[count($oldSTATS)-1] as $a){if($a['dateAccepted'] != "N" || $a['dateRouteForwarded'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
		array_push($oldSTATS,$newSTATS);
            }else{
		if($positionNo == 0){
			foreach($oldSTATS[$positionNo] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
			foreach($oldSTATS[$positionNo+1] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
		}else{
			foreach($oldSTATS[$positionNo-1] as $a){
				if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
				if($a['dateAccepted'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
			}
			if($positionNo != count($oldSTATS)-1){foreach($oldSTATS[$positionNo+1] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}}
		}
		$afterPlace = array_slice($oldSTATS,$positionNo);
		$beforePlace = array_slice($oldSTATS,0,$positionNo);
		array_push($beforePlace,$newSTATS);
		foreach($afterPlace as $a){array_push($beforePlace,$a);}
		$oldSTATS = $beforePlace;
	    }
            TM::where('transNO',$tno)->update(['fileSTATS'=>serialize($oldSTATS), 'trackingSTATS'=>$b2,'updateStatus'=>Carbon::now()]);
            return redirect()->back()->with('successMSG','Receiver was successfully added to the route');

        }elseif($request['SelectActionForChange'] == 'change'){
		if($positionNo == 0){
			foreach($oldSTATS[$positionNo] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
			foreach($oldSTATS[$positionNo+1] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
		}else{
			foreach($oldSTATS[$positionNo-1] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}
			if($positionNo != count($oldSTATS)-1){foreach($oldSTATS[$positionNo+1] as $a){if($a['EmpDEPT'] == $nameReplacement){return redirect()->back()->with('successMSG','CHANGES FAILED!');}}}
		}
		foreach($oldSTATS[$positionNo] as $a){
			if($a['dateAccepted'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
			if($a['dateRouteForwarded'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
		}
	    $oldSTATS[$positionNo] = $newSTATS;
            TM::where('transNO',$tno)->update(['fileSTATS'=>serialize($oldSTATS), 'trackingSTATS'=>$b2,'updateStatus'=>Carbon::now()]);
            return redirect()->back()->with('successMSG','Route of receivers was successfull changed');
        }elseif($request['SelectActionForChange'] == 'remove'){
		foreach($oldSTATS[$nameReplacement] as $a){
			if($a['dateAccepted'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
			if($a['dateRouteForwarded'] != "N"){return redirect()->back()->with('successMSG','CHANGES FAILED!');}
		}
	    unset($oldSTATS[$nameReplacement]);
            TM::where('transNO',$tno)->update(['fileSTATS'=>serialize($oldSTATS), 'trackingSTATS'=>$b2,'updateStatus'=>Carbon::now()]);
            return redirect()->back()->with('successMSG','Receiver was successfully removed from the route');
        }else{}
    }

// SENDER CHECKS THE NOTIFICATION SENT BY THE RECEIVER
	public function CheckNotifs($tno){
		$a0 = FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO')->first();
		FM::where('TNO',$tno)->join('bghmc_filestatus','dateTNO','TNO')->update(['pendingRouteRec'=>Null]);
		
		return redirect()->route('SpecificFileSender',['tno'=>$tno]);
		
	}

// ADMIN GENERATES REPORT
	public function GenerateReports(Request $request){
        $data['generatetype'] = "received";
        $data['files'] = "";
        $getFiles = "";

        // Get User info
        $userID = Auth::user()->emp_id;
		$user = DB::table('bghmc_employee_info')->where('emp_id',$userID)
            		->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $data['$user_info'] = $user->f_name." " .$user->l_name.' of '.$user->dept_name;
        

        // File Tracking Condition
        if($request->input('selectGenerate') == 'filesReceived'){
            $data['generatetype'] = 'received';
            if($request->input('selectTimeline') == 'sdays'){
                $getFiles = DB::table('bghmc_allfiles')
                		->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                                ->where('dateReply', '>=', \Carbon\Carbon::today()->toDateString())
                                ->where('REC_ID', 'LIKE', '%'.$userID.'%')
                                ->get();
            }
            else if($request->input('selectTimeline') == 'sweeks'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('REC_ID', 'LIKE', '%'.$userID.'%')
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->startOfWeek()->toDateString())
                            ->where('dateReply', '<=', \Carbon\Carbon::now()->endOfWeek()->toDateString())
                            ->get();
            }
            else if($request->input('selectTimeline') == 'smonths'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('REC_ID', 'LIKE', '%'.$userID.'%')
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->month)
                            ->get();
            }
            else if($request->input('selectTimeline') == 'syears'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('REC_ID', 'LIKE', '%'.$userID.'%')
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->year)
                            ->get();
            }
        }
        else if($request->input('selectGenerate') == 'actionTaken'){
            $data['generatetype'] = 'action';
        }
        else if($request->input('selectGenerate') == 'sentDocs'){
            $data['generatetype'] = 'sent';
            if($request->input('selectTimeline') == 'sdays'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('SEND_ID', 'LIKE', $userID)
                            ->where('dateReply', '>=', \Carbon\Carbon::today()->toDateString())
                            ->get();
                            
            }
            else if($request->input('selectTimeline') == 'sweeks'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('SEND_ID', 'LIKE', $userID)
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->startOfWeek()->toDateString())
                            ->where('dateReply', '<=', \Carbon\Carbon::now()->endOfWeek()->toDateString())
                            ->get();
                   
            }
            else if($request->input('selectTimeline') == 'smonths'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('SEND_ID', 'LIKE',  $userID)
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->month)
                            ->get();
                 
            }
            else if($request->input('selectTimeline') == 'syears'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')
                            ->where('SEND_ID', 'LIKE',  $userID)
                            ->where('dateReply', '>=', \Carbon\Carbon::now()->year)
                            ->get();
                  
            }
        }
        foreach($getFiles as $f){
            $f->ACCPT = unserialize($f->ACCPT);
            $f->FT_ID = DB::table('bghmc_filetypes')
                    ->where('filetype_id', $f->FT_ID)
                    ->first()->filetype_name;
            $f->dateReply = \Carbon\Carbon::parse($f->dateReply)->format('M d Y h:i A');
        }

        $data['files'] = $getFiles;
	return $data;
    	}
// ================== END ==================
}
