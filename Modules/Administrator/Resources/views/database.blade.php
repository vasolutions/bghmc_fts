@extends('template::admin-pages.menus.'.$template['menu']) 

@section('pagename')
    <span style="font-size: 25px;">Database Backup / Restore</span>
@stop

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <h4><b>BACKUP Database</b></h4>
                    <form action="{{ route('db.dbfunctions') }}" role="form" id="backupdbform" name="backupdbform" method="post" >
                        {{ csrf_field() }}
                        <p>( Default export type is .SQL file )</p>
                        <p>Choose export type:&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="exp_type" value="false" class="exp_type"> SQL &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="exp_type" value="true" class="exp_type"> GZIP COMPRESSED
                        </p>
                        <!-- <p>Choose location to save DB File: &nbsp;&nbsp;&nbsp;&nbsp;
                        <label for="filepath" style="background-color: initial; padding: 3px 8px; border: 0.5px solid #333; " >Select directory</label></p>
                        <input id="filepath" type="file" webkitdirectory directory onchange="getfolder(event)" style="display: none;" /> -->
                        <p></p>
                        <input type="hidden" name="db_type" value="db_export">
                        <input type="submit" value="Export" class="btn-db">
                        <!-- <button class="btn-db" onclick="$(this).dbfunctionexport();";>EXPORT</button> -->
                        <br>
                    </form>
                    <!-- <br> -->
                    
                    @if(Session::get('exportmess') != "")
                        <span class="db_mess" style="color: green; padding: 10px 0;"><i class="fa fa-check"></i> {{Session::get('exportmess')}}</span>
                    @endif
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <h4><b>RESTORE Database</b></h4>
                    <p style="color: red;">Please take note that importing a database file will permanently override the current database running as of the moment. <br> 
                    It is advisable that you export a back up first before proceeding. <br> Import at your own risk.</p>
                    <form file="true" enctype="multipart/form-data" action="{{ route('db.dbfunctions') }}" role="form" id="restoredbform" name="restoredbform" method="post">
                        {{ csrf_field() }}
                        <input type="file" name="getdbfile"><br>
                        <input type="hidden" name="db_type" value="db_import">
                        <input type="submit" value="Import" class="btn-db">
                        <!-- <button class="btn-db" onclick="$(this).dbfunctionimport('db_import');">IMPORT</button> -->
                        <br>
                    </form>
                    <!-- <br> -->
                    
                    @if(Session::get('importmess') != "")
                        <span class="db_mess" style="color: green; padding: 10px 0;"><i class="fa fa-check"></i> {{Session::get('importmess')}}</span>
                    @endif
                </div>
            </div>
        </section>
    </div>

@stop

@section('plugins-script')
    <script>
        // var loadFile = function(event) {
        //     console.log(URL.createObjectURL(event.target.files[0]));
        // };

        // $.fn.dbfunctionexport = function($dbtype){
        //     // $export_type = $('input[name=exp_type]:checked').val();
            
        //     $.ajax({
        //         type : 'POST',
        //         url : "{{ route('db.dbfunctions') }}", //from routes
        //         data: $('#backupdbform').serialize(),
        //         // dataType : 'json',
        //         error : function(){
        //             alert('error');
        //         },
        //         success : function(data){
        //             $('.db_mess').show();
        //             $('.db_mess').text(data["mess"]);
        //             console.log(data);
        //         }
        //     });
        // };
        // $.fn.dbfunctionimport = function($dbtype){
        //     $export_type = $('#getdbfile').val();
        //     $.ajax({
        //         type : 'GET',
        //         url : "http://local.bghmc.com/administrator/database_eximp/"+encodeURIComponent($export_type)+"/"+$dbtype, //from routes
        //         // data: {exp_type : $export_type, db_type : $dbtype},
        //         // dataType : 'json',
        //         error : function(){
        //             alert('error');
        //         },
        //         success : function(data){
        //             console.log(data);
        //             $('.db_mess').show();
        //             $('.db_mess').text(data["mess"]);
        //         }
        //     });
        // };
    </script>
@stop