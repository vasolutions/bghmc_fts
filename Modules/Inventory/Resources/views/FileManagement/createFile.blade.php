<!-- CALLS SCRIPT -->
@include('inventory::scripts._selectRouteReceivers')
@include('inventory::scripts._selectMultipleRecs')

<!-- ___________________MODAL TO CREATE NEW FILE__________________ -->
  <div class="modal fade" id="modal-create-file" type="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header headerStyle">
          <h3><span id="MessageTitle">Create Document</span> 
            <button type="button" class="fa fa-times" data-dismiss="modal" id="btnt"></button>
          </h3>
        </div>
        <div class="modal-body"> 
          <i style="font-size:12px; color:red;">Note: Fields with asterisks are required.</i>
          <i style="font-size:12px; color:red;" class="NoteNOTIF" id="hideThis"></i>
          <i style="font-size:12px; color:red;" class="errorNOTIF" id="hideThis"></i>
	<div id="selRecsWrapper">
          <span id="suckME">SELECTED RECEIVERS:</span>
          
          <textarea type="text" id="ALLselectedRECS" placeholder="SELECTED RECEIVERS" name="ALLselectedRECS" class="form-control textContent" value="NAMES" disabled required style="display:none;">
          </textarea>
          <div style="border:3px solid #c0c0c0; width:100%; z-index:9; background:#fff; border-radius:4px; overflow:auto; max-height:100px;min-height:100px;">
          <div id ="nameWrapper" style="max-height:100px;min-height:100px;font-size:15px;"></div>
          </div>
          <button type="button" class="btn btn-primary pull-right" id="CLEARSELCTEDNAMES">CLEAR SELECTED</button><br>
         </div>
<!-- ==================================== CREATE FILE ==================================== -->
  <div style="padding:0px;">
    <form class="form-horizontal" file='true' type="form" enctype="multipart/form-data" method="post" action="{{ URL::route('Conversations.store') }}" >
    {{ csrf_field() }}
      <div class="form-group">
        <!-- == SELECT RECEIVER == -->
        <div class="modal-body" data-spy="scroll" data-target="#scrollers" data-offset="50">
          <!-- == SELECT RECEIVER TYPE == -->
          <span>SELECT RECEIVER TYPE<i id="reqStyle">*</i></span>
          <select class="form-control textContent" id="smOsr" required name="smOsr">
            <option value="none">SELECT RECEIVER TYPE</option>
            <option value="sm">Multiple Departments</option>
            <option value="sr">Route Departments</option>
            <option value="sa">All Departments</option>
            <option value="pm">Personal Message</option>
          </select>

          <div id="scrollers">  
            <!-- === MULTIPLE RECEIVERS === -->
            <span id='MR' >
              <span>SELECT RECEIVER</span>
              <select id="SelectReceiverForMultiple" class="form-control textContent" name="PMNames">
                <option value="none" name="none">SELECT RECEIVER</option>
  
                @foreach($deptRecs as $emps13)
                  <option value="{{array_search ($emps13, $deptRecs)}}" name="{{$emps13}}" 
                    id="OptionReceiverForMultiple"> {{$emps13}}
                  </option>
                @endforeach
              </select>
            </span>
            
            <!-- === PERSONNAL MESSAGES == -->
            <span id='PM'>
            <span>SELECT RECEIVER</span>
              <select id="SelectReceiverForPM" class="form-control textContent">
                <option value="none" name="none">SELECT RECEIVER</option>
                @foreach($emps as $emps13)
                  <option value="{{$emps13->emp_id}}" name="{{$emps13->dept_name}} - {{$emps13->f_name}} {{$emps13->l_name}}"
                    id="OptionReceiverForPM"> {{$emps13->dept_name}} - {{$emps13->f_name}} {{$emps13->l_name}}
                  </option>
                @endforeach
              </select>
            </span>

          </div>
                  
    <!-- == FILE TYPE == -->
    <span>Document Type <i id="reqStyle">*</i></span>
    <div>
      <select class="form-control textContent" id="thisSelect" name="docTYPE" required>
        <option value="">SELECT DOCUMENT TYPE</option>
        @foreach($ftypes as $ftypes)
          <option value="{{$ftypes->filetype_id}}">{{$ftypes->filetype_name}}</option>
        @endforeach
      </select>
    </div>
    <!-- == SUBJECT == -->
    <span>Subject <i id="reqStyle">*</i></span>
    <input type="text" class="form-control textContent" placeholder="Subject" id="a2-style" name="a2-data" required>

    <!-- == MESSAGE == -->
    <span>Message</span>
    <textarea type="text" class="form-control textContent" placeholder="Message..." name="MSGhere" id="MessageInput"></textarea>

    <!-- == URGENT == -->
    <div class="col-sm-12">
    <span class="control-label float-left" for="Urgent">Urgent </span>
      <section title=".slideThree">
        <div class="slideThree">  
          <input type="checkbox" value="1" id="slideThree" name="a0-data" />
          <label for="slideThree"></label>
        </div>
      </section>
     </div>

    <!-- == HARD COPY == -->
    	<div class="col-sm-12" id="pmHC">
	<span class="control-label float-left" for="HardCopy">Hard Copy </span>
	      <section>
	        <div class="col-sm-3" id="ccheckHere"> <input type="checkbox" value="0" name="HardCopy" id="HardCopy"/></div>
	        <div class="col-sm-9" style="border-left:2px solid #c0c0c0; display:none;" id="hcNames">
	        	
	        	<div class="inputWrapper">
	        	<input type="text" class="form-control textContent" placeholder="Document Name" id="docName" name="docName[]" style="max-width:50%;">
	        	
	        	</div>
	        	<br><button class="btn btn-new btn-sm" id="addDocName">Add Document</button>
	        	
	        	
	        </div>
	      </section>
	      <br>
	</div>

     

  </div> <!-- END MODAL BODY -->
</div> <!-- END FORM GROUP -->

<!-- === FILE ATTACHMENT SOFT COPY === --> 
<div class="form-group" id="attachSoftFile">
  <i style="font-size:12px; color:#c0c0c0;" class="btn stuff">
    NOTE: File Extension that can be uploaded (.ppt, .txt, .docx, .xls, .png, .jpeg, .jpg, .doc, .pdf)
    <span class="hoverstext">List of all file extensions that can be uploaded<br> .ppt, .txt, .docx, .xls, .png, .jpeg, .jpg, .php, .gif, <br> .doc, .pdf</span>
  </i>
  <input type="file" class="form-control textContent" name="a5-data" id="filea5" style="border:none;">
</div>


<!-- === HIDDEN DATA === -->
<div id="hideThis">
    <input type="text" name="a1-data"  id="receiver" >
    <input typ='text' name='routeShit' id='routeShit'>
</div>

  <div class="modal-footer">
    <input type="submit" value="Send File" name="submit" class="btn btn-new" >
    <button type="button" class="btn btn-danger" data-dismiss="modal" id="CancelCompose">Cancel</button>
  </div>

</form>

</div>
            
<!-- ==================================== CREATE URGENT FILE ==================================== -->
            
      </div>
    </div>
  </div>
</div>


@section('plugins-script')
    <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
@stop