

<!-- = CREATE FILE MODAL = -->
@include('inventory::FileManagement.createFile')

<!-- = SCRIPTS = -->
@include('inventory::scripts._scripts')

<!-- = OTHER MODALS = -->
@include('inventory::FileManagement.MODALS')