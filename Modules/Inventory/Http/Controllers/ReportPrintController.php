<?php

namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ReportPrintController extends Controller
{
     /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        $this->data['_file'] = "";
        $this->data['SFF'] = "";
        // Session::put('slct_gen', '');
        // Session::put('slct_time', '');
        // Session::save();
        return $this->data;
    }

    public function viewReport(){
        $tite_page = 'Reports';
        $this->data['tbl_type'] = "";
        $this->data['results'] = "";
        Session::put('slct_gen', '');
        Session::put('slct_time', '');
        $this->data['results'] = "";
        $this->data['error'] = "";
        Session::save();
        
        return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));
    }

    // ADMIN GENERATES REPORT
	public function GenerateReports(Request $request){
        $this->data['error'] = "";
        $tbl_type = "";
        $getFiles = "";
        $this->data['results'] = "";
        Session::put('slct_gen', $request['selectGenerate']);
        Session::put('slct_time', $request['selectTimeline']);
        Session::save();
        // Get User info
        $userID = Auth::user()->emp_id;
		$user = DB::table('bghmc_employee_info')->where('emp_id',$userID)
            		->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
        $this->data['$user_info'] = $user->f_name." " .$user->l_name.' of '.$user->dept_name;

        if($request['selectGenerate'] == 'sentDocs'){
            $tbl_type = "sent";

            $recHere = "";

            if($request['selectTimeline'] == 'sdays'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_employee_info', 'bghmc_employee_info.emp_id', 'LIKE', 'bghmc_allfiles.SEND_ID')->join('bghmc_filestatus','bghmc_filestatus.dateTNO','=','bghmc_allfiles.TNO')->join('bghmc_filetypes','bghmc_filetypes.filetype_id','=','bghmc_allfiles.FT_ID')->where('SEND_ID',$user->emp_id)->where('dateReply','>=', Carbon::today()->toDateString())->get();
            }
            else if($request['selectTimeline'] == 'sweeks'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('SEND_ID', 'LIKE', $userID)->where('dateReply', '>=', \Carbon\Carbon::now()->startOfWeek()->toDateString())->where('dateReply', '<=', \Carbon\Carbon::now()->endOfWeek()->toDateString())->get();
            }
            else if($request['selectTimeline'] == 'smonths'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('SEND_ID', 'LIKE',  $userID)->where('dateReply', '>=', \Carbon\Carbon::now()->month)->get();                 
            }
            else if($request['selectTimeline'] == 'syears'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('SEND_ID', 'LIKE',  $userID)->where('dateReply', '>=', \Carbon\Carbon::now()->year)->get();  
            }

            $recDepts = [];
            // $recNames = []; -- will contain receiver names
            // $recIDs = []; -- will contain receiver IDs
            if($getFiles != ""){
                foreach($getFiles as $modifiedRec){
                    $recHere = unserialize($modifiedRec->REC_ID);
                    
                    for($i = 0; $i < count($recHere); $i++){
                        // array_push($recIDs, $recHere[$i]); -- for receiver IDs

                        $recDept = DB::table('bghmc_departments')
                                        ->where('dept_id', $recHere[$i])
                                        ->first();
                        array_push($recDepts, $recDept->dept_name);

                        // $recDept = DB::table('bghmc_employee_info')
                        //                 ->join('bghmc_departments', 'bghmc_departments.dept_id', 'bghmc_employee_info.dept_id')
                        //                 ->where('bghmc_employee_info.emp_id',$recHere[$i])
                        //                 ->first();
                        // array_push($recDepts, $recDept->dept_name);
                    }

                    $modifiedRec->dateArchiveByRec = $recDepts;
                    $modifiedRec->pendingRouteRec = unserialize($modifiedRec->ACCPT);
                    $modifiedRec->FT_ID = DB::table('bghmc_filetypes')->where('bghmc_filetypes.filetype_id', $modifiedRec->FT_ID)->first()->filetype_name;
                    // $modifiedRec->dateArchiveBySend = $recNames; -- for receiver names
                    // $modifiedRec->dateRestoredByRec = $recIDs; -- for receiver IDs
                    $recDepts = [];
                }
            } 
        }
        else if($request->input('selectGenerate') == 'filesReceived'){
            $tbl_type = "received";
            if($request->input('selectTimeline') == 'sdays'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('dateReply', '>=', \Carbon\Carbon::today()->toDateString())->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->get();
            }
            else if($request->input('selectTimeline') == 'sweeks'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->startOfWeek()->toDateString())->where('dateReply', '<=', \Carbon\Carbon::now()->endOfWeek()->toDateString())->get();
            }
            else if($request->input('selectTimeline') == 'smonths'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->month)->get();
            }
            else if($request->input('selectTimeline') == 'syears'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->year)->get();
            }

            if($getFiles != ""){
                foreach($getFiles as $modifiedRec){
                    $senderinfo = DB::table('bghmc_employee_info')
                    ->join('bghmc_departments', 'bghmc_departments.dept_id', 'bghmc_employee_info.dept_id')
                    ->where('bghmc_employee_info.emp_id', $modifiedRec->SEND_ID)
                    ->first();

                    $modifiedRec->dateArchiveByRec = $senderinfo->dept_name;
                    $modifiedRec->pendingRouteRec = unserialize($modifiedRec->ACCPT);
                    $modifiedRec->FT_ID = DB::table('bghmc_filetypes')->where('bghmc_filetypes.filetype_id', $modifiedRec->FT_ID)->first()->filetype_name;
                    $modifiedRec->dateArchiveBySend = $senderinfo->f_name . ' ' . $senderinfo->l_name; // for sender name
                    $modifiedRec->dateRestoredByRec = $senderinfo->emp_id; // for sender ID
                }
            } 

        }
        else if($request->input('selectGenerate') == 'actionTaken'){
            $tbl_type = "action";
            if($request->input('selectTimeline') == 'sdays'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('dateReply', '>=', \Carbon\Carbon::today()->toDateString())->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->get();
            }
            else if($request->input('selectTimeline') == 'sweeks'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->startOfWeek()->toDateString())->where('dateReply', '<=', \Carbon\Carbon::now()->endOfWeek()->toDateString())->get();
            }
            else if($request->input('selectTimeline') == 'smonths'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->month)->get();
            }
            else if($request->input('selectTimeline') == 'syears'){
                $getFiles = DB::table('bghmc_allfiles')->join('bghmc_filestatus', 'bghmc_filestatus.dateTNO', 'bghmc_allfiles.TNO')->where('REC_ID', 'LIKE', '%'.$user->dept_id.'%')->where('dateReply', '>=', \Carbon\Carbon::now()->year)->get();
            }
            
            if($getFiles != ""){
                foreach($getFiles as $modifiedRec){
                    $senderinfo = DB::table('bghmc_employee_info')
                    ->join('bghmc_departments', 'bghmc_departments.dept_id', 'bghmc_employee_info.dept_id')
                    ->where('bghmc_employee_info.emp_id', $modifiedRec->SEND_ID)
                    ->first();
                    
                    $arrayNum = array_search($user->dept_id, unserialize($modifiedRec->REC_ID));

                    $modifiedRec->dateAccepted = unserialize($modifiedRec->dateAccepted)[$arrayNum];
                    $modifiedRec->ARCHIVED = unserialize($modifiedRec->ARCHIVED)[$arrayNum];
                    $modifiedRec->dateRead = unserialize($modifiedRec->dateRead)[$arrayNum];

                    $modifiedRec->dateArchiveByRec = $senderinfo->dept_name;
                    $modifiedRec->pendingRouteRec = unserialize($modifiedRec->ACCPT);
                    $modifiedRec->FT_ID = DB::table('bghmc_filetypes')->where('bghmc_filetypes.filetype_id', $modifiedRec->FT_ID)->first()->filetype_name;
                    $modifiedRec->dateArchiveBySend = $senderinfo->f_name . ' ' . $senderinfo->l_name; // for sender name
                    $modifiedRec->dateRestoredByRec = $senderinfo->emp_id; // for sender ID
                }
            }

        }

        $this->data['results'] = $getFiles;
        $this->data['tbl_type'] = $tbl_type;

        // Store results in session for printing
        Session::put('allfiles', $getFiles);
        Session::put('tbl_type', $tbl_type);
        Session::save();

        $tite_page = 'Reports';
        return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));
    }

    // PDF PRINT REPORT
    public function PrintReports(Request $request){
        $gen = Session::get('slct_gen');
        $time = Session::get('slct_time'); 
        $file_gen = "";
        $file_time = "";
        $title = "";
        $subtitle = "";
        
        if($gen == "" || $time == ""){
            $tite_page = 'Reports';
            $this->data['results'] = "";
            $this->data['error'] = "Please select files to generate and print";
            return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));
        }
        else{
            if($gen == "filesReceived"){ $title = "Documents Received"; $file_gen = "docsreceived"; }
            else if($gen == "actionTaken"){ $tile = "Documents with Actions Taken"; $file_gen = "actionstaken"; }
            else if($gen == "sentDocs"){ $title = "Documents sent"; $file_gen = "sentdocs"; }

            if($time == "sdays"){ $file_time = \Carbon\Carbon::today()->toFormattedDateString(); $subtitle = " today ( " . \Carbon\Carbon::today()->toFormattedDateString() . " )";}
            else if($time == "sweeks"){ $file_time = \Carbon\Carbon::now()->startOfWeek()->toFormattedDateString() . "_" . \Carbon\Carbon::now()->endOfWeek()->toFormattedDateString(); $subtitle = " this week ( ". \Carbon\Carbon::now()->startOfWeek()->toFormattedDateString() . " - " . \Carbon\Carbon::now()->endOfWeek()->toFormattedDateString() ." )"; }
            else if($time == "smonths"){ $file_time = \Carbon\Carbon::now()->format('F'); $subtitle = " this year ( " . \Carbon\Carbon::now()->format('F') . " )"; }
            else if($time == "syears"){ $file_time = \Carbon\Carbon::now()->format('Y'); $subtitle = " this year ( " . \Carbon\Carbon::now()->format('Y') . " )"; }

            $total_title = $title . $subtitle;

            // RETURN PRINT PDF
            $userID = Auth::user()->emp_id;
            $user = DB::table('bghmc_employee_info')->where('emp_id',$userID)
                        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
            $this->data['user_info'] = $user->f_name." " .$user->l_name.' of '.$user->dept_name;

            // $this->data['title'] = $total_title;
            // $this->data['allfiles'] = Session::get('allfiles');
            // $this->data['tbl_type'] = Session::get('tbl_type');
            $L = new SLM;
            $L->setLog($total_title, '', 'report_print', null);

            // return view('inventory::reports.print-it', $this->setup());
            $pdf = PDF::loadView('inventory::reports.print-it', ['title' => $total_title, 'allfiles' => Session::get('allfiles'), 'tbl_type' => Session::get('tbl_type'), 'user_info' => $this->data['user_info']])->setPaper('legal', 'landscape');
            return $pdf->download('bghmc_'.$file_gen.'_'.$file_time.'.pdf');
        }
    }

    public function ViewReportinBrowser(Request $request){
        $gen = Session::get('slct_gen');
        $time = Session::get('slct_time'); 
        $file_gen = "";
        $file_time = "";
        $title = "";
        $subtitle = "";
        
        if($gen == "" || $time == ""){
            $tite_page = 'Reports';
            $this->data['results'] = "";
            $this->data['error'] = "Please select files to generate and print";
            return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));
        }
        else{
            if($gen == "filesReceived"){ $title = "Documents Received"; $file_gen = "docsreceived"; }
            else if($gen == "actionTaken"){ $tile = "Documents with Actions Taken"; $file_gen = "actionstaken"; }
            else if($gen == "sentDocs"){ $title = "Documents sent"; $file_gen = "sentdocs"; }

            if($time == "sdays"){ $file_time = \Carbon\Carbon::today()->toFormattedDateString(); $subtitle = " today ( " . \Carbon\Carbon::today()->toFormattedDateString() . " )";}
            else if($time == "sweeks"){ $file_time = \Carbon\Carbon::now()->startOfWeek()->toFormattedDateString() . "_" . \Carbon\Carbon::now()->endOfWeek()->toFormattedDateString(); $subtitle = " this week ( ". \Carbon\Carbon::now()->startOfWeek()->toFormattedDateString() . " - " . \Carbon\Carbon::now()->endOfWeek()->toFormattedDateString() ." )"; }
            else if($time == "smonths"){ $file_time = \Carbon\Carbon::now()->format('F'); $subtitle = " this year ( " . \Carbon\Carbon::now()->format('F') . " )"; }
            else if($time == "syears"){ $file_time = \Carbon\Carbon::now()->format('Y'); $subtitle = " this year ( " . \Carbon\Carbon::now()->format('Y') . " )"; }

            $total_title = $title . $subtitle;

            // RETURN PRINT PDF
            $userID = Auth::user()->emp_id;
            $user = DB::table('bghmc_employee_info')->where('emp_id',$userID)
                        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
            $this->data['user_info'] = $user->f_name." " .$user->l_name.' of '.$user->dept_name;

            $this->data['title'] = $total_title;
            $this->data['allfiles'] = Session::get('allfiles');
            $this->data['tbl_type'] = Session::get('tbl_type');
            $this->data['date_printed'] = \Carbon\Carbon::now()->format('l, F jS Y h:i A');

            return view('inventory::reports.print-it', $this->setup());
            // $pdf = PDF::loadView('inventory::reports.print-it', ['title' => $total_title, 'allfiles' => Session::get('allfiles'), 'tbl_type' => Session::get('tbl_type'), 'user_info' => $this->data['user_info']])->setPaper('legal', 'landscape');
            // return $pdf->download('bghmc_'.$file_gen.'_'.$file_time.'.pdf');
        }
    }
}

