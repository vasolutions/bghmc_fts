<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bghmc_account_activity', function (Blueprint $table) {
            $table->engine = 'InnoDB ROW_FORMAT=DYNAMIC';
            
            $table->increments('act_id');
            $table->integer('emp_id');
            $table->string('deact_reason', 500)->nullable();
            $table->boolean('isactive');
            $table->string('auth_by');
                    
            $table->timestamps();
            $table->softDeletes();

            $table->index(['emp_id', 'isactive'],'bghmc_account_activity'); //keywords for db para mabilis mahanap
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bghmc_account_activity');
    }
}
