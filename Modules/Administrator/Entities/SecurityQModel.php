<?php



namespace Modules\Administrator\Entities;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

use Modules\Administrator\Entities\SecurityQModel as SQM;

use Modules\Administrator\Entities\PersonnelModel as PM;

use Modules\Template\Entities\UserCredentials as Creds;

use Illuminate\Support\Facades\DB;



class SecurityQModel extends BaseModel

{

    protected $table = 'bghmc_emp_credentials_secu';

    protected $fillable = ['emp_id', 'question_1', 'answer_1', 'question_2', 'answer_2', 'iscurrent'];

    protected $primaryKey = 'emp_id';



    public function resetPass($request){

        $bol = "";

        $SQ = DB::table('bghmc_emp_credentials_secu')->WHERE('emp_id', $request->input('empid_secu'))->first();



        if($request->input('secuans_1') != "" && $request->input('secuans_2') != ""){

            // if(Hass::check($request->input('secuans_2') == $SQ->answer_1) && Hash::check(Hash::make($request->input('secuans_2')), $SQ->answer_2)){

            if($request->input('secuans_1') == $SQ->answer_1 && $request->input('secuans_2') == $SQ->answer_2){     

                $PM = PM::find($request->input('empid_secu'));

                $PM->password = Hash::make(1);

                $PM->save();

        

                $Creds = Creds::find($request->input('empid_secu'));

                $Creds->password = Hash::make(1);

                $Creds->save();

                $bol = true;

            }

            else{$bol = false;}

        }

        else{$bol = false;}

        return $bol;

    }

}

