@if (($tite_page)=='Home')        
    <i class="glyphicon glyphicon-home"></i> <span style="display="">{{$tite_page}}</span>
@elseif(($tite_page)=='All Files')
    <span style="display=''" id="titleHere"> <i class="glyphicon glyphicon-folder-open"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Outbox')
    <span style="display=''" id="titleHere"><i class="glyphicon glyphicon-save-file"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Inbox')
    <span style="display=''" id="titleHere"><i class="glyphicon glyphicon-open-file"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Dashboard')
    <span style="display=''" id="titleHere"><i class="glyphicon glyphicon-home"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Flagged')
    <span style="display=''" id="titleHere"><i class="fa fa-flag-o"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Reports')
    <span style="display=''" id="titleHere"><i class="fa fa-area-chart"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@elseif(($tite_page)=='Archived Files')
    <span style="display=''"><i class="glyphicon glyphicon-save-file"></i> {{$tite_page}}</span>
    <b style="display:none;" id="tracks">Track File...</b>
@endif    