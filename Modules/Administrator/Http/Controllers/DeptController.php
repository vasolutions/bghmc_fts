<?php

namespace Modules\Administrator\Http\Controllers;

use Modules\Setup\Init;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Administrator\Entities\DepartmentsModel as DM;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class DeptController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Departments';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $dept_info = DB::table('bghmc_departments')->get(); 
        $this->data['info'] = $dept_info;

        return view('administrator::dept_index', $this->setup());
    }

    public function new_department(Request $request){
        $data = $request->all();
        
        $is_existing = DB::table('bghmc_departments')->WHERE(strtolower('dept_name'), strtolower($request->input('deptname')))->first();
        
        $DM = new DM;
        if($DM->validate($data, ''))
        {
            if($is_existing){ 
                $data['status'] = 0;
                $data['errors']['message'] = "Department Already Registered";
            }
            else{
                $DM->setInfo($request);
                $data['status'] = 1;
                $data['errors']['message'] = 'Department successfully added';
            }
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $DM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }

    public function update_department(Request $request){
        $data = $request->all();
        
        $DM = new DM; 
        
        if($DM->validate($data, '')) 
        {
            $DM->updateInfo($request); 
            $data['status'] = 1;
            $data['errors']['message'] = 'Department successfully updated';
        }
        else
        {
            $data['status'] = 0;
            $data['errors'] = $DM->errors();
        }
        // Return to modal if success or fail
        return $data;
    }
}
