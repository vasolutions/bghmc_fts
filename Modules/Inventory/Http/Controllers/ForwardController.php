<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\FilesModel as FM;
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ForwardController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

    }
// RECEIVER FORWARDS THE FILE
    public function ForwardByReceiver($track, $forwRec, Request $request){	
	$receiverCTR = count(explode(',',$request['AllReceiversForward']));
        $actualReceiver = explode(',',$request['AllReceiversForward']);
	$statusShit = 'File was forwarded.' . Carbon::now()->toDayDateTimeString();
	
	$documentArray = array();
	$StatCTR = 0;
	$hc = "NO";
	$docs = array();
		array_push($docs, "Documents"); 	
		array_push($docs, "EmpNAME"); 		
		array_push($docs, "EmpDEPT");		
		array_push($docs, "AcceptStatus"); 	
		array_push($docs, "dateAccepted");	
		array_push($docs, "ArchiveStatus");  	
		array_push($docs, "dateArchived"); 	
		array_push($docs, "dateRead");		
		array_push($docs, "dateRouteForwarded");
		// ===== ADDS ALL THE FIELDS FOR EACH RECEIVER ===== //
        	foreach($actualReceiver as $x){
			$z = DB::table('bghmc_departments')->where('dept_id',$x)->first();
			foreach($docs as $y){
				$documentArray[$StatCTR][$x][$y] = "N";
				if($y == "EmpDEPT"){$documentArray[$StatCTR][$x][$y] = $z->dept_name;}
				if($y == "Documents"){$documentArray[$StatCTR][$x][$y] = "No Hard Documents";}
			}
			$StatCTR++;
		}
        // ==================== logic for tracking number =====================
        $happ = DB::table('bghmc_transactions')->count(); //number of records in the database
        $happy = DB::table('bghmc_transactions')->select('CTR1')->first(); //get counter
        $happy1 = DB::table('bghmc_transactions')->select('CTR2')->first(); //get counter
        $additionals = strtoupper(mb_substr($forwRec, 0, 3));
        $tnCTR1 = 0;$zzz = 0;$y1 = 0; $y2 = 0; $y3 = 0;
        if($happ == 0){
        DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>0]);
        $idss = '0'.'BG-'.$additionals.'000'.'-'.'000';}  //sets the very first tracking number if only there are no records
        else{
            $sadShit = $happy->CTR1 + 1;
            DB::table('bghmc_transactions')->update(['CTR1'=>$sadShit,]);
            $idss = '0'.'BG-'.$additionals.'000'.'-'.'00'.$sadShit;
            if($sadShit == 10){
                $idss = '0'.'BG-'.$additionals.'000'.'-'.$zzz.$sadShit;
            }elseif($sadShit == 100){
                $idss = '0'.'BG-'.$additionals.'000'.'-'.$sadShit;
            }elseif($sadShit == 1000){
                $sadShit1 = $happy1->CTR2 + 1;
                DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>$sadShit1]);
                $idss = '0'.'BG-'.$additionals.$y1.$y2.$sadShit1.'-'.'000';
                    if($sadShit1 == 10){
                        $idss = $tnCTR1.'BG-'.$additionals.$y1.$sadShit1.'-'.'000';
                    }elseif($sadShit1 == 100){
                        $idss = $tnCTR1.'BG-'.$additionals.$sadShit1.'-'.'000';
                    }elseif($sadShit1 == 1000){
                        $sadShit2 = $happy1->CTR3 + 1;
                        DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>$sadShit2]);
                        $idss = $sadShit2.'BG-'.$additionals.'000'.'-'.'000';
                            if ($sadShit2 == 10){
                                DB::table('bghmc_transactions')->update(['CTR1'=>0,'CTR2'=>0,'CTR3'=>0]);
                                $idss = '0'.'BH-'.$additionals.'000'.'-'.'000';
                            }
                    }
            }
        }
	// ====== END TRACKING NUMBER ====== //

        $a2 = TM::where('transNO',$track)->first();
	$senderInfo = DB::table('bghmc_employee_info')->where('emp_id',$a2->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	// CREATES FILE FOR THE DATABASE
        TM::create(['transNO' =>    $idss,
			'recTYPE' =>    "FORWARDED MESSAGE FROM ". $senderInfo->f_name." ".$senderInfo->l_name." of ".$senderInfo->dept_name." department.",
			'docTYPE' =>    $a2->docTYPE,
                    	'SUBJ' 	=>   	$a2->SUBJ,
                    	'MSG'	=>    	$a2->MSG,
        		'IMP'	=>     	$a2->IMP,
			'hardCOPY'  => 	$hc,
			'fileSTATS' => 	serialize($documentArray),
			'attachNAME'=>  "NO ATTACHMENT",
			'sendID'    => 	$forwRec,
			'threadMSG' =>  $a2->threadMSG,
			'SenderDateSENT'  => Carbon::now()->toDayDateTimeString(),
			'SenderDateArchived'  => 'N',
			'trackingSTATS' =>   $statusShit,
                    	'ROUTESTAT'	=>   '0',
                    	'CTR1'=>    0,
                    	'CTR2'=>    0,
                    	'CTR3'=>    0,
                    
                ]);
        
	// ===== END CREATION OF SHIT ===== //
        $b = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$ctDate = Carbon::now()->toDayDateTimeString();
	$b0 = $b->l_name." of ".$b->dept_name. ' forwarded the file. (' .$ctDate.')'; 
        $b1 = $a0->trackingSTATS . "\n" . $b0;
	TM::where('transNO',$track)->update(['trackingSTATS'=>$b1]);



        $L = new SLM;
        //$receiversList = $a0;
        $empnames = DB::table('bghmc_employee_info')->get();

        //foreach($empnames as $e){
        //    for($i = 0; $i < count($receiversList); $i++){
        //        if($e->emp_id == $receiversList[$i]){
        //            $receiversList[$i] = $e->l_name;
        //        }
        //    }
        //}
        //array_push($receiversList, $a2->attachNAME, $idss);
        //$L->setLog($receiversList, '', 'forward', null);

        return redirect()->back()->with('successMSG','File successfully forwarded!');
    }
    
// ================== END ==================
}
