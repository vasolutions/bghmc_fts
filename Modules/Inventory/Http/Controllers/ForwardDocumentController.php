<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ForwardDocumentController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        return $this->data;

    }
// RECEIVER FORWARDS THE FILE
    public function ForwardDocument($track, $deptName, $deptID,$NUM){
	$a0 = TM::where('transNO',$track)->first();
	$a1 = unserialize($a0->fileSTATS);
	$a2 = count(unserialize($a0->fileSTATS));

	$a1[$NUM][$deptID]['dateRouteForwarded'] = Carbon::now()->toDayDateTimeString();
	$a1[$NUM][$deptID]['dateAccepted'] = Carbon::now()->toDayDateTimeString();
	if(($a2-1) != $NUM){
	   $a1[$NUM+1][implode(" ",array_keys($a1[$NUM+1]))]['dateRouteForwarded'] = Carbon::now()->toDayDateTimeString();
	}
//	dd($a1);
	$stats = 'File was forwarded to the next receiver.' . Carbon::now()->toDayDateTimeString();
	$b0 = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
	$b1 = $b0->l_name.' of '.$b0->dept_name. ' forwarded the documents to the next receiver. (' .Carbon::now()->toDayDateTimeString().')'; 
        $b2 = $a0->trackingSTATS . "\n" . $b1;
	TM::where('transNO',$track)->update(['fileSTATS'=>serialize($a1), 'trackingSTATS'=>$b2,'updateStatus'=>Carbon::now()]);
        return redirect()->back()->with('successMSG','File successfully forwarded to the next receiver!');
    }
    
// ================== END ==================
}
