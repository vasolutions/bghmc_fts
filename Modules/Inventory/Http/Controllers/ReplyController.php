<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class ReplyController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null) { $Init = new Init; $vars['page'] = $this->page_title; $this->data['template'] = $Init->setup($vars); $logs = new SLM; $this->data['logs'] = $logs->show_logs(); return $this->data; }

// ======================================== //
// ===== USERS REPLIES TO THE MESSAGE ===== //
// ======================================== //

    public function RepyShit($track, Request $request){ 
    	$forlog = [];
    	Carbon::now()->toDayDateTimeString();
        $a0 = TM::where('transNO',$track)->first();  
        $a1 = DB::table('bghmc_employee_info')->where('emp_id',Auth::user()->emp_id)
            ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first(); 
        $a3 = $a1->l_name. ' (' .Carbon::now()->toDayDateTimeString().'): '. $request['thread']; 
        $a4 = $a0->threadMSG."\n".$a3; 
        $a5 = $a0->trackingSTATS."\n".$a1->l_name. ' sent a reply. ' .Carbon::now()->toDayDateTimeString();

        if($a0->threadMSG == ''){
            TM::where('transNO',$track)->update(['threadMSG'=>$a3,'trackingSTATS'=>$a5]);
        }else{
            TM::where('transNO',$track)->update(['threadMSG'=>$a4,'trackingSTATS'=>$a5]);
        }
        
        $L = new SLM;
        array_push($forlog, $a0->attachNAME, $a0->transNO);
        $L->setLog($forlog, '', 'reply', null);
        
        return redirect()->back()->with('successMSG','Reply Sent ');
    }
// ==================== END ======================//

    


}// ================== END ==================
