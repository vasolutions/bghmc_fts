@extends('template::admin-pages.menus.'.$template['menu']) 

@section('plugins-css')
    <link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
@stop

@section('pagename')
    <span style="font-size: 25px;">Subject: {{$a0->SUBJ}}</span>
@stop

@section('content')
<script type="text/javascript">
      $(document).ready(function(){
        setInterval(function(){
              $("#Replies").load(location.href+" #Replies>*","");
              $("#SPECIFICFILERECROUTE").load(location.href+" #SPECIFICFILERECROUTE>*","");
        }, 5000);
      });
</script>
    <div class="content-wrapper">
      @include('inventory::includes._ALL') 
      <section class="content">
        <div class="box">
          <div class="box-header" id="SPECIFICFILERECROUTE">
            @if(Session::has('message'))
              <div class="alert alert-success pull-right alertSession" onload="alerts()">{{Session::get('message')}}</div>
            @endif
            
            <!-- BACK BUTTON -->
            <button class="backings btn btn-primary" style="margin-right:2px;">
              <span class="glyphicon glyphicon-arrow-left"></span>&nbsp; Back
            </button>&nbsp;

            <!-- CREATE MESSAGE BUTTON-->
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#modal-create-file" style="margin-right:2px;">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Compose
            </button>&nbsp;
      
            <!-- FORWARD MESSAGE BUTTON-->
            <button type="button" class="btn btn-new" data-toggle="modal" data-target="#FileReceiverForward" style="margin-right:2px; ">
              <i class="glyphicon glyphicon-pencil"></i> &nbsp; Forward
            </button>&nbsp;

            <!-- ACCEPT FILE BUTTON-->
            <a href="#" data-toggle="modal" class="btn btn-info" style="margin-right:2px;">
                <span class="fa fa-file"> &nbsp; </span>
            </a>&nbsp;
            
            <!-- DELETE BUTTON -->
            <a class="btn btn-primary pull-right" href="#" data-toggle="modal">
              <span class="glyphicon glyphicon-trash">&nbsp;</span>
            </a>

          </div>


          <!-- START OF BODY CONTENT -->
          <hr>
          <div class="box-body">
            <div class="col-md-8" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
            	<span style="font-size:20px;"><b>Document Type: </b>{{$a0->docTYPE}}</span><br>
            	<span style="font-size:20px;"><b>Subject: </b>{{strtoupper($a0->SUBJ)}}</span><br>
            	<span style="font-size:17px;"><b>Sender:</b> {{$sender}}</span><br>
                <span style="font-size:17px;"><b>Tracking Number:</b> {{$a0->transNO}}</span><br> 
                <span style="font-size:17px;"><b>Date Received:</b> {{$a0->SenderDateSENT}}</span><br>
                <span style="font-size:17px;"><?php if($a0->IMP == 1){echo "<b style='color:red;'>IMPORTANT &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}?></span><br>
  		<span style="font-size:17px;"><?php if($a0->ROUTESTAT == 1){echo "<b style='color:red;'>ROUTE MESSAGE &nbsp;<i class='fa fa-exclamation-circle'></i></b>";}?></span><br>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <h4 style="font-weight:bolder;">{{$a0->MSG}}</h4>
             <hr style="width:70%; text-align: left; margin-left: 0px; padding-left: 0px;">
             <a href="{{$AttachTipModal}}" data-toggle="modal"
                class="btn btn-link stuff" style="color:#000; margin-left:0px; padding-left:0px;">
                <span><h4>{{$a0->attachNAME}}</h4></span>
                <span class="hoverstext">{{$AttachTip}}</span>    
              </a><br>
              
             <!-- RECEIVERS INFROMATION -->
                <span style="font-weight:bolder; font-size:18px">RECEIVER INFORMATION: </span>
			<?php
			   if($a0->ROUTESTAT != '0'){echo "<a href='#SenderchangeRouteModal' data-toggle='modal'>Change Route</a>";}
				echo "<br>";
			   if($a0->recTYPE == "ALL DEPARTMENTS"){
				echo "<b>Sent to all Departments</b><br>No Hard Documents";
		 	   }elseif($a0->recTYPE != "PERSONAL MESSAGE"){
				foreach(unserialize($a0->fileSTATS) as $a){
			  	  foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
					   echo $b['EmpDEPT']."<br>";					
					   if($a0->hardCOPY == 'NO'){echo "&nbsp; No Hard Copy Sent <br>";}
					   else{echo "&nbsp; Hard Documents: <br>";
					   	foreach (array_keys($b['Documents']) as $c){
						   echo "&nbsp;  &nbsp; ".$c.": ";
						   if($b['Documents'][$c] == "N"){echo "&nbsp;  &nbsp; Not Accepted<br>";}
					   	   else{echo "&nbsp;  &nbsp; ".$b['Documents'][$c]."<br>";}
					   	}
				  	   }
					}
			  	   }
			    	}
		  	   }else{echo "No Hard Documents for Personal Message";}	
			   
			?>

                <!-- END OF RECEIVERS INFORMATION -->
           
            </div>
            <div class="col-md-4" style="margin-left: auto; margin-right: auto; border-right: 1px solid #ccc;">
              <br><br>
              <h4 style="border-bottom: 2px solid #000;">REPLIES</h4>
              <div id= 'RepliesSender' style="height: 270px; max-height: 400px;">
                <?php
                  $messages = nl2br($a0->threadMSG);
                    if($messages == ''){
                      echo "<i style='font-size:15px;' class='replyThisOne'> NO REPLIES </i>";
                    }else{
                      echo "<i style='font-size:15px;' class='replyThisOne'>";
                      echo $messages;
                      echo '</i>';
                    }
                ?>
              </div>

              <form file='true' type="form" enctype="multipart/form-data" method="post" 
              action="{{route('ReplyShit', ['track'=>$a0->transNO]) }}">
              {{ csrf_field() }}
                <div class="form-group">
                    <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                        <input type="text" class="form-control textContent" name="thread" placeholder="Send a Reply">
                        <button class="btn btn-primary btn-block" type="submit" name="SENDREPLY">SEND REPLY</button>
                    </div>
                </div>

              </form>
            </div>
          </div>

        </div>
    

<!-- === CALLS MODALS ===  -->
@include('inventory::FileManagement.MODALS')
<!-- ===== MODAL FOR ACCEPtING the FILE CONFIrMAtiON ALL FILES ==== -->
        <div class="modal fade" id="AcptNotif" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE ACCEPT -</h3></center>
                <center style="font-weight: bolder;">PLEASE MAKE SURE THAT YOU HAVE ACCEPTED ALL THE FILE/S. THANKS </center>
              </div>
              <div class="modal-footer">
              <a href="{{route('ReceiverAccept', ['track'=>$a0->transNO]) }}" class="btn btn-primary">ACCEPT</a>
                <button class="btn btn-danger">CANCEL</button>
              </div>
            </div>
          </div>
        </div>
        
<!-- ===== MODAL FOR ACCEPtING the FILE CONFIrMAtiON SPECIFIC FILE==== -->
        <div class="modal fade" id="AcptNotif" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE ACCEPT -</h3></center>
                <center style="font-weight: bolder;">PLEASE MAKE SURE THAT YOU HAVE ACCEPTED THE FILE. THANKS </center>
              </div>
              <div class="modal-footer">
              <a href="{{route('ReceiverAccept', ['track'=>$a0->transNO]) }}" class="btn btn-primary">ACCEPT</a>
                <button class="btn btn-danger">CANCEL</button>
              </div>
            </div>
          </div>
        </div>
        
<!-- ===== MODAL FOR NO FILE TO ACCEPT ==== -->
        <div class="modal fade" id="NOFILETOACCEPT" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE ACCEPT -</h3></center>
                <center style="font-weight: bolder;">YOU DONT HAVE ANY FILES TO ACCEPT. </center>
              </div>
              <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal">CLOSE</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR DELETING FILE ALERT ==== -->
        <div class="modal fade" id="CANdeleteAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DELETE -</h3></center>
                <center style="font-weight: bolder;">PLEASE MAKE SURE THAT YOU HAVE ACCEPTED THE FILE. THANKS </center>
              </div>
              <div class="modal-footer">
                <a href="{{route('RouteArchive',['track'=>$a0->transNO,'titlePage'=>'none']) }}" class="btn btn-primary">ACCEPT</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>

<!-- ===== MODAL FOR DOWNLOADING FILE ALERT ==== -->
        <div class="modal fade" id="DownloadFileAlert" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE DOWNLOAD -</h3></center>
                <center style="font-weight: bolder;">PLEASE CONFIRM DOWNLOAD OF FILE</center>
              </div>
              <div class="modal-footer">
                <a href="{{route('DownloadFile',['tno'=>$a0->transNO,'attachName'=>$a0->attachNAME,'sender'=>$a0->sendID]) }}" class="btn btn-primary">DOWNLOAD</a>
                <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
            </div>
          </div>
        </div>
@if($a0->ROUTESTAT == 2)
<!-- ===== MODAL FOR CHANGING A ROUTE OF A  FILE ALERT ==== -->
        <div class="modal fade" id="changeRouteModal" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- ROUTE CHANGE -</h3></center>
                <i style="font-size:12px; color:red;">NOTE: Changing it to wrong routes could reflect badly to the credibility of the company and the efficiency of the employees. If unsure of the needed changes, you can always notify the sender. </i>
                <form method="post" action="{{route('changeROUTErec',['tno'=>$a0->transNO,'rec'=>$RECID])}}" id="CHANGEROUTEFORM">
                  {{ csrf_field() }}
                    <select class="form-control textContent" name="USERCHANGEPOSITION" id="USERCHANGEPOSITION">
                      <option value="none">SELECT POSITION</option>
                      @foreach($depArr as $sREC)
                        <option style="font-size:14px" value="{{substr($sREC,0,1)}}">Before {{$sREC}}</option>
                      @endforeach
                        <option style="font-size:14px" value="last">After {{end($depArr)}}</option>
                    </select>
                    
                    <br>
                    <select class="form-control textContent" name="USERTOCHANGEROUTE" id="USERTOCHANGEROUTE">
                      <option value="none">SELECT NEW RECEIVER</option>
                      @foreach($deptRecs as $emps13)
	                  <option value="{{array_search ($emps13, $deptRecs)}}" name="{{$emps13}}" 
	                    id="OptionReceiverForMultiple"> {{$emps13}}
	                  </option>
	              @endforeach
                    </select>
                    
                    <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                    <label>Reason:</label>
                          <textarea class="form-control textContent" name="reasons" placeholder="Enter Reasons" required></textarea>
                      </div>
                    
                    
                    <div class="modal-footer">
                      <button class="btn btn-primary" type="submit" >Save Route</button>
                      <button class="btn btn-info" id="NOTIFYSENDER" type="button">NOTIFY SENDER</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">CANCEL</button>
                    </div>

                </form>

                <form method="post" action="{{route('changeROUTErecNotif',['tno'=>$a0->transNO,'rec'=>$RECID])}}" id="NOTIFYROUTEFORM">
                  {{ csrf_field() }} <br>
                  <div class="form-group">
                      <div class="form-group form-group-pad" style="margin-left:0px; padding-left:0px;">
                          <input type="text" class="form-control textContent" name="Commentaries" placeholder="Enter Commentaries" required>
                      </div>
                      <div class="modal-footer">
                          <button class="btn btn-primary" type="submit">SEND QUERIES</button>
                          <button class="btn btn-danger" id="CANCELNOTIFY" type="button" data-dismiss="modal">CANCEL</button>  
                      </div>
                  </div>
              </form>
              </div>
              
            </div>
          </div>
        </div>
        <script type="text/javascript">
  $("#NOTIFYROUTEFORM").hide();
  $("#NOTIFYSENDER").click(function(){
    $("#CHANGEROUTEFORM").hide('slow');
    $("#NOTIFYROUTEFORM").show('slow');
  });

  $("#CANCELNOTIFY").click(function(){
    $("#CHANGEROUTEFORM").show('slow');
    $("#NOTIFYROUTEFORM").hide('slow');
  });
</script>
@endif

        
        
        

<!-- ===== MODAL FOR FORWARDING A FILE ==== -->
        <div class="modal fade" id="FileReceiverForward" type="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                <center><h3>- FILE FORWARD -</h3></center>
                <center style="font-weight: bolder;">PLEASE BE SURE WHO YOU SEND THE FILE TO. THANKS </center><br>
                  <i style="font-size:12px; color:red;" class="errorFNOTIF" id="hideThis"></i>
		
                <form method="post" action="{{route('forwardByRec', ['track'=>$a0->transNO,'forwRec'=>$a0->sendID]) }}">
                  {{ csrf_field() }}
                  <select id="SelectReceiverForForwardFile" class="form-control textContent">
                    <option value="none" name="none">SELECT RECEIVER</option>
                    @foreach($deptRecs as $emps1)
                      <option value="{{array_search ($emps1, $deptRecs)}}" name="{{$emps1}}" 
                        id="OptionReceiverForForwardFile">{{$emps1}}
                      </option>
                    @endforeach
                  </select>
                  <hr style="width:50%;">
                  <div id="selRecsFWrapper">
			<span id="suckME">SELECTED RECEIVERS:</span>
			<textarea type="text" id="ALLselectedFRECS" placeholder="SELECTED RECEIVERS" name="ALLselectedFRECS" class="form-control textContent" value="FNAMES" disabled required style="display:none;"></textarea>
			<div style="border:3px solid #c0c0c0; width:100%; z-index:9; background:#fff; border-radius:4px; overflow:auto; max-height:100px;min-height:100px;">
				<div id ="nameFWrapper" style="max-height:100px;min-height:100px;font-size:15px;"></div>
			</div>
			<button type="button" class="btn btn-primary pull-right" id="CLEARSELCTEDFNAMES">CLEAR SELECTED</button><br>
		</div>
		
                  <input type="text" name="AllReceiversForward" id="AllReceiversForward" style="display:none;" required>
                  <input type="text" name="AllReceiversForwardName" id="AllReceiversForwardName" style="display:none;" required>
                    <script type="text/javascript">
                    $("#AllReceiversForward").hide();
                      $RecArrID = []; $RecArrNAME = [];
                      var nWrapper = $("#nameFWrapper");
                      $("#SelectReceiverForForwardFile").change(function(){
                        $RecID = $("#SelectReceiverForForwardFile").val();
                        $RecName = $('#SelectReceiverForForwardFile option:selected').text();
                        $arrayCount = $RecArrID.length+1;
                        if($RecName == 'none'){return false; return;}
                        if($RecID == "none"){ return; }
                        
	                        if($.inArray($RecID,$RecArrID) != -1){
			  		$(".errorFNOTIF").text('Duplicate receiver is not allowed. '); $(".errorFNOTIF").show(); return;
			  	}else{
			  		$RecArrID.push($RecID);
		                        $RecArrNAME.push($RecName);
			  		if($arrayCount == 1){
					$(nWrapper).append('<span>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">Delete</button></span>');}
					else{
					$(nWrapper).append('<span><br>'+ $RecName +'<button name="'+$RecID+'" id="rTF" class="btn-link">Delete</button></span>');}
			  	}
                        
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                      });
                      
                      //REMOVE SPECIFIC NAME FROM THE RECEIVERS
				  $removeItem = '';
				  $(nWrapper).on("click","#rTF", function(e){ 
				  	$s = [];
				  	e.preventDefault(); $(this).parent('span').remove();
				  	$removeItem = $(this).attr("name");
				  	arr = jQuery.grep($RecArrID, function( a ) {return  a !== $removeItem;});
				
					document.getElementById("AllReceiversForward").value = arr;
					document.getElementById("AllReceiversForwardName").value = arr;
				  });
				
                      
                      $("#CLEARSELCTEDFNAMES").click(function(){
                        $RecArrID = []; $RecArrNAME = [];
                        document.getElementById("AllReceiversForward").value = $RecArrID;
                        document.getElementById("AllReceiversForwardName").value = $RecArrID;
                        $('#SelectReceiverForForwardFile option:selected').removeAttr('selected');
                        $(".errorNOTIF").text(".");
			$(".errorNOTIF").hide();
			$(nWrapper).children('span').remove();
			$(nWrapper).children('br').remove();
                        return;
                      });
                    </script>
                
              </div>
              <div class="modal-footer">
                <button class="btn btn-primary" id="submitForward" type="submit">FORWARD</button>
                <button class="btn btn-danger" data-dismiss="modal" type="button">CANCEL</button>
              </div>
              </form>
            </div>
          </div>
        </div>

        
      </section>
    </div>




@stop

@section('plugins-script')
        <script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('adminlte')}}/plugins/daterangepicker/moment.js"></script>
    <!-- <script src="{{asset('adminlte')}}/jQuery/jquery-2.2.3.min.js"></script>
    <script src="{{asset('js')}}/jquery.min.js"></script> -->
    <script src="{{asset('js')}}/moment.min.js"></script>
    <script src="{{asset('js')}}/fullcalendar.min.js"></script>
    <!-- <script src="{{asset('includes-jquery')}}/jquery-3.2.1.js"></script> -->
    <script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>

@stop

