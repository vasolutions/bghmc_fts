@extends('template::top-nav.default') 

@section('content')

    <div class="login-box" style="margin: 0 auto;">

        <center>

            @if($hasSecu != true)

            <h3>We detected a newly created account.</h3>

            <h4>You are required to enter a new password and choose your security questions for future security confirmations.</h4>

            @else

            <h3>We detected your account was reset to default</h3>

            <h4>You are required to enter new password &amp; security details for future security confirmations.</h4>

            @endif

        </center>

        <div class="login-box-body">

            <form name="changePasswordform" id="changePasswordform">

                {{csrf_field()}}

                <div id="status"></div>

                <input type="hidden" name="empid" value="{{$empid}}">

                <div class="form-group has-feedback">

                    <input type="password" class="form-control" name="password" placeholder="New Password">

                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                </div>

                <div class="form-group has-feedback">

                    <input type="password" class="form-control" name="repeat_password" placeholder="Re-type New Password">

                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                </div>

                <hr>

                <br>

                <input type="hidden" name="hasSecu" value="{{$hasSecu}}">
                <input type="hidden" name="hasCurrent" value="{{$hasCurrent}}">

                @if($hasCurrent != 1)

                <div class="form-group has-feedback">

                    <select class="form-control" name="secuques_1">

                        <option value="">Security Question 1</option>

                        @for($i = 0; $i < 15; $i++)

                            <option value="{{$secuqs[$i]}}">{{$secuqs[$i]}}</option>

                        @endfor

                    </select>

                </div>

                <div class="form-group has-feedback">

                    <input type="text" class="form-control" name="secuans_1" placeholder="Answer for question 1">

                </div>

                <br>

                <div class="form-group has-feedback">

                    <select class="form-control" name="secuques_2">

                        <option value="">Security Question 2</option>

                        @for($i = 0; $i < 15; $i++)

                            <option value="{{$secuqs[$i]}}">{{$secuqs[$i]}}</option>

                        @endfor

                    </select>

                </div>

                <div class="form-group has-feedback">

                    <input type="text" class="form-control" name="secuans_2" placeholder="Answer for question 2">

                </div>

                <hr>

                @endif

                <div class="form-group">

                    <a onClick="$(this).changePassword();" class="btn btn-bts-login btn-function btn-flat">Save New Password and Sign Me In</a>

                </div>

            </form>

        </div>

    </div>

@stop 

@section('plugins-script')

    <script>

        $.fn.changePassword = function () {

            $.ajax({

                type: 'POST',

                url: "{{route('accnt.passchangepost')}}", //from routes

                data: $('#changePasswordform').serialize(),

                error: function () {

                    alert('error');

                },

                success: function (data) {

                    var errors = '';

                    if (data['status'] == 0) {

                        for (var key in data['errors']) {

                            errors += data['errors'][key] + '<br />';

                        }

                        $(' #changePasswordform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i></h4>' +errors + '</div>').fadeIn();

                    } 

                    else if(data['status'] == 2){

                        $(' #changePasswordform  #status').html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i></h4>' + data['errors'] + '</div>').fadeIn();

                    }

                    else{

                        $(' #changePasswordform  #status').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i>' +data['errors']['message'] + '</h4></div>').fadeIn().delay(1500).fadeOut(1000);

                        if(data['accnttype'] == 'administrator'){ window.location = "{{route('admin.index')}}"; }

                        else{ window.location = "{{route('inventory.index')}}"; }

                    }

                }

            });

        };

    </script>

    @stop