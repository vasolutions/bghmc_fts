<?php 

namespace Modules\Inventory\Entities\Handlers\Events;

// use Modules\Inventory\Entities\deceasedModel;

// use Modules\Inventory\Entities\mongodb_logs as mongodb_logs;

use Carbon;
class ModelEventHandler{

    /**
     * Create the event handler.
     *
     * @return \App\Handlers\Events\ModelEventHandler
     */
    public function __construct()
    {
    }

    /**
    * Handle mdl.created event
    */

   public function created($model, $oldinfo)
   {
      $dt_created = Carbon\Carbon::now();

      // $mngo_logs = new mongodb_logs;
      // $mngo_logs->user_id = $model->id;
      // $mngo_logs->admin_id = \Auth::user()->id;
      // $mngo_logs->logged_in_user = \Auth::user()->ucrd_realname;
      // $mngo_logs->date_added = $dt_created->toDateTimeString();
      // $mngo_logs->type = 'add';
      // $mngo_logs->message = $model->set_message($model, 'added');
      // $mngo_logs->before = json_decode($oldinfo, true);
      // $mngo_logs->after = json_decode($model, true);
      // $mngo_logs->save();
   }

   /**
   * Handle article.updated event
   */

   public function updated($model, $oldinfo)
   {
      $dt_created = Carbon\Carbon::now();

      // $mngo_logs = new mongodb_logs;
      // $mngo_logs->user_id = $model->id;
      // $mngo_logs->admin_id = \Auth::user()->id;
      // $mngo_logs->logged_in_user = \Auth::user()->ucrd_realname;
      // $mngo_logs->date_added = $dt_created->toDateTimeString();
      // $mngo_logs->type = 'update';
      // $mngo_logs->message = $model->set_message($model, 'updated');
      // $mngo_logs->before = json_decode($oldinfo, true);
      // $mngo_logs->after = json_decode($model, true);
      // $mngo_logs->save();
   }

  /**
  * Handle article.deleted event
  */

  public function deleted(deceasedModel $model)
  {
     //Implement logic
  }

  
}

?>