<link rel="stylesheet" href="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.css">

@if($error != "")
    <p class="container" style="color: red;">{{$error}}</p>
@endif
<br>
<form action="{{route('admin.gREPORTS')}}" method="get" id="reportForms" class="container">
    {{csrf_field()}}
    <div class="form-group col-sm-12">
    <select class="form-control textContent col-sm-6" id="selectGenerate" name="selectGenerate">
        <option value="none">SELECT DOCUMENTS TO GENERATE</option>
        <option value="filesReceived">DOCUMENTS RECEIVED</option>
        <option value="actionTaken">DOCUMENTS WITH ACTION TAKEN</option>
        <option value="sentDocs">SENT DOCUMENTS</option>

    </select>
    <select class="form-control textContent col-sm-4" id="selectTimeline" name="selectTimeline">
        <option value='salls'>SELECT TIMELINE</option>
        <option value='sdays'>Within the day</option>
        <option value='sweeks'>Within the week</option>
        <option value='smonths'>Within the month</option>
        <option value='syears'>Within the year</option>
    </select>
    </div>
</form>

<div style="text-align: right; padding-right: 30px;">
    <!-- view -->
    <form action="{{route('admin.vREPORTS')}}" method="get" id="viewpdf" class="container" style="display: inline-block; padding: 0px 2px;">
        {{csrf_field()}}
        <input type="submit" id="btn-print" class="btn" value="View">
    </form>
    <!-- print -->
    <form action="{{route('admin.pREPORTS')}}" method="get" id="printpdf" class="container" style="display: inline-block; padding: 0px 2px;">
        {{csrf_field()}}
        <input type="submit" id="btn-print" class="btn btn-new" value="Print">
    </form>
</div>
<hr>

<div class="container">
    @if($results != "")
    <table id="tbl_reports" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <?php echo $tbl_type == "action" ? '<td>Action</td>' : ''; ?>
            <td>Tracking No.</td>
            <td>Document Type</td>
            <td>Subject</td>
            <?php echo $tbl_type == "sent" ? '' : '<td>Sender ID</td>'; ?>
            <?php echo $tbl_type == "sent" ? '' : '<td>Sender Name</td>'; ?>
            <?php echo $tbl_type == "sent" ? '<td>Receiver Department/s</td>' : '<td>Sender Department</td>'; ?>
            <td>Accept Status</td>
            <td>File Importance</td>
            <?php echo $tbl_type == "sent" ? '<td>Date Sent</td>' : '<td>Date Received</td>'; ?>
        </tr>
    </thead>
    <tbody>
        @foreach($results as $s)
            <tr>
                @if($tbl_type == "action")
                    <td>
                    @if($s->dateRead == 0 && $s->dateAccepted == 0 && $s->ARCHIVED == 0)
                        No Action Taken
                    @else
                        @if($s->dateRead == 1)
                            Read
                        @endif
                    <br>
                        @if($s->dateAccepted == 1)
                            {{$s->dateAccepted}}
                        @endif
                    <br>
                        @if($s->ARCHIVED == 1)
                            {{$s->ARCHIVED}}
                        @endif
                    @endif
                    </td>
                @endif
                <td>{{$s->TNO}}</td>
                <td>{{$s->FT_ID}}</td>
                <td>{{$s->SUBJ}}</td>
                <!-- td for SENDER/RECEIVER ID-->
                @if($tbl_type == "received" || $tbl_type == "action")
                    <td>{{$s->dateRestoredByRec}}</td>
                @endif

                <!-- td for SENDER/RECEIVER ID-->
                @if($tbl_type == "received" || $tbl_type == "action")
                    <td>{{$s->dateArchiveBySend}}</td>
                @endif
                
                <td>
                    @if($tbl_type == "sent")
                        <!-- LOOP FOR DEPARTMENT NAME GROUPED-->
                        <?php $isunique = []; ?>
                        @for($i = 0; $i < count($s->dateArchiveByRec); $i++)
                            <?php if(!in_array($s->dateArchiveByRec[$i], $isunique)){ ?>
                                - {{$s->dateArchiveByRec[$i]}}<br>
                            <?php } ?>
                            <?php array_push($isunique, $s->dateArchiveByRec[$i]); ?>
                        @endfor
                    @elseif($tbl_type == "received" || $tbl_type == "action")
                        {{$s->dateArchiveByRec}}
                    @endif
                </td>
                <td>
                    @for($i = 0; $i < count($s->pendingRouteRec); $i++) 
                        - <?php echo $s->dateArchiveByRec[$i] == 0 ? 'Not Accepted' : 'Accepted' ?><br>
                    @endfor
                </td>
                <td><?php echo ($s->IMP == 0 ? 'Not Important' : 'Important') ?></td>
                <td style="width: 100px;">{{$s->dateSentBySend}}</td>
            </tr>
        @endforeach
    </tbody>
    </table>
    @endif
</div>


<script src="{{asset('includes-jquery')}}/jquery-3.2.1.min.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('adminlte')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $('#tbl_reports').DataTable();

    $("#selectTimeline").hide();
    $("#selectGenerate").change(function(){
      $selectedGenerate = $("#selectGenerate").val();
      if($selectedGenerate == "none"){$("#selectTimeline").hide('slow'); return;}
      else{
        $("#selectTimeline").show('slow');
        $("#selectTimeline").change(function(){
            $('#reportForms').submit();
        });
      }
    });
</script>