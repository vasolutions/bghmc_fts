<?php
namespace Modules\Inventory\Http\Controllers;
use Modules\Setup\Init;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;   
use Modules\Inventory\Entities\TransactionsModel as TM;
use Modules\Inventory\Resources\views\files\upload;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Administrator\Entities\SystemLogsModel as SLM;

class EveryFilesController extends Controller
{
   /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $data;
    protected $page_title = 'Files';

    function setup($vars = null)
    {
        $Init = new Init;
        $vars['page'] = $this->page_title;
        $this->data['template'] = $Init->setup($vars);
        $logs = new SLM;
        $this->data['logs'] = $logs->show_logs();
        //SENT FILE COUNTER
        $sentCTR = TM::where('sendID',Auth::user()->emp_id)->count();
        $this->data['sentCTR'] = $sentCTR;
        $userHere=Auth::user()->emp_id;
        $this->data['userHere'] = $userHere;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps=DB::table('bghmc_employee_info')->where('emp_id','!=',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->where('bghmc_employee_info.isactive',1)->get();
            $this->data['emps'] = $emps;
        // ========= EMPLOYEES JOIN DEPARTMENTS =============
            $emps2=DB::table('bghmc_employee_info')->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->where('bghmc_employee_info.isactive',1)->get();
            $this->data['emps2'] = $emps2;
//            $allDepts = DB::table('bghmc_departments')->get();
		$allDepts = DB::table('bghmc_employee_info')
	    	->join('bghmc_departments','bghmc_departments.dept_id','=','bghmc_employee_info.dept_id')->get();
	    	$deptArray=[];  $dArray=[];
	    	foreach ($allDepts as $sadShit){array_push($deptArray,$sadShit->dept_id); $dArray[$sadShit->dept_id]=$sadShit->dept_name;}
	//    	return array_unique($deptArray);

            $this->data['deptRecs'] = $dArray;
        // === file type ===
            $ftypes=DB::table('bghmc_filetypes')->get();
            $this->data['ftypes'] = $ftypes;
        // === departments ===
            $asd = DB::table('bghmc_departments')->get();
            $this->data['allDepts'] = $asd;
        // $z = $this->data;

        return $this->data;

    }
// ============================================================= //
// ===== DASHBOARD FOLDER -- ALL SENT AND RECEIVED FILES ======= //
// ============================================================= //
    public function DASHY(){
    	$userHere=Auth::user()->emp_id;
        $this->data['RECID'] = $userHere;
	$inboxCounter = 0;
        $depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	$this->data['userDATA'] = $depts;
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
        foreach ($SF as $ss){
		if($ss->sendID == $userHere){
			$inboxCounter++;
			$ss->INVOLVED = 'TRUE';
			$ss->RECEIVERS = 'FALSE';
			$ss->SENDERS = 'TRUE';}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = 'TRUE';
				$ss->RECEIVERS = 'TRUE';
				$ss->SENDERS = 'FALSE';}
			else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = 'TRUE';  
						$ss->RECEIVERS = 'TRUE';
						$ss->SENDERS = 'FALSE';
						break 2;}
					else{
						$ss->INVOLVED = 'FALSE';
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: #000;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;

		if($ss->updateStatus == $ss->updated_at){$ss->updatedStatus = 'background:#c0c0c0;';}
		else{$ss->updatedStatus = 'background:none;';}
        } 

	$this->data['inboxCounter'] = $inboxCounter;
        $a = $this->data['_file'] = $SF;
        $tite_page = 'Dashboard';
        return $this->returnsEverything($a,$tite_page,$userHere);
    }
// ============================== END =============================== //

// ============================================================== //
// ===== ALL FILES FOLDER -- ALL SENT AND RECEIVED FILES ======== //
// ============================================================== //
    public function allFiles(){
        $userHere=Auth::user()->emp_id;
        $this->data['RECID'] = $userHere;
	$inboxCounter = 0;
        $depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	$this->data['userDATA'] = $depts;
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
        foreach ($SF as $ss){
		if($ss->sendID == $userHere){
			$inboxCounter++;
			$ss->INVOLVED = 'TRUE';
			$ss->RECEIVERS = 'FALSE';
			$ss->SENDERS = 'TRUE';}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = 'TRUE';
				$ss->RECEIVERS = 'TRUE';
				$ss->SENDERS = 'FALSE';}
			else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = 'TRUE';  
						$ss->RECEIVERS = 'TRUE';
						$ss->SENDERS = 'FALSE';
						break 2;}
					else{
						$ss->INVOLVED = 'FALSE';
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: red;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;
        } 
	$this->data['inboxCounter'] = $inboxCounter;
        $a = $this->data['_file'] = $SF;
        $tite_page = 'All Files';
        return $this->returnsEverything($a,$tite_page,$userHere);

    } 
// ============================== END =============================== //


// ====================================================== //
// ===== FLAGGED FOLDER -- ALL FLAGGED SENT FILES ======= //
// ====================================================== //
    public function importantFiles(){
        $userHere=Auth::user()->emp_id;
        $this->data['RECID'] = $userHere;
	$inboxCounter = 0;
        $depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	$this->data['userDATA'] = $depts;
	$SF = DB::table('bghmc_transactions')->where('IMP',1)->orderBy('updated_at','desc')->paginate(10);
        foreach ($SF as $ss){
		if($ss->sendID == $userHere){
			$inboxCounter++;
			$ss->INVOLVED = 'TRUE';
			$ss->RECEIVERS = 'FALSE';
			$ss->SENDERS = 'TRUE';}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = 'TRUE';
				$ss->RECEIVERS = 'TRUE';
				$ss->SENDERS = 'FALSE';}
			else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = 'TRUE';  
						$ss->RECEIVERS = 'TRUE';
						$ss->SENDERS = 'FALSE';
						break 2;}
					else{
						$ss->INVOLVED = 'FALSE';
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: #000;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;
        } 
	$this->data['inboxCounter'] = $inboxCounter;
        $a = $this->data['_file'] = $SF;
        $tite_page = 'Flagged';
        return $this->returnsEverything($a,$tite_page,$userHere);
    }
// =========================== END =========================== //

// ====================================================== //
// ===== INBOX FOLDER -- ALL RECEIVED FILES ============= //
// ====================================================== //

    public function receivedFiles(){
        $userHere=Auth::user()->emp_id;
        $this->data['RECID'] = $userHere;
	$inboxCounter = 0;
        $depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();
	$this->data['userDATA'] = $depts;
	$SF = DB::table('bghmc_transactions')->orderBy('updated_at','desc')->paginate(10);
	foreach($SF as $ss){
		if($ss->sendID == $userHere){$ss->INVOLVED = 'FALSE';}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = 'TRUE';
				$ss->RECEIVERS = 'TRUE';
				$ss->SENDERS = 'FALSE';}
			else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = 'TRUE';  
						$ss->RECEIVERS = 'TRUE';
						$ss->SENDERS = 'FALSE';
						break 2;}
					else{
						$ss->INVOLVED = 'FALSE';
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->updateStatus == $ss->updated_at){$ss->updatedStatus = 'background:#c0c0c0;';}
		else{$ss->updatedStatus = 'background:none;';}
	}
	$this->data['inboxCounter'] = $inboxCounter;
        $a = $this->data['_file'] = $SF;
        $tite_page = 'Inbox';
        return $this->returnsEverything($a,$tite_page,$userHere);
        
    }
// =========================== END =========================== //

// =========== ALL ARCHIVED FILES ========== //
    public function ArchivedFiles(){
        $userHere=Auth::user()->emp_id;
        $this->data['USERID'] = $userHere;     
	$depts = DB::table('bghmc_employee_info')->where('emp_id',$userHere)->first();
        $SF = DB::table('bghmc_transactions')->where('dateARCHIVED','!=','')->paginate(10);
	foreach($SF as $ss){
		if($ss->sendID == $userHere){
			$ss->INVOLVED = 'TRUE';
		}else{
			foreach(explode(" ",$ss->ARCHIVE) as $recs){	
				if($recs == $depts->dept_id){
					$ss->INVOLVED = 'TRUE';
				}else{$ss->INVOLVED = 'FALSE';}
			}
			
		}
	}
        $a = $this->data['_file'] = $SF;
        $tite_page = 'Archived Files';
        return $this->returnsEverything($a,$tite_page,$userHere);
    }
// ====================================================== //
// =========== OUTBOX FOLDER -- ALL SENT FILES ========== //
// ====================================================== //
    public function sentFiles(){
        $userHere=Auth::user()->emp_id;
        $SF = DB::table('bghmc_transactions')->where('sendID',$userHere)->where('SenderDateArchived','=','')->orderBy('created_at','desc')->paginate(10);
	$inboxCounter = 0;
        $NAMESHIT = array(); $recDEPTS = array(); $ACCPTS = array();
        foreach ($SF as $ss){
		$ss->INVOLVED = 'TRUE';
		$ss->RECEIVERS = 'FALSE';
		$ss->SENDERS = 'TRUE';
		$inboxCounter++;
            // FILE IMPORTANCE
	    // THE COLOR OF THE ROW WILL BE RED IF THE FILE IS IMPORTANT
            if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: #000;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;

		if($ss->updateStatus == $ss->updated_at){$ss->updatedStatus = 'background:#c0c0c0;';}
		else{$ss->updatedStatus = 'background:none;';}
        }
	$this->data['inboxCounter'] = $inboxCounter;
        $tite_page = 'Outbox';
        $a = $this->data['_file'] = $SF;

        return $this->returnsEverything($a,$tite_page,$userHere);
    }
// =========================== END =========================== //

    public function REPORTS(){
        $userHere=Auth::user()->emp_id;
        $this->data['USERID'] = $userHere;
        
        $SF = FM::join('bghmc_filestatus','dateTNO','=','TNO')
        ->join('bghmc_filetypes','filetype_id','=','FT_ID')
        ->join('bghmc_employee_info','bghmc_employee_info.emp_id','sendID')
        ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')
        ->where('dateArchiveBySend',null)
        ->orderBy('dateReply','desc')
        ->paginate(10);
        foreach ($SF as $m) {
            if($m->sendID == $userHere){
                $m->involved = "INVOLVED";
                $NAMESHIT = array(); $DEPTSHIT = array(); $ACCPTS = array();
                // GETS THE DEPARTMENT AND NAME OF RECEIVERS
                foreach(unserialize($m->REC_ID) as $sss){
                    $NameAndDEPT = DB::table('bghmc_employee_info')->where('emp_id',$sss)
                    ->join('bghmc_departments','bghmc_employee_info.dept_id','bghmc_departments.dept_id')->first();
                    array_push($NAMESHIT, $NameAndDEPT->f_name . ' ' . $NameAndDEPT->l_name);
                    array_push($DEPTSHIT, $NameAndDEPT->dept_name);
                    $m->NAMESHITS = $NAMESHIT;
                    $m->DEPTSHITS = $DEPTSHIT; }

                // GETS THE ACCEPteD FILE STATUS OF EACH RECEIVER
                foreach(unserialize($m->ACCPT) as $sa){
                    if($sa == 0){   array_push($ACCPTS, 'NOT ACCEPTED'); $m->ACCPTSS = $ACCPTS; }
                    else{  array_push($ACCPTS, 'ACCEPTED'); $m->ACCPTSS = $ACCPTS; } }

                $NAMESHIT = array(); $DEPTSHIT = array(); $ACCPTS = array();
            }else{
                
                //CHECKS IF USER IS RECEIVER
                if(in_array($userHere, unserialize($m->REC_ID) )){
                    $m->involved = "INVOLVED";
                    $m->arrayNO = array_search($userHere,unserialize($m->REC_ID) );}
                else{$m->involved = "NOT INVOLVED"; $m->arrayNO = 0;}

                //CHECKS IF ACCEPTED OR NOT
                if((unserialize($m->ACCPT)[$m->arrayNO]) == 0){
                    $m->ACCPTSS = 'FILE NOT ACCEPTED';
                    $m->AcptTip = 'Accept File';
                    $m->AcptNotif = "{{route('ReceiverAccept',['track'=>$m->TNO,'rec'=>$userHere,'acpt'=>$m->ACCPTSS,'ANum'=>$m->arrayNO])}}"; 
                    $m->AcpTogle = "";
                    $m->AcptBox = 'fa fa-square-o';
                    $m->AcptNo = 0;
                    $m->AcptStyle = "color:red;";
                }else{
                    $m->ACCPTSS = 'FILE ACCEPTED';
                    $m->AcptTip = 'You have accepted the file already!';
                    $m->AcptNotif = "#AlreadyAcptNotif"; $m->AcpTogle = "modal";
                    $m->AcptBox = 'fa fa-check-square-o';
                    $m->AcptNo = 1;
                    $m->AcptStyle = "";
                }

                //CHECKS IF ARCHIVED OR NOT
                if((unserialize($m->ARCHIVED)[$m->arrayNO]) == 0){ $m->ARCH = 'NOT ARCHIVED';}
                else{ $m->ARCH = 'ARCHIVED'; }

                //CHECKS IF READ OR NOT
                if((unserialize($m->dateReadByRec)[$m->arrayNO]) == 0){ $m->ReadStyle = 'background:#ccc;'; }
                else{ $m->ReadStyle = ''; }
                
                //GETS THE FILE TYPE 
                $sadFT = DB::table('bghmc_filetypes')->where('filetype_id',$m->FT_ID)->first(); 
                $m->FT = $sadFT->filetype_name;

                if($m->ROUTESTAT == 1){
                    $m->ACCPTSS = nl2br(e('VIEW FILE FOR MORE INFORMATION -- FILE SENT AS A ROUTE'));
                    $m->AcptTip = 'VIEW FILE FOR MORE INFORMATION';
                    $m->AcptNotif = '#AcptNotif';
                }
            }
            //CHECKS IMPORTANT OR NOT
                if($m->IMP == 0){    $m->ImpStyle = ' '; }
                else{  $m->ImpStyle= 'color: red;'; }
        }


        $a = $this->data['_file'] = $SF;
        $tite_page = 'Reports';
        return $this->returnsEverything($a,$tite_page,$userHere);
        
    }
    

    public function returnsEverything($a,$tite_page,$userHere){
        foreach ($SF as $ss){
		if($ss->sendID == $userHere){
			$inboxCounter++;
			$ss->INVOLVED = 'TRUE';
			$ss->RECEIVERS = 'FALSE';
			$ss->SENDERS = 'TRUE';}
		else{
			if($ss->recTYPE == "PERSONAL MESSAGE"){
				$inboxCounter++;
				$ss->INVOLVED = 'TRUE';
				$ss->RECEIVERS = 'TRUE';
				$ss->SENDERS = 'FALSE';}
			else{
			  foreach(unserialize($ss->fileSTATS) as $a){
				foreach($a as $b){
					if($b['EmpDEPT'] == $depts->dept_name){
						$inboxCounter++;
						$ss->INVOLVED = 'TRUE';  
						$ss->RECEIVERS = 'TRUE';
						$ss->SENDERS = 'FALSE';
						break 2;}
					else{
						$ss->INVOLVED = 'FALSE';
						$ss->RECEIVERS = 'FALSE';
						$ss->SENDERS = 'FALSE';}
				}
			  }
			}
			$rawSenderDept = DB::table('bghmc_employee_info')->where('emp_id',$ss->sendID)->join('bghmc_departments','bghmc_departments.dept_id','bghmc_employee_info.dept_id')->first();		
			$ss->SenderDept = $rawSenderDept->dept_name;
			$ss->SenderName = $rawSenderDept->f_name." ".$rawSenderDept->l_name;
		}
		if($ss->IMP == 0){  $imports = "fa fa-flag-o"; $impName = "Mark as Important"; $ss->ImpStyle = ' ';}
            else{  $imports = "fa fa-flag"; $impName = "Mark as Not Important"; $ss->ImpStyle= 'color: #000;';}
            $this->data['imports'] = $imports; $this->data['impName'] = $impName;
        } 
        
        $SFTRACKINGS = 's';

        $this->data['SFF'] = $SFTRACKINGS;

        return view('inventory::FileManagement.fileIndex',$this->setup(),compact('tite_page'));

    }
  


// ================== END ==================
}
