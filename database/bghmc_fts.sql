-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2017 at 01:02 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bghmc_fts`
--

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` int(10) UNSIGNED NOT NULL,
  `auditable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old` text COLLATE utf8mb4_unicode_ci,
  `new` text COLLATE utf8mb4_unicode_ci,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `type`, `auditable_id`, `auditable_type`, `old`, `new`, `user_id`, `route`, `ip_address`, `created_at`) VALUES
('015a3ace-37b8-491b-a792-4cf823092a63', 'updated', 2015, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 14:08:47'),
('015c22b9-ead8-481d-8d1b-d803ce32d86e', 'updated', 20135776, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2003', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-08-26 07:21:30'),
('02672604-0001-4ac4-8d72-3c3d652a5577', 'updated', 1234, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2003', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-04 01:14:24'),
('02bf3d2d-f719-4396-80c4-be7c823d39a2', 'updated', 2013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 08:28:11'),
('05d0e3af-9f29-4ccd-983f-e9621e98979d', 'created', 12, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:01:58'),
('067ba414-dba2-4107-a09d-41dd2e24b148', 'updated', 2017, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"Tw4BtIdRVfukAvG9OV3OJKPiqfp97oF2DPeJvfjhXvdhLT1rWrFWjAFzLHgj\"}', '{\"remember_token\":\"MFpbBy6EDGY2fhuZugy0bwQZ4GDGJ304GQcPJ9r19EgJc383DmsuLW3Y2s1M\"}', '2017', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-09 10:46:57'),
('07e78b44-1d69-4237-9175-6a6be4acefa4', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"tkAhPeslSmbprj2NHYTlOJgZLSCH8mshoDtsAGQOuNPCx9H9I3tDdInLO6l7\"}', '{\"remember_token\":\"s3BSWLNybuzXqS0nRwdkPAKky1UcUr43haFdKLZi04LmpTQOlbcJLqLeZA98\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-16 03:33:54'),
('0ba30526-4689-4f03-be42-d2656519b1fd', 'updated', 3, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"2k9TL6RLq16A6oDL1mIFcServw4f8EM5YewAHz7Xbl0TYiJfs5ShiqRHBUp8\"}', '3', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-22 05:48:41'),
('0c6a5b9f-ad0e-4c45-bbaa-5b2e92419e08', 'created', 9, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2015\",\"group_id\":2,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-23 04:45:21'),
('0ed0bfcc-3954-4a55-ad96-5237f9608140', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2013\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:24:04'),
('1113db73-4756-4562-b2e6-cc3f4d24bdb8', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"yjoIRmosa7nYzZETHxFkIxysB1ZgmEKM2ioJP3dbUjTI4Q5CFPsg3sm1cPSP\"}', '{\"remember_token\":\"P9COyrYogzVWmzribyMFE0PayN4ceZuMyqvwdYKjRRwYM5tQ6qUhvTaU2tzK\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-02 23:38:51'),
('11b3e113-85f7-49fb-92c6-0991c863215c', 'created', 12, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"4444\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-02 23:34:27'),
('123e60c4-a990-4ed5-b323-b94c8b631be9', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"8L2PDE3kXBf9AwrQKL6IQ6R2Hkuszo2LLBnH14vqqLkNWyK1cASuInyWDzXe\"}', '{\"remember_token\":\"TOFM4FIWoUrDEYZpF79093EjHroPX3P7i5EPMAepfSSKtsiwwisvR1pRxsAS\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:19:13'),
('15fbf17b-5502-4f32-a4ba-a8611062e352', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"cUnh3oh2oFCOz3geySWFMI5D5saN9z7AjsY6ScB6Sw2jMp11d91qiCNJenIo\"}', '{\"remember_token\":\"FD9oZi4x2OYQcCsy32CRS0nCVTe5sDTHWgGfhVksLknPubMIsxmMIRjt8jWu\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-22 05:45:56'),
('16980339-605e-453c-91c6-dfc82258dc35', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:46:41'),
('1704ebfe-921c-4662-a268-f1c3a9cdde8b', 'updated', 2017, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"e8x9dcu130w6b2bPWelmKYMmtNnIdq16Vq1pYv7THClbV0JUZTdH7n18IK06\"}', '2017', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-09 09:49:06'),
('1799ae5d-0f8d-44be-aec5-f60f016275ef', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:29:57'),
('1880afe4-e70a-422b-9d73-2f3d5fcb96de', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"6ZhtXIFn9MWlD7zzEW90pacpYcDw2n4nhOEM6EF2OTaRSSmi4WL5K8mGTU0t\"}', '{\"remember_token\":\"FyIgNefF20pybE0oZKw7WvnXftyf7pfBTD2zn8XReCeA0vK46VcF7q3wHrt2\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:20:17'),
('1a3654f9-34d7-404b-9ec9-6b17ceeab23a', 'updated', 2016, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"o74kq41VGdYJkFGHs16RB5AgHQv6SICHnh5yyRhf9lkcwban1ym2wdnDIhe6\"}', '2016', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-01 01:03:15'),
('1bfa112b-f2e5-40dd-b606-0e7a6091b654', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"kB6tLDw2sXtFwiqPObVtdd0KsPU913gVich525cE7lnuc5j99S6MHM98dMY8\"}', '1', 'http://localhost:8080/template/public/logout', '::1', '2017-01-31 22:33:39'),
('1cb8db26-3677-4ee1-9b02-dc3a59e5d125', 'updated', 2018, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 13:36:17'),
('1ce802f1-c869-49d3-a230-e26a395b4267', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:27:08'),
('1ce9f949-a893-4029-b192-e1cff8bb4287', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"DIQxIXi6UvUazehJvLsIh4K4hPSmc3T2fObpOt5I9ZwQDuMKDeQJQOi4h8px\"}', '{\"remember_token\":\"lGPLwlX6M68b39uNaUHZPzFMC7OrD4whp9lDBql7WnDcy7kkhwxps0hxaXjn\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 23:52:00'),
('1d5d0bdb-42b4-4065-a149-cc9537fcb444', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"e9aa5CgyMpwWFGS9UF1cKqEmZVp2JiNRLmQi8WpsNa3M8GVThJy9UHuJfJSZ\"}', '{\"remember_token\":\"CSPcXNA3YQId6QGMDUxksVbU26A8xoUjQd2iZISv99rXrnkIlV7a1pxMXty2\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 05:12:32'),
('1eb29ed5-892b-4d7f-91c9-104a6cfdd757', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"FD9oZi4x2OYQcCsy32CRS0nCVTe5sDTHWgGfhVksLknPubMIsxmMIRjt8jWu\"}', '{\"remember_token\":\"azHaUbV3G25Fg6wrfRQhTHAbmJJYTdTCnKGOPwpYhF57ELFfY1rpO6Y9Ni0j\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-22 05:54:07'),
('1f23ae84-c616-4a00-ba9a-ef29cd39cf89', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:40:39'),
('1fe1fe04-6bd7-4d84-b650-353803b6c125', 'created', 15, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2004\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:46:39'),
('21700a1c-0587-457d-9702-c6a05588b767', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20122642\",\"group_id\":0,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-22 05:45:42'),
('238d8c91-ec03-496d-b4a0-c022ba4cd186', 'created', 13, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135555\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-08-26 06:13:58'),
('23fbb582-20cd-4c61-8709-f7776f7aad2c', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"4999\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2003', 'http://localhost:8000/administrator/register_account', '127.0.0.1', '2017-08-27 07:05:46'),
('249dc80e-6d0b-4d45-8736-96c555216fe6', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:40:04'),
('25005429-9d63-4312-8926-22a1e346acd8', 'created', 11, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"3333\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-01 01:06:09'),
('2d406e78-3d25-4bf8-82f3-95b9a1776960', 'created', 11, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2011\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:00:03'),
('2ff8422b-526f-4ba4-bcda-7756b462dbcd', 'created', 13, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"5555\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-02 23:38:46'),
('32885e7b-e323-4043-8454-627999f55e52', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"kd1PZ8y2WmZ7feIxnGSg2hOkwrNBZrugdzINgutUXNNPw07CLEdatpLD8kc2\"}', '2', 'http://localhost:8080/template/public/logout', '::1', '2017-01-31 23:01:18'),
('337867ea-3f54-41f3-9b7d-0d2aaefc0da6', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:47:03'),
('33ac5b94-4cef-47b7-8372-fee08015c09b', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:26:29'),
('34bb29ed-56cd-499e-8ff8-bc543018bbd4', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135160\",\"group_id\":2,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-22 20:48:40'),
('354647ca-f8bf-4ead-92cd-4cba1e206314', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:53:44'),
('35be22f5-2a5e-4bd4-b652-7dcaca55a75e', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2015\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:54:46'),
('369d2b7d-68e7-4dc0-8f0b-391f80b8d992', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"12131415\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:05:06'),
('38eedbbe-97b1-4be3-832e-68e350b8a02d', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"PJkX9E60ZS1QRtVxeCpLLSiw0Bl1Wug9bveoZMaFw5E98WEBFYFTwcYpY4Jc\"}', '2005', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-09 09:51:00'),
('39034f2d-f908-46a5-9430-e00a396f1ec1', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:54:50'),
('396996b1-6a0e-4325-a748-4c9d8b65eba9', 'created', 12, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2007\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:39:33'),
('39bc948a-14f5-4365-99f9-444877978b2b', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"Wl7uoF95bsRB4Ef36jgVG2Saf4shrYUu6r3CbG2l9gEOIvWYWaMy5GYSHUV6\"}', '{\"remember_token\":\"yjoIRmosa7nYzZETHxFkIxysB1ZgmEKM2ioJP3dbUjTI4Q5CFPsg3sm1cPSP\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-02 23:34:32'),
('3a1a594e-1ae5-4a99-b00e-9b65126b7909', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:42:05'),
('3a6357e5-725c-4f9c-ae6d-6a384d77cfdc', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"Uz9KI9uh9A1cJBwXE8dhXbIVs18RpLRuVdHg3KrQPMZyXul4jpLhzOzVR4q3\"}', '{\"remember_token\":\"EIoNbALgZZeI8CwNVB4AQA4J10HIOArc8sYP6t56vjHn2OcBqtj3GH2ZXdma\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-01 00:52:42'),
('3c5a615d-81db-4d18-99f8-072efa471970', 'updated', 6666, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"tHO17Gjslw5uNx3TTr1CMMWtDv0Jt1iUXO6W5PHSzANraxgDowB5VAyHrcGe\"}', '6666', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 04:21:39'),
('3c63ec65-045d-4801-82ac-d413fcd76349', 'created', 7, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2012\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:32:37'),
('3ced8527-aa80-4ee6-b628-e9bd1bc116a0', 'created', 9, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2019\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:56:38'),
('3dfebb97-12ab-467e-8fe3-6aebb38f438f', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:38:45'),
('3ed67b91-2275-42d0-8c71-e5b05452c939', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"Y6n7DqFKWgwZi57jYDckN0yvCuAC9Z3cpdsjC12GKr7uZPDR1gm5xEVs0wU3\"}', '{\"remember_token\":\"Wl7uoF95bsRB4Ef36jgVG2Saf4shrYUu6r3CbG2l9gEOIvWYWaMy5GYSHUV6\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-01 01:06:17'),
('425ad49d-5a15-4b1e-92f3-1fca825142ac', 'created', 16, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2003\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:47:09'),
('4282893b-9805-4dec-8a8a-7526ec1c22bd', 'updated', 2000, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:10:45'),
('43bfb627-e992-4aec-bede-7952be5b0eda', 'created', 15, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"7777\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-03 04:22:51'),
('43ddbf12-c302-4097-94dd-b64b04063802', 'updated', 12131415, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 13:11:10'),
('44092897-2135-4abe-945f-3b0c651802ee', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"GINj2BrmESU3f1mQl3OFCjwmKHWXzpLm8V2MFqgUBomFntJwzPOM3uCsPdQj\"}', '{\"remember_token\":\"aBjNOUehx7JYCDrIGwvxa7OikQHjBcVYX89cCBdsmJxXZwOg6dp6REAUDLjL\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 04:07:00'),
('4579b9a4-0671-4ac3-81a8-8ef6f7eafd0b', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:00:16'),
('45b4d794-e60a-46ae-abb6-c0e40e418dd0', 'created', 12, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135160\",\"group_id\":2,\"is_approved\":1}', '11', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-08-24 05:18:08'),
('48650fe5-50c4-4e68-81b4-c13fefce1061', 'created', 10, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2222\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registeraccount', '127.0.0.1', '2017-08-24 01:53:29'),
('4acbd78b-c6a1-4298-8c24-6674818da1a4', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1256\",\"group_id\":1,\"is_approved\":1}', '3', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-22 05:48:02'),
('4b443669-9d0e-4834-bece-b284e2e08350', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"ULhSe9dAcN62dByBa9mhd2Pqe0O2TG9M1S63mcWthX5KwZgZeDs93XCOZS5I\"}', '{\"remember_token\":\"cUnh3oh2oFCOz3geySWFMI5D5saN9z7AjsY6ScB6Sw2jMp11d91qiCNJenIo\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-22 02:56:26'),
('4b5675ce-1bb2-44ca-b712-f604d886edef', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"TOFM4FIWoUrDEYZpF79093EjHroPX3P7i5EPMAepfSSKtsiwwisvR1pRxsAS\"}', '{\"remember_token\":\"4cuiXg3RVstQCOnBft6lOD6sEZPABaSkOdT7pZ3wBGbnmUn2xfn4rvdF8eJS\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:20:32'),
('4b720d53-b8ce-4874-a1e7-29b105769d93', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"8484\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2003', 'http://localhost:8000/administrator/register_account', '127.0.0.1', '2017-08-27 20:21:38'),
('4f7e402a-4904-42a7-8d2c-b01bba9175e9', 'created', 7, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2013\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:55:29'),
('507ecde2-aa50-4452-84f2-dffecb334745', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"zicAZ5dyyPayoYNsBlcFyZgqXrUgLzksZT2KVBM9NqZLYlQOEpaVdYziepYV\"}', '{\"remember_token\":\"2RI60EwP4rYl04A3Dg4roElUOA8mfhRxzTVc1r6HE2PqHPBxeXaQnwoyQVFE\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-22 03:07:06'),
('5154dc20-dca9-494f-b5a3-2b3217e69748', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"jm3nu1sEjYeGwmYs2OjmyA3KJEmgzwR7jFgjViW9wYLdmH0717Oq0BwgvQ67\"}', '{\"remember_token\":\"dlA36Vo1fErl550Qw1QicEznih5F3Gi3qz9posXTfeaIBUvxTSwT8l6VMZYI\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-15 04:01:31'),
('516d6b9e-1a78-4d23-ab90-f1eeee75b07d', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135845\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:44:16'),
('520e71a7-8f7c-4344-9ed4-db02db9056b5', 'created', 10, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2020\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:59:46'),
('54398484-cac9-4775-9151-196f17ab5a51', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"C2Vk7DgQeUjihvUeA9nQ868JRMkyobldPHTnte643SEBJuQXPVpVHL70GU2z\"}', '{\"remember_token\":\"gUFaSWi7Tt2hTmJ9Yf6fl77r12lKCRoWg0MncPwYFbVVY5Rao7Mc2aLMNnWV\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-19 01:30:09'),
('5598939d-0293-49d3-87fa-6ea706191833', 'updated', 1111, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2003', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-04 01:14:14'),
('57018ea9-23d5-4c1f-9726-ec1fa6647ffa', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:14:28'),
('574f1dbb-b8d2-435a-a133-526f4a64129c', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"SFqSU0aXyKFdZGA3hN9RatcKgdbpJUBLJKdt96qoXnF7PBbUyGY9V0pDnwv1\"}', '{\"remember_token\":\"1ohXvPvXQhQP2rfGCUROAUgK4fovHB7WnNLrRd0AB7UKlFiSWiZMGxK2h8hK\"}', '2', 'http://template.dev:8080/logout', '127.0.0.1', '2017-02-13 22:16:54'),
('57bbb24b-bd89-4cfc-ba03-28325d225132', 'created', 8, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2011\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:33:16'),
('597cec86-bfb8-4856-910b-cf922d5c35d9', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:14:40'),
('5a92cf32-4863-407d-b3e0-59ec9db4578d', 'created', 17, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2002\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:48:17'),
('5c865ded-7310-439e-ac2a-5ccaf82db2ac', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"1ohXvPvXQhQP2rfGCUROAUgK4fovHB7WnNLrRd0AB7UKlFiSWiZMGxK2h8hK\"}', '{\"remember_token\":\"7435FmozIcj37DBEu1KCX1CyAE8s5oAvL7wsI9gXKwlQG7L4OySjthgRflgl\"}', '2', 'http://template.dev:8080/logout', '127.0.0.1', '2017-02-28 19:58:51'),
('5f8e64f6-50f8-4427-9f2a-1843cc43fae1', 'updated', 2015, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 14:10:01'),
('605f57e2-c1be-4bce-a4d2-12be6a993c9d', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:47:22'),
('62a52336-8ded-4f9e-889a-d5fcdf94e543', 'created', 9, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2010\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:35:10'),
('64ce1063-bb6c-4260-940b-fb464045a3c4', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"7GdKquH9CIdsfcfzotvIpaqa3mxbQLl3tmtXXuNBo5jJbj3IkRfxwoDBx5gj\"}', '{\"remember_token\":\"oW2w89w2tTYk2686A0m6P6sfbUeYVV52Xvoso8lqaoUyhlmqCs8G4ZNMr7BP\"}', '2', 'http://localhost:8080/template/public/logout', '::1', '2017-01-31 23:44:29'),
('658ae480-e4ea-49ee-b0a2-2704ee2e48df', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"kd1PZ8y2WmZ7feIxnGSg2hOkwrNBZrugdzINgutUXNNPw07CLEdatpLD8kc2\"}', '{\"remember_token\":\"7GdKquH9CIdsfcfzotvIpaqa3mxbQLl3tmtXXuNBo5jJbj3IkRfxwoDBx5gj\"}', '2', 'http://localhost:8080/template/public/logout', '::1', '2017-01-31 23:09:26'),
('66616328-8553-43e4-8004-4fa1fa41cd75', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"0JFZRNyOPSDmiq9k7bnGd9dSJem62EmYg8oPkO9ep0QWNJKaZo5fn4tvTs48\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:17:36'),
('66b40a84-e56d-46bf-8501-27334114a4e0', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:39:11'),
('682e91ce-083b-4192-adba-21426fe6b47f', 'created', 8, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-01 01:00:11'),
('68a2ae60-dba7-4ca0-a942-22d2e20c303b', 'updated', 2015, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2016', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 13:53:12'),
('68cb6d91-4947-4fbf-9ede-c9dcd8ffd1e3', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"lGPLwlX6M68b39uNaUHZPzFMC7OrD4whp9lDBql7WnDcy7kkhwxps0hxaXjn\"}', '{\"remember_token\":\"vXdlqsnNzLAq1r5kjViIcj1KGn21D1V5V0IOOUeDzxBRASTPozD05iwUhicF\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 23:55:04'),
('6baa16ae-3154-4ac7-acde-6648065073a5', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"VmY7Lp5jt00ntCXqs6ICcM2dr8cPuN33H6GugBEGu7Zo26nRm4RMlVghew6m\"}', '{\"remember_token\":\"GINj2BrmESU3f1mQl3OFCjwmKHWXzpLm8V2MFqgUBomFntJwzPOM3uCsPdQj\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 03:02:15'),
('6bfda1d1-56c9-44d6-a133-ff1f9d7d7b03', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:36:26'),
('719ee0d3-2b06-4a9e-b564-e2c18d3ebe21', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"vXdlqsnNzLAq1r5kjViIcj1KGn21D1V5V0IOOUeDzxBRASTPozD05iwUhicF\"}', '{\"remember_token\":\"C2Vk7DgQeUjihvUeA9nQ868JRMkyobldPHTnte643SEBJuQXPVpVHL70GU2z\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-19 00:22:45'),
('72807897-5593-4ad3-89cf-a6fd1ce9a337', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:24:17'),
('731548cc-2b1a-469c-8c03-9797d0ad336e', 'created', 9, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1111\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-01 01:03:07'),
('7357db46-b92f-4c64-8543-8a73b8ce2d74', 'updated', 12131415, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 13:06:16'),
('74696c9a-eaf5-4f97-9850-0a95f174c464', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:03:25'),
('754d9db5-ca8a-4283-90ec-af6888a63a43', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:24:10'),
('7609d5d0-8c07-4024-ae70-1f021041c2f4', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"BqXQNDAenb99MZUtIQPOPFbNSPBwV2UDLctbec3g8PQHjjFASq1qG1FmifWM\"}', '{\"remember_token\":\"bkprexXtfhMRC3kENFtVsz5daoTuAtvevxif7bRMqSrgaKStGpSvgvOwsLBn\"}', '3013', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-08 02:10:20'),
('76e4e804-6ea0-49d7-afc7-82a45c69a5d7', 'created', 9, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2010\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:01:06'),
('79142e3e-3b57-47b9-b3f7-d94636cae909', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135776\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '1', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-08-26 07:17:07'),
('7a0735be-63b3-4f10-8fdf-2d2af5657271', 'updated', 121314, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:47:13'),
('7ae3b3e5-2233-4a97-a8f9-2c12eebfac1d', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"OfcDlePABaFmw7KiKWNahNWlELpYGoQ0VTTge994gB84vivrMltgWatU5mc8\"}', '{\"remember_token\":\"zicAZ5dyyPayoYNsBlcFyZgqXrUgLzksZT2KVBM9NqZLYlQOEpaVdYziepYV\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-19 00:22:56'),
('7eef0caa-a97d-43fa-9c60-6b204e34fc3b', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"azHaUbV3G25Fg6wrfRQhTHAbmJJYTdTCnKGOPwpYhF57ELFfY1rpO6Y9Ni0j\"}', '{\"remember_token\":\"QJO4kk1wGPqR0P0AChaCaLx5SIxlj0c4sVEtOvUIG3rIiGTVBipjUQOPWBhi\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-26 06:14:59'),
('7f6458e6-d945-434a-8868-1e4ed07625c4', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"4cuiXg3RVstQCOnBft6lOD6sEZPABaSkOdT7pZ3wBGbnmUn2xfn4rvdF8eJS\"}', '{\"remember_token\":\"OfcDlePABaFmw7KiKWNahNWlELpYGoQ0VTTge994gB84vivrMltgWatU5mc8\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 23:52:18'),
('7fe14936-bcd1-4e72-be5a-58af3eb7be11', 'created', 10, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2009\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:37:55'),
('821e2483-b444-463e-9e8c-f57caf7b2a03', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135845\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-22 05:53:55'),
('8278959c-cc8f-4de7-acbe-d223ff08a449', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"0qjdkXBCI9EOGhFSjbmlhkydSTErZ6VjH1M2WG938Ljd0adUQ9LcmmMW1MLo\"}', '{\"remember_token\":\"BqXQNDAenb99MZUtIQPOPFbNSPBwV2UDLctbec3g8PQHjjFASq1qG1FmifWM\"}', '3013', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-08 02:09:59'),
('83c349cb-7595-4e51-9956-f027adc51ee4', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-04 01:56:26'),
('87bfb887-63dd-4430-a7ee-d64b8fd5d99f', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"121314\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 02:10:15'),
('87e7974a-7b17-4c87-b4da-5df664483b19', 'created', 18, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2001\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:51:08'),
('89cdcdf5-cce2-48c3-acbf-4c8bbb86155f', 'updated', 3333, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"SAjdVQ2t418dL7OpbymWEh07sH6nVpmnWDIKvsjYsVSU9vPDhpQeteaAJtE3\"}', '3333', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-02 23:10:04'),
('8afed4f0-0168-4254-9f91-cde505da9207', 'created', 11, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135845\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registeraccount', '127.0.0.1', '2017-08-24 03:47:48'),
('8bce53aa-51e5-4df8-a083-87757431c89b', 'updated', 3433, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:00:04'),
('8d9bc1e4-f0e2-4ec0-840f-8c4e47ff9ccc', 'updated', 2013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 08:27:59'),
('8ee27e07-a97c-4d0f-ac86-d7eeafad0797', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1234\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2003', 'http://localhost:8000/administrator/register_account', '127.0.0.1', '2017-08-27 20:22:12'),
('90765a13-2659-4fc6-bc7f-8281a4334f23', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:13:43'),
('9261c982-7e11-41ae-afbe-05acc4208e73', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:03:25'),
('958f8007-066b-4f99-872f-848a23816aab', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:30:34'),
('98cb8693-3634-43e4-907a-4ea0a782a6a2', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"ngqvN7BkraDB3wsCvD6uN8IOJYTqB7usYk98ddfhnvLByAioYtAhRQ83Gejc\"}', '{\"remember_token\":\"DXjXpLMPL3rE3ZzqSaIdS1Yge9U1VpiNn3kU8k5QZhO3fFazHjVNKfeKG8vD\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-15 04:42:31'),
('98faf450-176e-46d9-ac50-d08298eef063', 'created', 4, 'Modules\\Template\\Entities\\MainNavigation', '[]', '{\"group_id\":\"2\",\"title\":\"Client list\",\"route\":\"inventory.clientlist\",\"parent\":\"0\",\"arangement\":\"1\",\"properties\":\"{\\\"i\\\":{\\\"class\\\":\\\"fa fa-id-card\\\"}}\"}', '1', 'http://localhost:8080/template/public/template/add-main-menus', '::1', '2017-01-31 22:33:27'),
('997febd3-58a7-48fb-a05d-696f9a44f9a9', 'updated', 4444, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"hIgXNPUsct228rbQnew6I0dcWdkjUGzDXNNb1Q8GF87miJQez47PfhIkZQKe\"}', '4444', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-02 23:38:21'),
('99cdafee-8493-41a3-9c2b-58522e61d153', 'created', 10, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2222\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-01 01:05:21'),
('9d44b418-d4fe-44f9-ad2d-6d752c9c9be8', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"bkprexXtfhMRC3kENFtVsz5daoTuAtvevxif7bRMqSrgaKStGpSvgvOwsLBn\"}', '{\"remember_token\":\"ybL8UjEnW7x9r1lrjRBHOK9dwNRFc4ZR6ahoAAJuoX5PmzLbZYkC8KetQqdL\"}', '3013', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-08 02:13:20'),
('9e4eed9a-4778-4c0c-a33c-09fe2fae7254', 'created', 7, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2012\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:58:25'),
('a215d6f9-e89c-4c44-a7b3-06d4d7b55796', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"QJO4kk1wGPqR0P0AChaCaLx5SIxlj0c4sVEtOvUIG3rIiGTVBipjUQOPWBhi\"}', '{\"remember_token\":\"MKxTmf5u09SVpNKw5fcEzdG7IRigaPNTekMhtuJchek76lKT6Dm8jvYO7x7o\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-26 06:32:44'),
('a22f1dce-b4c3-4262-a9dd-587e5accbcbc', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2015\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:18:10'),
('a476b2ff-7475-4b26-921a-10469add4ca3', 'created', 14, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135845\",\"group_id\":2,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-08-26 06:37:16'),
('a5a9515a-b3c1-43cc-b986-4b8e402513b2', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:34:15'),
('a6b5710c-76ca-4669-945c-999af88c0412', 'updated', 20135776, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"QVlTXaPbpd6qd5AXTrsIXNBiGXei4XyGHOpudcSWhjg3XIVW04n1yWJrWUea\"}', '20135776', 'http://localhost:8000/logout', '127.0.0.1', '2017-08-27 22:22:45'),
('a6cc64f9-c2e6-4293-b957-066b6873ab4b', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:04:11'),
('a776ebbf-bcd2-48a6-a33e-d819730ddcba', 'created', 11, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2008\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:38:26'),
('a7edb5f6-c124-419f-abab-073600858014', 'updated', 2015, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 13:53:18'),
('a99ae115-773f-4ca5-ae43-aaa630208aee', 'updated', 5370, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2003', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-08-26 07:21:40'),
('aa28ade0-3682-417c-a54f-3676cbe32f80', 'updated', 2000, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:10:55'),
('aa82f495-c3a2-4bc0-938b-cb94c0c32bde', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"DXjXpLMPL3rE3ZzqSaIdS1Yge9U1VpiNn3kU8k5QZhO3fFazHjVNKfeKG8vD\"}', '{\"remember_token\":\"tkAhPeslSmbprj2NHYTlOJgZLSCH8mshoDtsAGQOuNPCx9H9I3tDdInLO6l7\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-16 03:13:02'),
('aadbd432-daf3-4f3e-8979-019163b74ee0', 'created', 12, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2007\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:15:26'),
('ab12ecd4-327f-446a-ab80-509a88229109', 'created', 8, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2012\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:56:02'),
('ab8b6a1c-cb7c-4f89-89cf-18325ccbd450', 'created', 10, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2009\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:13:40'),
('adc9a2c5-70d5-441a-acd5-a2533afaadf1', 'updated', 20135776, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2003', 'http://localhost:8000/administrator/update_activity', '127.0.0.1', '2017-08-27 07:05:55'),
('ae7d2c37-8eb6-4605-85fe-b357f4806fbd', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"8L2PDE3kXBf9AwrQKL6IQ6R2Hkuszo2LLBnH14vqqLkNWyK1cASuInyWDzXe\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:18:52'),
('b2047b0f-099c-44f6-ad74-79a229620ad3', 'updated', 7777, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"x6EsskEOc2daeSx6aaTMAocTkbRNvTmTLM0ZGtcmEkX8oMkt3VRqw2RY0YwC\"}', '7777', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 04:23:14'),
('b26236fb-960d-403d-9692-b810af96218e', 'created', 11, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2008\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:14:14'),
('b2d745fc-2dd8-4878-a655-c4cc56348e48', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:35:55'),
('b33a06b8-cdf8-4d6e-9c32-df644f940fd8', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:15:51'),
('b595bfea-f0d0-44ac-82b1-c91a5e22939d', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:06:27'),
('b71474ae-d6b5-4c38-b07b-ef386b5a47e8', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"aBjNOUehx7JYCDrIGwvxa7OikQHjBcVYX89cCBdsmJxXZwOg6dp6REAUDLjL\"}', '{\"remember_token\":\"LYp1A23A8HvNZMKgfPndwWzvjAsMqVt8r2rm6BrVc55d7HHjV6Wz5dJi8T33\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 04:22:56'),
('b7647810-27a1-4377-98d5-acbff6bf6e91', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"LYp1A23A8HvNZMKgfPndwWzvjAsMqVt8r2rm6BrVc55d7HHjV6Wz5dJi8T33\"}', '{\"remember_token\":\"e9aa5CgyMpwWFGS9UF1cKqEmZVp2JiNRLmQi8WpsNa3M8GVThJy9UHuJfJSZ\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 04:27:22'),
('b7c07ae4-e458-40d8-9cd4-5a22105be1fd', 'updated', 3333, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"SAjdVQ2t418dL7OpbymWEh07sH6nVpmnWDIKvsjYsVSU9vPDhpQeteaAJtE3\"}', '{\"remember_token\":\"nAVJ9oZzOuA929UDOiwZLYdCrJKxBxmp6ObZxJfLFlcRXHFktny347oKIXc3\"}', '3333', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-02 23:34:08'),
('b9d5b6cd-144f-4547-ada9-afc74b6528c1', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"0qjdkXBCI9EOGhFSjbmlhkydSTErZ6VjH1M2WG938Ljd0adUQ9LcmmMW1MLo\"}', '3013', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-08 02:09:14'),
('ba129a2d-0947-4201-a4f3-ac3e6d7f55c5', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"FyIgNefF20pybE0oZKw7WvnXftyf7pfBTD2zn8XReCeA0vK46VcF7q3wHrt2\"}', '{\"remember_token\":\"DIQxIXi6UvUazehJvLsIh4K4hPSmc3T2fObpOt5I9ZwQDuMKDeQJQOi4h8px\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:20:23'),
('be51dce7-f12d-4d50-b8d1-af929dcf4dd9', 'created', 7, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2017\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-01 00:50:23'),
('be7b2c96-f073-43bc-a8da-645477be0ea3', 'updated', 2005, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:50:20'),
('beb314d7-6791-4750-a1e5-c2746e9d181c', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:16:41'),
('c06073e2-32f8-4bd8-884a-6776ec601d25', 'updated', 3433, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 05:01:41'),
('c1d945c8-3053-4d70-ac54-25bc16a68932', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2005', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 09:50:49'),
('c426fa16-d686-48b3-b978-6fa1f704cea0', 'updated', 2017, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"e8x9dcu130w6b2bPWelmKYMmtNnIdq16Vq1pYv7THClbV0JUZTdH7n18IK06\"}', '{\"remember_token\":\"Tw4BtIdRVfukAvG9OV3OJKPiqfp97oF2DPeJvfjhXvdhLT1rWrFWjAFzLHgj\"}', '2017', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-09 09:50:23'),
('c4ac5ecc-8696-43e9-8370-40963043e3f4', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2013\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:57:38'),
('c5beabdb-25de-49a5-9b8b-be26abc6c710', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"oW2w89w2tTYk2686A0m6P6sfbUeYVV52Xvoso8lqaoUyhlmqCs8G4ZNMr7BP\"}', '{\"remember_token\":\"SFqSU0aXyKFdZGA3hN9RatcKgdbpJUBLJKdt96qoXnF7PBbUyGY9V0pDnwv1\"}', '2', 'http://template.dev:8080/logout', '127.0.0.1', '2017-02-08 22:40:58'),
('c5eacf6b-b6df-44e9-958f-6ed17a39f396', 'created', 14, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"6666\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-03 04:06:55'),
('c902c2c5-8d9d-4695-a97b-b021af1390a2', 'created', 16, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"8888\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-03 04:27:19'),
('c9d810a9-9ce4-4923-a91a-762c6ce73bb9', 'updated', 2000, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 12:45:26'),
('cf457852-6aab-4af0-9565-f3829436bc4c', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2014\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:20:07'),
('d1892f10-36be-433c-a2d7-e82745f11b7d', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:30:24'),
('d2306afb-2455-46a3-86a1-18b1ad3980b2', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1213\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 02:09:10'),
('d4fe3ffd-a8fe-47dc-8c53-eca2baf5655a', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"EIoNbALgZZeI8CwNVB4AQA4J10HIOArc8sYP6t56vjHn2OcBqtj3GH2ZXdma\"}', '{\"remember_token\":\"9bXUerak1iVJLvS0O5hyKH0BtqHpPjFJKeXMGT8XPzfsSAMKONfjP709gajg\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-01 01:00:16'),
('d66652e3-a108-41c8-81ef-33369e6025dd', 'updated', 2017, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 11:01:26'),
('dad22586-fdca-4a17-8e78-388c060f3570', 'created', 3, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2000\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-06 05:10:36'),
('db9e8882-a5b3-4363-8b1c-70aa6e07a786', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-08 02:33:23'),
('dc559452-0e23-4b8c-8217-299018146f70', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"dlA36Vo1fErl550Qw1QicEznih5F3Gi3qz9posXTfeaIBUvxTSwT8l6VMZYI\"}', '{\"remember_token\":\"ngqvN7BkraDB3wsCvD6uN8IOJYTqB7usYk98ddfhnvLByAioYtAhRQ83Gejc\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-15 04:03:45'),
('dca7aeed-5006-41ea-9609-e2e6e2be10cb', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2015\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:52:50'),
('df35dd51-db25-4f08-802d-ff2afc133d4d', 'updated', 1213, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"jE64q4xHHEuG5ci98Pi5P1TW46iyvYSOQ42Hgeaudkbx1bAVd6PCdw6MU4KN\"}', '1213', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-08 02:13:44'),
('e055ca86-084e-459a-9fe8-7a65f87e0f26', 'created', 13, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2006\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:16:26'),
('e1ba23e8-dc31-4532-b8a6-ce5c4ba1bf15', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"9bXUerak1iVJLvS0O5hyKH0BtqHpPjFJKeXMGT8XPzfsSAMKONfjP709gajg\"}', '{\"remember_token\":\"Y6n7DqFKWgwZi57jYDckN0yvCuAC9Z3cpdsjC12GKr7uZPDR1gm5xEVs0wU3\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-01 01:05:27');
INSERT INTO `audits` (`id`, `type`, `auditable_id`, `auditable_type`, `old`, `new`, `user_id`, `route`, `ip_address`, `created_at`) VALUES
('e4226ef4-13af-4776-abab-d5c08519c1ad', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"0JFZRNyOPSDmiq9k7bnGd9dSJem62EmYg8oPkO9ep0QWNJKaZo5fn4tvTs48\"}', '{\"remember_token\":\"6ZhtXIFn9MWlD7zzEW90pacpYcDw2n4nhOEM6EF2OTaRSSmi4WL5K8mGTU0t\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-18 22:19:06'),
('e424a21d-d37f-45f5-906a-dfe329c4d891', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"P9COyrYogzVWmzribyMFE0PayN4ceZuMyqvwdYKjRRwYM5tQ6qUhvTaU2tzK\"}', '{\"remember_token\":\"VmY7Lp5jt00ntCXqs6ICcM2dr8cPuN33H6GugBEGu7Zo26nRm4RMlVghew6m\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-09-03 01:25:19'),
('e4494ca3-a32d-4bd2-9899-727edfc2d22c', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":null}', '{\"remember_token\":\"iIfHhGT7YuEAtS72RtEbyztEUw8pGRWbiYEmSPMEbYgjcz6t21mhTT5L1Qra\"}', '2003', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-26 07:21:55'),
('e719ee43-8393-4879-9c92-c76f55bec935', 'created', 8, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1234\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-23 01:59:44'),
('e7ff8d3c-cb9a-45c3-81dd-fbcdae8828d7', 'created', 8, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2011\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:58:59'),
('e9360060-745d-4a3f-836d-bea89b4522a4', 'created', 6, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2014\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2016', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:54:58'),
('e953b883-68d4-4719-92f4-e53576c26be6', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-06 04:59:14'),
('eb137caf-1da5-474d-96c5-99a8b6c02b66', 'created', 7, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"20135776\",\"group_id\":1,\"is_approved\":1}', '1', 'http://local.bghmc.com/administrator/registerpersonnel', '127.0.0.1', '2017-08-22 20:48:58'),
('efcf6c55-a21e-4cde-8a6b-aa80c2fdb66d', 'updated', 2, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"7435FmozIcj37DBEu1KCX1CyAE8s5oAvL7wsI9gXKwlQG7L4OySjthgRflgl\"}', '{\"remember_token\":\"jm3nu1sEjYeGwmYs2OjmyA3KJEmgzwR7jFgjViW9wYLdmH0717Oq0BwgvQ67\"}', '2', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-15 04:01:22'),
('f077d577-fa5d-4bdf-91e4-9cedde2dea97', 'created', 14, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2005\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:45:41'),
('f1658a8f-633a-4bca-a99e-34b830536251', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"gUFaSWi7Tt2hTmJ9Yf6fl77r12lKCRoWg0MncPwYFbVVY5Rao7Mc2aLMNnWV\"}', '{\"remember_token\":\"ULhSe9dAcN62dByBa9mhd2Pqe0O2TG9M1S63mcWthX5KwZgZeDs93XCOZS5I\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-21 22:45:03'),
('f3f5291d-a575-4ff2-8c86-102827c445d0', 'updated', 2007, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '2017', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-09 10:06:41'),
('f700f797-6e9f-4dd3-a233-57fd46215803', 'created', 14, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2005\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 09:24:52'),
('f914477b-4255-4efe-8ba9-95ec04ec2f75', 'updated', 5370, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":0}', '{\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-08-26 07:21:46'),
('f986aa55-7bf4-4ae1-bf1d-17462cf7c2c3', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2015\",\"group_id\":2,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 14:07:46'),
('fb0d3af9-8bdb-4bfd-b7e4-71b77cfaceb4', 'updated', 1, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"kB6tLDw2sXtFwiqPObVtdd0KsPU913gVich525cE7lnuc5j99S6MHM98dMY8\"}', '{\"remember_token\":\"nzGjAdAwQBaTMlMHvQ0rqtt8bz8UMoSc40haBZ8Ai8XiDnoktRCPUYHejdF0\"}', '1', 'http://local.bghmc.com/logout', '127.0.0.1', '2017-08-15 04:01:38'),
('fc6bff4b-e3b3-46de-a334-618b374c66fc', 'created', 13, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2006\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:40:20'),
('fdc69306-6d92-49fa-91d0-22cfcfd3d0ba', 'created', 5, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2014\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-09 08:55:27'),
('fe1576a7-09e2-4091-a7ae-f2e3e6b4bee2', 'created', 17, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"1213\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2003', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-03 05:12:27'),
('fe84e984-a715-4fb5-9004-fbd371facd18', 'updated', 2003, 'Modules\\Template\\Entities\\UserCredentials', '{\"remember_token\":\"iIfHhGT7YuEAtS72RtEbyztEUw8pGRWbiYEmSPMEbYgjcz6t21mhTT5L1Qra\"}', '{\"remember_token\":\"Uz9KI9uh9A1cJBwXE8dhXbIVs18RpLRuVdHg3KrQPMZyXul4jpLhzOzVR4q3\"}', '2003', 'http://localhost:8000/logout', '127.0.0.1', '2017-08-27 22:22:12'),
('ff16fdc7-b9d2-432d-9edd-8eebd2fbc4c6', 'created', 4, 'Modules\\Template\\Entities\\UserCredentials', '[]', '{\"emp_id\":\"2016\",\"group_id\":1,\"is_approved\":1,\"isactive\":1}', '2017', 'http://local.bghmc.com/administrator/register_account', '127.0.0.1', '2017-09-08 13:45:49'),
('ffd5f007-20d6-4404-940e-10a3b141234d', 'updated', 3013, 'Modules\\Template\\Entities\\UserCredentials', '{\"isactive\":1}', '{\"isactive\":0}', '3013', 'http://local.bghmc.com/administrator/update_activity', '127.0.0.1', '2017-09-04 01:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_account_activity`
--

CREATE TABLE `bghmc_account_activity` (
  `act_id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `deact_reason` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_account_activity`
--

INSERT INTO `bghmc_account_activity` (`act_id`, `emp_id`, `deact_reason`, `isactive`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2017, 'sample reason', 0, '2017-09-09 11:01:26', '2017-09-09 11:01:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_allfiles`
--

CREATE TABLE `bghmc_allfiles` (
  `tracking_no` int(11) NOT NULL,
  `file_name` varchar(80) NOT NULL,
  `filetype_id` int(11) NOT NULL,
  `receiverID` int(11) NOT NULL,
  `deptID_of_receiver` int(11) NOT NULL,
  `senderID` int(11) NOT NULL,
  `deptID_of_sender` int(11) NOT NULL,
  `attachmentID` int(11) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `_important` int(8) NOT NULL DEFAULT '0',
  `_drafts` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `_extra1` int(11) NOT NULL,
  `_extra2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bghmc_allfiles`
--

INSERT INTO `bghmc_allfiles` (`tracking_no`, `file_name`, `filetype_id`, `receiverID`, `deptID_of_receiver`, `senderID`, `deptID_of_sender`, `attachmentID`, `remarks`, `_important`, `_drafts`, `created_at`, `updated_at`, `_extra1`, `_extra2`) VALUES
(100001, 'Death Certificate', 70003, 8484, 680013, 4999, 68005, 100001, 'None', 1, 0, '2017-08-29 11:29:40', '2017-08-27 15:30:12', 0, 0),
(100002, 'Bills', 70002, 1234, 68006, 4999, 68005, 100002, 'None', 1, 0, '2017-08-28 04:25:11', '2017-08-27 15:31:14', 0, 0),
(100018, 'Billing and Receipts', 70003, 1234, 68006, 8484, 680013, 100018, 'None', 0, 0, '2017-08-29 11:30:02', '2017-08-28 00:06:30', 0, 0),
(100019, 'Communication letter', 70001, 4999, 68005, 1234, 68006, 100019, 'My Message', 0, 0, '2017-08-28 04:25:31', '2017-08-27 19:05:48', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_departments`
--

CREATE TABLE `bghmc_departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_departments`
--

INSERT INTO `bghmc_departments` (`dept_id`, `dept_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(68001, 'Medical Center Chief', '2017-09-09 10:59:15', NULL, NULL),
(68002, 'Quality Management Office', '2017-09-09 10:59:15', NULL, NULL),
(68003, 'Professional Education and Training Office', '2017-09-09 10:59:15', NULL, NULL),
(68004, 'Integrated Hospital Operation Management Office', '2017-09-09 10:59:15', NULL, NULL),
(68005, 'Legal Services', '2017-09-09 10:59:15', NULL, NULL),
(68006, 'Hems', '2017-09-09 10:59:15', NULL, NULL),
(68007, 'Anesthesiology', '2017-09-09 10:59:15', NULL, NULL),
(68008, 'Family Medicine', '2017-09-09 10:59:15', NULL, NULL),
(68009, 'Internal Medicine', '2017-09-09 10:59:15', NULL, NULL),
(680010, 'Dental', '2017-09-09 10:59:15', NULL, NULL),
(680011, 'Obstetrics & Gynecology', '2017-09-09 10:59:15', NULL, NULL),
(680012, 'Ophthalmology', '2017-09-09 10:59:15', NULL, NULL),
(680013, 'ORL -HNS', '2017-09-09 10:59:15', NULL, NULL),
(680014, 'Orthopedics & Rehabilitation Medicine', '2017-09-09 10:59:15', NULL, NULL),
(680015, 'Pediatrics', '2017-09-09 10:59:15', NULL, NULL),
(680016, 'Psychiatry', '2017-09-09 10:59:15', NULL, NULL),
(680017, 'Surgery', '2017-09-09 10:59:15', NULL, NULL),
(680018, 'Out Patient Department', '2017-09-09 10:59:15', NULL, NULL),
(680019, 'Emergency Room', '2017-09-09 10:59:15', NULL, NULL),
(680020, 'Reproductive Health Care Unit', '2017-09-09 10:59:15', NULL, NULL),
(680021, 'Operating Room', '2017-09-09 10:59:15', NULL, NULL),
(680022, 'Pay Services', '2017-09-09 10:59:15', NULL, NULL),
(680023, 'WCPU', '2017-09-09 10:59:15', NULL, NULL),
(680024, 'Cancer Center', '2017-09-09 10:59:15', NULL, NULL),
(680025, 'Pathology', '2017-09-09 10:59:15', NULL, NULL),
(680026, 'Imaging', '2017-09-09 10:59:15', NULL, NULL),
(680027, 'Pharmacy', '2017-09-09 10:59:15', NULL, NULL),
(680028, 'Health Information Management Office', '2017-09-09 10:59:15', NULL, NULL),
(680029, 'Nutrition and Deitetics Office', '2017-09-09 10:59:15', NULL, NULL),
(680030, 'Medical social Works Services Office', '2017-09-09 10:59:15', NULL, NULL),
(680031, 'Nursing Quality Standards and Policy Unit', '2017-09-09 10:59:15', NULL, NULL),
(680032, 'Center Supply Room (CSR)', '2017-09-09 10:59:15', NULL, NULL),
(680033, 'Medical', '2017-09-09 10:59:15', NULL, NULL),
(680034, 'Orthopedics', '2017-09-09 10:59:15', NULL, NULL),
(680035, 'Hemodialysis', '2017-09-09 10:59:15', NULL, NULL),
(680036, 'Delivery Room Complex', '2017-09-09 10:59:15', NULL, NULL),
(680037, 'Contagious Disease Unit', '2017-09-09 10:59:15', NULL, NULL),
(680038, 'Pay/PR/SPR Suite', '2017-09-09 10:59:15', NULL, NULL),
(680039, 'Procurement Management Office', '2017-09-09 10:59:15', NULL, NULL),
(680040, 'Materials Management Office', '2017-09-09 10:59:15', NULL, NULL),
(680041, 'Security Services Office', '2017-09-09 10:59:15', NULL, NULL),
(680042, 'Human Resource Management Office', '2017-09-09 10:59:15', NULL, NULL),
(680043, 'Engineering & Facilities Management Office', '2017-09-09 10:59:15', NULL, NULL),
(680044, 'General Services Unit', '2017-09-09 10:59:15', NULL, NULL),
(680045, 'Cash Operations Office', '2017-09-09 10:59:15', NULL, NULL),
(680046, 'Accounting Office', '2017-09-09 10:59:15', NULL, NULL),
(680047, 'Billing and Claims Office', '2017-09-09 10:59:15', NULL, NULL),
(680048, 'Budget Office', '2017-09-09 10:59:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_employee_info`
--

CREATE TABLE `bghmc_employee_info` (
  `emp_id` int(11) NOT NULL,
  `password` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accnt_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_employee_info`
--

INSERT INTO `bghmc_employee_info` (`emp_id`, `password`, `f_name`, `l_name`, `accnt_type`, `pos_id`, `dept_id`, `isactive`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2017, '$2y$10$MJK2SKuOLQK.e6XfYNZDp.ZHfr40FI.v1By3TGomVQvwww7iEPn1W', 'Ezekiel', 'Hermiston', 'administrator', 80001, 680032, 0, '2017-09-09 10:59:21', '2017-09-09 11:01:25', NULL),
(2018, '$2y$10$fst5RPGHdafCnsZqKnKACOID9k4x50ZJe8HnEH98XzlGhbYI1b0rO', 'Alize', 'Wunsch', 'personnel', 80001, 68006, 1, '2017-09-09 10:59:21', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_emp_credentials`
--

CREATE TABLE `bghmc_emp_credentials` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL COMMENT 'refer to bghmc_user_groups',
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_approved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bghmc_emp_credentials`
--

INSERT INTO `bghmc_emp_credentials` (`id`, `emp_id`, `password`, `group_id`, `remember_token`, `isactive`, `created_at`, `updated_at`, `deleted_at`, `is_approved`) VALUES
(1, 2017, '$2y$10$oyqauLvd34lHcseLRz/6yusVnMba./Y48jIA1giIJhFwW3qGKDjkq', 1, NULL, 0, '2017-09-09 10:59:02', '2017-09-09 11:01:25', NULL, 1),
(2, 2018, '$2y$10$oyqauLvd34lHcseLRz/6yusVnMba./Y48jIA1giIJhFwW3qGKDjkq', 2, NULL, 1, '2017-09-09 10:59:02', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_filetypes`
--

CREATE TABLE `bghmc_filetypes` (
  `filetype_id` int(11) NOT NULL,
  `filetype_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_filetypes`
--

INSERT INTO `bghmc_filetypes` (`filetype_id`, `filetype_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(70001, 'Communication letter', '2017-09-09 10:59:25', NULL, NULL),
(70002, 'Purchase order', '2017-09-09 10:59:25', NULL, NULL),
(70003, 'Medical Records', '2017-09-09 10:59:25', NULL, NULL),
(70004, 'Department Vouchers', '2017-09-09 10:59:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_positions`
--

CREATE TABLE `bghmc_positions` (
  `pos_id` int(11) NOT NULL,
  `pos_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_positions`
--

INSERT INTO `bghmc_positions` (`pos_id`, `pos_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(80001, 'hospital chief', '2017-09-09 10:59:29', NULL, NULL),
(80002, 'department head', '2017-09-09 10:59:29', NULL, NULL),
(80003, 'administrative officer', '2017-09-09 10:59:29', NULL, NULL),
(80004, 'administrative assistant', '2017-09-09 10:59:29', NULL, NULL),
(80005, 'training officer', '2017-09-09 10:59:29', NULL, NULL),
(80006, 'medical officer', '2017-09-09 10:59:29', NULL, NULL),
(80007, 'medical specialist', '2017-09-09 10:59:29', NULL, NULL),
(80008, 'head nurse', '2017-09-09 10:59:29', NULL, NULL),
(80009, 'nurse', '2017-09-09 10:59:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_system_logs`
--

CREATE TABLE `bghmc_system_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `logged_user` int(11) NOT NULL,
  `log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_data_before` longtext COLLATE utf8mb4_unicode_ci,
  `log_data_after` longtext COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_system_logs`
--

INSERT INTO `bghmc_system_logs` (`id`, `logged_user`, `log`, `log_data_before`, `log_data_after`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2017, 'Account of \"Ezekiel Hermiston\" with ID 2017 deactivated.', 'N;', 'a:4:{s:6:\"_token\";s:40:\"KPGAjHRt1gS1fFMJZ6TNq2wk8Bg4KpL3nPmMgVOe\";s:4:\"e_id\";s:4:\"2017\";s:6:\"reason\";s:13:\"sample reason\";s:3:\"act\";s:10:\"deactivate\";}', 'update_activity', '2017-09-09 11:01:26', '2017-09-09 11:01:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_tmpl_main_navigation`
--

CREATE TABLE `bghmc_tmpl_main_navigation` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL COMMENT 'refer to bghmc_user_groups',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '0 no subs , 1 parent with subs',
  `arangement` int(10) UNSIGNED NOT NULL COMMENT 'arangement of the navigation',
  `properties` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bghmc_tmpl_main_navigation`
--

INSERT INTO `bghmc_tmpl_main_navigation` (`id`, `group_id`, `title`, `route`, `parent`, `arangement`, `properties`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Dashboard', 'admin.index', '0', 1, '{\"i\" : {\"class\":\"fa fa-dashboard\"}}', NULL, NULL, NULL),
(2, 1, 'Accounts', 'admin.accounts_list', '0', 2, '{\"i\" : {\"class\":\"fa fa-group\"}}', NULL, NULL, NULL),
(3, 1, 'References', '', '1', 3, '{\"i\" : {\"class\":\"fa fa-file-text\"}}', NULL, NULL, NULL),
(4, 1, 'File Archives', 'admin.file_archives', '0', 4, '{\"i\" : {\"class\":\"fa fa-archive\"}}', NULL, NULL, NULL),
(5, 1, 'Reports', 'admin.file_generate', '0', 5, '{\"i\" : {\"class\":\"fa fa-file-text\"}}', NULL, NULL, NULL),
(6, 2, 'File Archives', 'admin.file_archives', '0', 6, '{\"i\" : {\"class\":\"fa fa-archive\"}}', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_tmpl_sub_navigation`
--

CREATE TABLE `bghmc_tmpl_sub_navigation` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'refer to bghmc_tmpl_main_navigation',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arangement` int(10) UNSIGNED NOT NULL COMMENT 'arangement of the navigation',
  `properties` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bghmc_tmpl_sub_navigation`
--

INSERT INTO `bghmc_tmpl_sub_navigation` (`id`, `parent_id`, `title`, `route`, `arangement`, `properties`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Departments', 'admin.departments_list', 1, '{\"i\" : {\"class\":\"fa fa-building-o\"}}', NULL, NULL, NULL),
(2, 3, 'Positions', 'admin.positions_list', 1, '{\"i\" : {\"class\":\"fa fa-user-md\"}}', NULL, NULL, NULL),
(3, 3, 'File Types', 'admin.filetypes_list', 1, '{\"i\" : {\"class\":\"fa fa-word-o\"}}', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bghmc_user_groups`
--

CREATE TABLE `bghmc_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `ugrp_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ugrp_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ugrp_homepage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bghmc_user_groups`
--

INSERT INTO `bghmc_user_groups` (`id`, `ugrp_name`, `ugrp_description`, `ugrp_homepage`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Privileged users', 'admin.index', NULL, NULL),
(2, 'Personnel', 'None Priveledged users ', 'inventory.index', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `file_attachments`
--

CREATE TABLE `file_attachments` (
  `attachment_id` int(11) NOT NULL,
  `attachment_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2017_02_01_064256_create_disease_info', 2),
(27, '2017_02_02_052558_create_deceased_person_info', 3),
(181, '2017_08_18_112342_employee_info', 13),
(182, '2017_08_18_123018_positions', 13),
(183, '2017_08_18_123046_departments', 13),
(184, '2017_08_22_100800_filetypes', 13),
(185, '2017_09_04_091130_account_activity', 13),
(186, '2017_09_08_211238_bghmc_system_logs', 13),
(187, '2016_12_21_145601_create_dnlx_user_group', 14),
(188, '2016_12_21_145852_create_dnlx_user_credentials', 14),
(189, '2017_01_14_023420_create_main_navigation_table', 14),
(190, '2017_01_14_023843_create_sub_navigation_table', 14);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audits_auditable_id_auditable_type_index` (`auditable_id`,`auditable_type`(191));

--
-- Indexes for table `bghmc_account_activity`
--
ALTER TABLE `bghmc_account_activity`
  ADD PRIMARY KEY (`act_id`),
  ADD KEY `bghmc_account_activity` (`emp_id`,`isactive`);

--
-- Indexes for table `bghmc_allfiles`
--
ALTER TABLE `bghmc_allfiles`
  ADD UNIQUE KEY `tracking_no` (`tracking_no`),
  ADD KEY `depttID_of_sender` (`deptID_of_sender`);

--
-- Indexes for table `bghmc_departments`
--
ALTER TABLE `bghmc_departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `bghmc_employee_info`
--
ALTER TABLE `bghmc_employee_info`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `bghmc_employee_info` (`accnt_type`,`l_name`,`isactive`);

--
-- Indexes for table `bghmc_emp_credentials`
--
ALTER TABLE `bghmc_emp_credentials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bghmc_emp_credentials` (`id`,`group_id`,`emp_id`);

--
-- Indexes for table `bghmc_filetypes`
--
ALTER TABLE `bghmc_filetypes`
  ADD PRIMARY KEY (`filetype_id`);

--
-- Indexes for table `bghmc_positions`
--
ALTER TABLE `bghmc_positions`
  ADD PRIMARY KEY (`pos_id`),
  ADD KEY `bghmc_positions` (`pos_name`);

--
-- Indexes for table `bghmc_system_logs`
--
ALTER TABLE `bghmc_system_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bghmc_system_logs` (`logged_user`);

--
-- Indexes for table `bghmc_tmpl_main_navigation`
--
ALTER TABLE `bghmc_tmpl_main_navigation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bghmc_tmpl_main_navigation` (`id`,`title`,`route`,`parent`,`arangement`);

--
-- Indexes for table `bghmc_tmpl_sub_navigation`
--
ALTER TABLE `bghmc_tmpl_sub_navigation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bghmc_tmpl_sub_navigation` (`id`,`parent_id`,`title`,`route`,`arangement`);

--
-- Indexes for table `bghmc_user_groups`
--
ALTER TABLE `bghmc_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bghmc_user_groups_ugrp_name_unique` (`ugrp_name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bghmc_account_activity`
--
ALTER TABLE `bghmc_account_activity`
  MODIFY `act_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bghmc_emp_credentials`
--
ALTER TABLE `bghmc_emp_credentials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bghmc_system_logs`
--
ALTER TABLE `bghmc_system_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bghmc_tmpl_main_navigation`
--
ALTER TABLE `bghmc_tmpl_main_navigation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bghmc_tmpl_sub_navigation`
--
ALTER TABLE `bghmc_tmpl_sub_navigation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bghmc_user_groups`
--
ALTER TABLE `bghmc_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
